<?php
/**
 * @file
 * lollatinoguides.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function lollatinoguides_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'language_content_type_lolguide';
  $strongarm->value = '0';
  $export['language_content_type_lolguide'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_options_lolguide';
  $strongarm->value = array(
    0 => 'main-menu',
  );
  $export['menu_options_lolguide'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_parent_lolguide';
  $strongarm->value = 'main-menu:0';
  $export['menu_parent_lolguide'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_lolguide';
  $strongarm->value = array(
    0 => 'promote',
  );
  $export['node_options_lolguide'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_preview_lolguide';
  $strongarm->value = '0';
  $export['node_preview_lolguide'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_submitted_lolguide';
  $strongarm->value = 1;
  $export['node_submitted_lolguide'] = $strongarm;

  return $export;
}
