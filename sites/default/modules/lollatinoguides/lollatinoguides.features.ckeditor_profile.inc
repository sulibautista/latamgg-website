<?php
/**
 * @file
 * lollatinoguides.features.ckeditor_profile.inc
 */

/**
 * Implements hook_ckeditor_profile_defaults().
 */
function lollatinoguides_ckeditor_profile_defaults() {
  $data = array(
    'LoLGuides' => array(
      'name' => 'LoLGuides',
      'settings' => array(
        'ss' => 2,
        'toolbar' => '[
    [\'Format\',\'Bold\',\'Italic\',\'Underline\',\'Strike\',\'TextColor\',\'RemoveFormat\'],
    [\'JustifyLeft\',\'JustifyCenter\',\'JustifyRight\',\'-\',\'NumberedList\',\'BulletedList\',\'-\',\'HorizontalRule\',\'-\',\'Outdent\',\'Indent\'],
    [\'Link\',\'Unlink\',\'-\',\'Image\',\'OCUpload\',\'Video\'],
    [\'LoldataViewMode\',\'LoldataSize\']
]',
        'expand' => 't',
        'default' => 't',
        'show_toggle' => 'f',
        'uicolor' => 'default',
        'uicolor_user' => '#D3D3D3',
        'width' => '100%',
        'lang' => 'es',
        'auto_lang' => 'f',
        'language_direction' => 'default',
        'allowed_content' => 't',
        'extraAllowedContent' => '',
        'enter_mode' => 'p',
        'shift_enter_mode' => 'br',
        'font_format' => 'p;pre;h3;h4;h5',
        'custom_formatting' => 'f',
        'formatting' => array(
          'custom_formatting_options' => array(
            'indent' => 'indent',
            'breakBeforeOpen' => 'breakBeforeOpen',
            'breakAfterOpen' => 'breakAfterOpen',
            'breakAfterClose' => 'breakAfterClose',
            'breakBeforeClose' => 0,
            'pre_indent' => 0,
          ),
        ),
        'css_mode' => 'self',
        'css_path' => '%t/css/editor.css',
        'css_style' => 'theme',
        'styles_path' => '',
        'filebrowser' => 'none',
        'filebrowser_image' => '',
        'filebrowser_flash' => '',
        'UserFilesPath' => '%b%f/',
        'UserFilesAbsolutePath' => '%d%b%f/',
        'forcePasteAsPlainText' => 'f',
        'html_entities' => 'f',
        'scayt_autoStartup' => 'f',
        'theme_config_js' => 'f',
        'js_conf' => '// We set h1,h2,h3 in the GUI so we can have the \'Header 1,2,3\' in the title, little hack until we find
// how to override lang strings
//config.format_h1  = {element: \'h3\'};
//config.format_h2  = {element: \'h4\'};
//config.format_h3  = {element: \'h5\'};

config.removePlugins = \'resize,elementspath\';
//config.autoGrow_onStartup = true;
config.autoGrow_maxHeight = 500;  
config.disableObjectResizing = true;
config.removeDialogTabs = \'image:advanced;link:advanced;link:target\';',
        'loadPlugins' => array(
          'loldataembed' => array(
            'name' => 'loldataembed',
            'desc' => 'LoLData Embed',
            'path' => '%base_path%sites/default/modules/loldata_embed/loldataembed/',
            'buttons' => FALSE,
          ),
          'ocupload' => array(
            'name' => 'OCUpload',
            'desc' => 'One Click Upload',
            'path' => '%base_path%sites/all/modules/ocupload/static/',
            'buttons' => array(
              'OCUpload' => array(
                'icon' => 'button.png',
                'label' => 'One Click Upload',
              ),
            ),
          ),
        ),
      ),
      'input_formats' => array(
        'guides_text_format' => 'Texto de las guias',
      ),
    ),
  );
  return $data;
}
