<?php
/**
 * @file
 * lollatinoguides.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function lollatinoguides_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function lollatinoguides_node_info() {
  $items = array(
    'lolguide' => array(
      'name' => t('Guía de campeón'),
      'base' => 'node_content',
      'description' => t('Guía de campeones de League of Legends.'),
      'has_title' => '1',
      'title_label' => t('Título'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
