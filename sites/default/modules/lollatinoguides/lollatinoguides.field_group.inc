<?php
/**
 * @file
 * lollatinoguides.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function lollatinoguides_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_lolguide_abilities|node|lol_guide|form';
  $field_group->group_name = 'group_lolguide_abilities';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'lolguide';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Habilidades',
    'weight' => '4',
    'children' => array(
      0 => 'field_lolguide_abilities',
      1 => 'field_lolguide_abilities_desc',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Habilidades',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-lolguide-abilities field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $export['group_lolguide_abilities|node|lol_guide|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_lolguide_abilities|node|lolguide|default';
  $field_group->group_name = 'group_lolguide_abilities';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'lolguide';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Habilidades',
    'weight' => '4',
    'children' => array(
      0 => 'field_lolguide_abilities',
      1 => 'field_lolguide_abilities_desc',
    ),
    'format_type' => 'html5',
    'format_settings' => array(
      'label' => 'Habilidades',
      'instance_settings' => array(
        'classes' => 'lolguide-section panel panel-body',
        'wrapper' => 'section',
        'id' => 'node_lolguide_full_group_lolguide_abilities',
      ),
    ),
  );
  $export['group_lolguide_abilities|node|lolguide|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_lolguide_basic|node|lol_guide|form';
  $field_group->group_name = 'group_lolguide_basic';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'lolguide';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Guía',
    'weight' => '0',
    'children' => array(
      0 => 'field_lolguide_background_image',
      1 => 'field_lolguide_champion',
      2 => 'field_lolguide_form_status',
      3 => 'field_lolguide_role',
      4 => 'field_lolguide_summoner',
      5 => 'field_lolguide_join_contest',
      6 => 'title',
      7 => 'group_lolguide_contest',
    ),
    'format_type' => 'html-element',
    'format_settings' => array(
      'label' => 'Guía',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'lolguide-main-section',
        'element' => 'div',
        'attributes' => '',
        'id' => 'node_lolguide_form_group_lolguide_basic',
      ),
    ),
  );
  $export['group_lolguide_basic|node|lol_guide|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_lolguide_basic|node|lolguide|default';
  $field_group->group_name = 'group_lolguide_basic';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'lolguide';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Guide',
    'weight' => '7',
    'children' => array(),
    'format_type' => 'hidden',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'element' => 'div',
        'classes' => '',
        'attributes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_lolguide_basic|node|lolguide|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_lolguide_intro|node|lolguide|default';
  $field_group->group_name = 'group_lolguide_intro';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'lolguide';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Introducción',
    'weight' => '0',
    'children' => array(
      0 => 'field_lolguide_intro',
    ),
    'format_type' => 'html5',
    'format_settings' => array(
      'label' => 'Introducción',
      'instance_settings' => array(
        'classes' => 'lolguide-section panel panel-body',
        'wrapper' => 'section',
        'id' => 'node_lolguide_full_group_lolguide_intro',
      ),
    ),
  );
  $export['group_lolguide_intro|node|lolguide|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_lolguide_intro|node|lolguide|form';
  $field_group->group_name = 'group_lolguide_intro';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'lolguide';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Introducción',
    'weight' => '1',
    'children' => array(
      0 => 'field_lolguide_intro',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Introducción',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => '',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $export['group_lolguide_intro|node|lolguide|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_lolguide_items|node|lol_guide|form';
  $field_group->group_name = 'group_lolguide_items';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'lolguide';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Objetos',
    'weight' => '6',
    'children' => array(
      0 => 'field_lolguide_items',
      1 => 'field_lolguide_items_desc',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Objetos',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-lolguide-items field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $export['group_lolguide_items|node|lol_guide|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_lolguide_items|node|lolguide|default';
  $field_group->group_name = 'group_lolguide_items';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'lolguide';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Objetos',
    'weight' => '5',
    'children' => array(
      0 => 'field_lolguide_items',
      1 => 'field_lolguide_items_desc',
    ),
    'format_type' => 'html5',
    'format_settings' => array(
      'label' => 'Objetos',
      'instance_settings' => array(
        'classes' => 'lolguide-section panel panel-body',
        'wrapper' => 'section',
        'id' => 'node_lolguide_full_group_lolguide_items',
      ),
    ),
  );
  $export['group_lolguide_items|node|lolguide|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_lolguide_masteries|node|lol_guide|form';
  $field_group->group_name = 'group_lolguide_masteries';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'lolguide';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Maestrías',
    'weight' => '5',
    'children' => array(
      0 => 'field_lolguide_masteries',
      1 => 'field_lolguide_masteries_desc',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Maestrías',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-lolguide-masteries field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $export['group_lolguide_masteries|node|lol_guide|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_lolguide_masteries|node|lolguide|default';
  $field_group->group_name = 'group_lolguide_masteries';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'lolguide';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Maestrías',
    'weight' => '3',
    'children' => array(
      0 => 'field_lolguide_masteries',
      1 => 'field_lolguide_masteries_desc',
    ),
    'format_type' => 'html5',
    'format_settings' => array(
      'label' => 'Maestrías',
      'instance_settings' => array(
        'classes' => 'lolguide-section  panel panel-body',
        'wrapper' => 'section',
        'id' => 'node_lolguide_full_group_lolguide_masteries',
      ),
    ),
  );
  $export['group_lolguide_masteries|node|lolguide|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_lolguide_runes|node|lolguide|default';
  $field_group->group_name = 'group_lolguide_runes';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'lolguide';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Runas',
    'weight' => '1',
    'children' => array(
      0 => 'field_lolguide_runes',
      1 => 'field_lolguide_runes_desc',
    ),
    'format_type' => 'html5',
    'format_settings' => array(
      'label' => 'Runas',
      'instance_settings' => array(
        'classes' => 'lolguide-section  panel panel-body',
        'wrapper' => 'section',
        'id' => 'node_lolguide_full_group_lolguide_runes',
      ),
    ),
  );
  $export['group_lolguide_runes|node|lolguide|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_lolguide_runes|node|lolguide|form';
  $field_group->group_name = 'group_lolguide_runes';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'lolguide';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Runas',
    'weight' => '3',
    'children' => array(
      0 => 'field_lolguide_runes',
      1 => 'field_lolguide_runes_desc',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Runas',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-lolguide-runes field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $export['group_lolguide_runes|node|lolguide|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_lolguide_section|field_collection_item|field_lolguide_section|default';
  $field_group->group_name = 'group_lolguide_section';
  $field_group->entity_type = 'field_collection_item';
  $field_group->bundle = 'field_lolguide_section';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Guide Section',
    'weight' => '0',
    'children' => array(
      0 => 'field_lolguide_section_title',
      1 => 'field_lolguide_section_body',
    ),
    'format_type' => 'html5',
    'format_settings' => array(
      'label' => 'Guide Section',
      'instance_settings' => array(
        'classes' => 'lolguide-section panel panel-body',
        'wrapper' => 'section',
        'id' => 'field_collection_item_field_lolguide_section_full_group_lolguide_section',
      ),
    ),
  );
  $export['group_lolguide_section|field_collection_item|field_lolguide_section|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_lolguide_section|field_collection_item|field_lolguide_section|form';
  $field_group->group_name = 'group_lolguide_section';
  $field_group->entity_type = 'field_collection_item';
  $field_group->bundle = 'field_lolguide_section';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Guide Section',
    'weight' => '0',
    'children' => array(
      0 => 'field_lolguide_section_title',
      1 => 'field_lolguide_section_body',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Guide Section',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => '',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $export['group_lolguide_section|field_collection_item|field_lolguide_section|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_lolguide_spells|node|lol_guide|form';
  $field_group->group_name = 'group_lolguide_spells';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'lolguide';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Hechizos de Invocador',
    'weight' => '2',
    'children' => array(
      0 => 'field_lolguide_spells',
      1 => 'field_lolguide_spells_desc',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Hechizos de Invocador',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => '',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $export['group_lolguide_spells|node|lol_guide|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_lolguide_spells|node|lolguide|default';
  $field_group->group_name = 'group_lolguide_spells';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'lolguide';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Hechizos de Invocador',
    'weight' => '2',
    'children' => array(
      0 => 'field_lolguide_spells',
      1 => 'field_lolguide_spells_desc',
    ),
    'format_type' => 'html5',
    'format_settings' => array(
      'label' => 'Hechizos de Invocador',
      'instance_settings' => array(
        'classes' => 'lolguide-section panel panel-body',
        'wrapper' => 'section',
        'id' => 'node_lolguide_full_group_lolguide_spells',
      ),
    ),
  );
  $export['group_lolguide_spells|node|lolguide|default'] = $field_group;

  return $export;
}
