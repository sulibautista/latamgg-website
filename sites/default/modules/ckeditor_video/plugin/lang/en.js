CKEDITOR.plugins.setLang('video', 'en', {
	buttonLabel: 'Video',
	videoProperties: 'Video Properties',
	videoInformation: 'Video Information',
	videoUrl: 'URL',
	invalidUrl: 'Can not get the video from the provided URL.'
});