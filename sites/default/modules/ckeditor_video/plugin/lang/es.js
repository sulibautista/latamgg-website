CKEDITOR.plugins.setLang('video', 'es', {
	buttonLabel: 'Video',
	videoProperties: 'Propiedades de Video',
	videoInformation: 'Información de Video',
	videoUrl: 'URL',
	invalidUrl: 'No se puede obtener el video desde la URL proporcionada.'
});