if(!Drupal.settings.ckeditorVideo) Drupal.settings.ckeditorVideo = {};
if(!Drupal.settings.ckeditorVideo.providers) Drupal.settings.ckeditorVideo.providers = [];

Drupal.settings.ckeditorVideo.providers.push(
	{
		type: 'youtube',
		label: 'YouTube',
		tryProcessURL: function(url) {
			var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/,
				match = url.match(regExp),
				videoId;
				
			if(match && match[2].length == 11){
			    videoId = match[2];
			    return {
					placeholder: 'http://img.youtube.com/vi/' + videoId +'/hqdefault.jpg',
					videoId: videoId
				};
			} 		
			
			return false;
		}
	},
	{
		type: 'twitch',
		label: 'Twitch.tv',
		tryProcessURL: function(url){
			var regExp = /^.*twitch.tv\/([^\/]+).*\/([0-9]+)/,
				match = url.match(regExp);
			
			if(match){
			    return {
					channel: match[1],
					chapter: match[2]
				};
			}
			
			return false;
		}		
	}
);

(function($){
	"use strict";
	
	var emptyPlaceholder,
		dialogAdded = false,
		
		embeddedRegExp = /\[\[video:([^\]]+)\]\]/gi,
		dummyContainer = $('<div>'),
		
		embeddedToVideoInputRules = {
			text: function(text){
				return text.replace(embeddedRegExp, function(match, dataStr){
					try {		
						return createPlaceholder(JSON.parse(dataStr).placeholder || emptyPlaceholder, dataStr);
					} catch(e){
						console.log(e);
						return '';
					}				
				});
			}
		},
			
		videoToEmbbededOutputRules = {
			elements: {
				img: function(element){
					var attrs = element.attributes,
						video = attrs && attrs['data-cke-video'];
										
					if(video) {
						return new CKEDITOR.htmlParser.text('[[video:' + dummyContainer.html(video).text() + ']]');
					}
				}
			}				
		};
	
	function createPlaceholder(placeholderUrl, dataStr) {
		return $('<img src="' + placeholderUrl + '">')
			//.attr('data-cke-realelement', 1)
			.attr('data-cke-video', dataStr)
			[0].outerHTML;
	}
	
	function getSelectedVideo(editor, element){
		if(!element){
	        element = editor.getSelection().getSelectedElement();
		}
		
		if(element && element.is('img') && element.data('cke-video')){
			return element;
		}	
	}
		
	CKEDITOR.plugins.add('video', {
		requires: 'dialog',
		lang: 'en,es',
		icons: 'video',
		
		init: function(editor) {	
			emptyPlaceholder || (emptyPlaceholder = this.path + 'images/placeholder.png');
			
			if(!dialogAdded){
				addDialog();
				dialogAdded = true;
			}

			// Transforms [[video:]] tags to placeholders
			editor.dataProcessor.dataFilter.addRules(embeddedToVideoInputRules);
			
			// Transforms video placeholders to [[video:]] tags
			editor.dataProcessor.htmlFilter.addRules(videoToEmbbededOutputRules);
			
			editor.on('dialogShow', function(event){
				var dialog = event.data;
				if(dialog.getName() == 'image' && getSelectedVideo(editor)){
	       			dialog.hide();
	       			editor.execCommand('videoDialog');
				}
			});
				
			editor.addCommand('videoDialog', new CKEDITOR.dialogCommand('videoDialog'));
			
			editor.ui.addButton('Video', {
			    label: editor.lang.video.buttonLabel,
			    command: 'videoDialog',
			    toolbar: 'insert'
			});
			
			editor.addMenuItem && editor.addMenuItem('video', {
	            label: editor.lang.video.videoProperties,
	            command: 'videoDialog',
	            group: 'image'
	        });
	        
	        editor.contextMenu && editor.contextMenu.addListener(function(element, selection) {        	   	
		        if(getSelectedVideo(editor, element)){
		        	// Remove the image option
		        	for(var i = 0, len = editor.contextMenu.items.length; i < len; ++i){
		        		var item = editor.contextMenu.items[i];
		        		if(item.command && item.command === 'image'){
		        			editor.contextMenu.items.splice(i, 1);
		        			break;
		        		}
		        	}
		        	   
		        	return {
	        			video: CKEDITOR.TRISTATE_OFF			
	    			};
		        }
	        });
		}	
	});
	
	function addDialog(){
		CKEDITOR.dialog.add('videoDialog', function(editor) {
			return {
		        title: editor.lang.video.videoProperties,
		        minWidth: 400,
		        minHeight: 60,
		        contents: [
		            {
		                id: 'tab1',
		                label: editor.lang.video.videoInformation,
		                elements: [
		                    {
		                        type: 'text',
		                        id: 'videoUrl',
		                        label: editor.lang.video.videoUrl,
		                        validate: function(){
		                        	var url = this.getValue(),
		                        		providers = Drupal.settings.ckeditorVideo.providers,
		                        		data;
		                        	
		                        	for(var i = 0, len = providers.length; i < len; ++i) {
		                        		data = providers[i].tryProcessURL(url);
		                        		if(data){
		                        			this.newData = CKEDITOR.tools.extend({
		                        				url: url,
		                        				type: providers[i].type
		                        			}, data);
		                        			return true;
		                        		}
		                         	}
		                         	                        	
		                        	return editor.lang.video.invalidUrl;
		                        },
		                        setup: function(data) {
		                            this.setValue(data.url);
		                        },
		                        commit: function(data) {
		                        	CKEDITOR.tools.extend(data, this.newData);
		                        }
		                    }
		                ]
		            }
		        ],
		
		        onShow: function() {
		            var element = getSelectedVideo(editor);                         
		            if(element){         	
		                this.setupContent(JSON.parse(element.getAttribute('data-cke-video')));
		                this.video = element;
		            }
		        },
		
		        onOk: function() {
		        	var newData = {},
		        		strData;
		        		
		        	this.commitContent(newData);
		        	strData = JSON.stringify(newData);
		        	      	
		        	if(this.video){
		        		this.video.setAttribute('src', newData.placeholder || emptyPlaceholder);
		            	this.video.setAttribute('data-cke-saved-src', newData.placeholder || emptyPlaceholder);
		            	this.video.setAttribute('data-cke-video', strData);
		        	} else {
		        		editor.insertHtml(createPlaceholder(newData.placeholder || emptyPlaceholder, strData));
		        	}    
		        }
		    };
		});	
	}	
})(jQuery);