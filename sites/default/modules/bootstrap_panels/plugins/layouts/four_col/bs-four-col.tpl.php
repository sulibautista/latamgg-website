<div class="row <?php print $classes ?>" <?php if (!empty($css_id)) { print "id=\"$css_id\""; } ?>>
  <?php print $content['left']; ?>
  <?php print $content['center_left']; ?>
  <?php print $content['center_right']; ?>
  <?php print $content['right']; ?>
</div>
