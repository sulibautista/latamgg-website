<?php

/**
 * Implements hook_panels_layouts().
 */
// Plugin definition
$plugin = array(
  'title' => t('Four Columns'),
  'category' => t('Bootstrap'),
  'icon' => 'four_col.png',
  'theme' => 'bs_four_col',
  'admin css' => '../bootstrap-panels-admin.css',
  'regions' => array(
    'left' => t('Left'),
    'center_left' => t('Center Left'),
    'center_right' => t('Center Right'),
    'right' => t('Right')
  ),
);
