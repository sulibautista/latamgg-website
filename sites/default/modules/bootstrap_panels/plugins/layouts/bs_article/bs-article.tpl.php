<div class="row <?php print $classes . (!empty($css_id)? ' ' . $css_id : ''); ?>" <?php if (!empty($css_id)) { print "id=\"$css_id\""; } ?>>
  <article class="col-xxs-12 col-sm-9 col-sm-push-3">
    <div class="row">
      <?php print $content['content_top']; ?>
    </div>
    <?php print $content['content_bottom']; ?>
  </article>
  <aside class="col-xxs-12 col-xs-6 col-sm-3 col-sm-pull-9">
    <div class="maybe-stick">
      <?php print $content['side']; ?>
    </div>
  </aside>
</div>
