<?php

/**
 * Implements hook_panels_layouts().
 */
// Plugin definition
$plugin = array(
  'title' => t('Article'),
  'category' => t('LatamGG'),
  'icon' => 'bs_article.png',
  'theme' => 'bs_article',
  'admin css' => '../bootstrap-panels-admin.css',
  'regions' => array(
    'content_top' => t('Content Top'),
    'content_bottom' => t('Content Bottom'),
    'side' => t('Side')
  ),
);
