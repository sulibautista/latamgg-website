<?php

/**
 * Implements hook_panels_layouts().
 */
// Plugin definition
$plugin = array(
  'title' => t('Two Columns Sandwich'),
  'category' => t('Bootstrap'),
  'icon' => 'two_col_sandwich.png',
  'theme' => 'bs_two_col_sandwich',
  'admin css' => '../bootstrap-panels-admin.css',
  'regions' => array(
    'top' => t('Top'),
    'left' => t('Left side'),
    'right' => t('Right side'),
    'bottom' => t('Bottom')
  ),
);
