<?php

/**
 * Implements hook_panels_layouts().
 */
// Plugin definition
$plugin = array(
  'title' => t('Two Columns'),
  'category' => t('Bootstrap'),
  'icon' => 'two_col.png',
  'theme' => 'bootstrap_panels_two_col',
  'admin css' => '../bootstrap-panels-admin.css',
  'regions' => array(
    'left' => t('Left side'),
    'right' => t('Right side')
  ),
);
