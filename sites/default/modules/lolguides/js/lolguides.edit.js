// Remove empty hash from URL
if (location.hash === '' && window.history && window.history.replaceState){ 
	window.history.replaceState('', '', window.location.pathname);
}

if(!window.CKEDITOR){
	var CKEDITOR;
}

(function($){$(document).ready(function(){
	"use strict";
	
	var championData,
		dataLoaded = false,
		pendingUpdate = false,		
		$championInput = $('#edit-field-lolguide-champion-und'),	
		$form = $championInput.closest('form'), 
		$skinInput = $('#edit-field-lolguide-background-image-und'),	
		skinOptionCache = {},
		abilitiesField = selfSite.abilityField['edit-field-lolguide-abilities'],
		$toc = $('#lolguide-toc').find('ol'),
		$backgroundCont = $('#lolguide-bg-wrapper'),
		$refresh = $('#lolguides-refresh-on-back');	
					
	selfSite.loldata.load({champion: true, ability: true}).then(function(data){
		championData = data.champion;
		dataLoaded = true;
		
		if(pendingUpdate) {
			onChampionInputChange(pendingUpdate);
		}		
	}, function(xhr, err){
		console.log(err);
	});
	
	// Update abilities & available background images(skins) whenever the champion changes
	$championInput.change(function(){
		var championId = parseInt($championInput.val(), 10);
		
		if(dataLoaded){
			onChampionInputChange(championId);				
		} else {				
			pendingUpdate = championId;
			$skinInput.prop('disabled', true);
		}		
	});
	
	// Sets the form background to the selected skin whenever the skin option changes
	$skinInput.change(function(){		
		$backgroundCont.css('background-image', 'url(' + $skinInput.find(':selected').data('splash-url') + ')');
	});
			
	// Sections
	var $sections = $('#edit-field-lolguide-section'),	
		// Cache the fixed part of the ToC	
		$fixedSections = $('form > div > .lolguide-user-section-id-holder'),
		$fixedSectionsToC =	$fixedSections.map(makeToCItem),
		$editorStates = $('#edit-field-lolguide-form-status').find('input'); 	
	
	// Update section & toc title whenever the section input changes
	$sections.on('keyup input', '.field-name-field-lolguide-section-title input', function(){
		var $this = $(this),
			$label = $this.data('label');
		
		if(!$label) {
			var $cont = $this.closest('.lolguide-user-section'),
				$sect = $cont.find('.lolguide-user-section-id-holder');
				
			$label = $cont.find('.fieldset-title');
			
			$this.data('label', $label);
			$this.data('section', $sect);
		}
		
		$label.add($this.data('section').data('tocItem')).text($.trim($this.val()) || 'Sección nueva').end();
	});
		
	// Autogrow section on uncollapse
	$fixedSections.on('collapsed', 'fieldset', editorCollapseToggle);
	$sections.on('collapsed', 'fieldset', editorCollapseToggle);
	
	// Enabled up/down sorting buttons
	$sections.on('click', '.lolguide-section-actions .up, .lolguide-section-actions .down', function(){
		var $this = $(this);
		moveSection($this.closest('.lolguide-user-section'), $this.hasClass('up')? 'up':'down');
	});
	
	// Deletes the section by hiding it, fully empty field collection items are ignored by the server
	$sections.on('click', '.lolguide-section-actions .delete', function(){
		var $section = $(this).closest('.lolguide-user-section');
		$section.find('input').val('');
		Drupal.detachBehaviors($section[0], Drupal.settings);
		$section.find('textarea').val('');
		$section.slideUp(350, updateSectionDOM);
		
		// This fixes some bugs that occur when the user presses the back button after deleting and saving
		// a guide section.
		$refresh.val(1);
	});		
	
	// Enable manual sorting of sections
	var $sortableSections = $sections.find('.lolguide-sortable-wrapper').sortable({
		axis: 'y',
		handle: 'legend',
		delay: 150,
		items: '> .lolguide-user-section',		
		opacity: .7,
		//revert: true,
		tolerance: 'pointer',
		containment: 'parent',
		helper: function(event, $item){
			var $helper = $('<fieldset class="collapsed panel panel-default">' +
				'<legend class="panel-heading"><span class="panel-title fieldset-legend">' +
					'<a class="fieldset-title">' +  getFieldsetTitleText($item.find('.fieldset-title')) + '</a>' +
				'</span></legend></fieldset>');
			
			$helper.height($item.find('fieldset.collapsible > legend').outerHeight());
			
			return $helper;
		} ,
		
		start: function(event, ui){
			ui.placeholder.height(ui.helper.outerHeight() + 20);						
			Drupal.detachBehaviors(ui.item[0], Drupal.settings, 'move');
			
			// Fixes some problems with height not being detected correctly by the plugin			
			setTimeout(function(){
				$sortableSections.sortable('refreshPositions');
			}, 10);	
		},
		
		stop: function(event, ui){			
			Drupal.attachBehaviors(ui.item[0], Drupal.settings);
		},
		
		update: function(event, ui){
			updateSectionDOM();     
		},
	});
	
	// Listen for new sections
	Drupal.behaviors.LoLGuidesSections = {
		attach: function(context) {
			var $wrapper = $(context);
			if($wrapper.attr('id') == 'lolguide-newsections'){
				// An empty div is being added for no reason
				$wrapper.find('#lolguide-nextsection-wrapper').unwrap().empty();
				updateSectionDOM();
			}
	 	}
	};	

	// Restorre previosly saved page scroll. Wait a little bit so ckeditor instances get proper height
	setTimeout(function(){
		var savedPageScroll = sessionStorage.getItem(savedPageScrollKey);
		if(savedPageScroll !== null){
			$('html, body').scrollTop(savedPageScroll);
			sessionStorage.removeItem(savedPageScrollKey);
		}
	}, 500);
	
	// Form operations
	var ignoreFormChanges = false,
		savedPageScrollKey = 'selfSite.lolguides.pageY';
	
	// On cancel, we discard data just by reloading, make sure we save the scroll position. Cancel is not
	// a submit button, so we need to set ignoreFormChanges to true
	$form.find('.lolguide-op-cancel').click(function(){
		ignoreFormChanges = true;
		saveScrollPosition();
		location.reload();
		return false;
	});
	
	// Our custom submit handler will take care of the saving and redirecting, we still need to save the scroll
	// positions.
	$form.find('.lolguide-op-save').click(function(){
		saveScrollPosition();
	});
				
	$form.submit(function(){
		ignoreFormChanges = true;
		
		// Save editors state
		var data = allSections().map(function(){
			var $fieldset = $(this).find('fieldset.collapsible'),
				collapsed = $fieldset.hasClass('collapsed'),
				height = collapsed && $fieldset.data('last-height')?
					$fieldset.data('last-height') :
					CKEDITOR && CKEDITOR.instances[$fieldset.find('textarea').attr('id')].ui.space('contents').getStyle('height');
			
			return {
				collapsed: collapsed,
				height: height				
			};
		}).get();
		
		$editorStates.val(JSON.stringify(data));
	});
		
	// Promt the user before leaving
	window.onbeforeunload = function(){ 
		if(!ignoreFormChanges && !$refresh.val()){
			return 'Si realizaste cambios a tu guía, asegúrate de guardarlos antes de salir.';
		}
	};
	
	// Find initial empty sections left over deletion
	var oneEmptySectionFound = false;
	$sections.find('.lolguide-user-section').each(function(){
		var $section = $(this);
		if($section.find('input[type="text"]').val() == '' && $section.find('textarea').val() == ''){
			if(oneEmptySectionFound){
				setTimeout(function(){ Drupal.detachBehaviors($section[0], Drupal.settings); }, 5000);
				$section.hide();
			} else {
				oneEmptySectionFound = true;
			}
		}
	});
	
	// Execute initial operations
	updateSectionDOM();	
	$sections.find('.field-name-field-lolguide-section-title input').trigger('keyup');

	// Restore editor states
	var $allSections = allSections(),
		statesValStr = $editorStates.val(),
		states = statesValStr && JSON.parse(statesValStr) || [],
		pendingEditors = {};
	
	for(var i = 0, len = states.length; i < len; ++i){
		var $fieldset = $allSections.eq(i).find('fieldset.collapsible');
		$fieldset.data('last-height', states[i].height);
		if(!states[i].collapsed && $fieldset.hasClass('collapsed')){
			$fieldset.removeClass('collapsed')
      			.trigger({ type: 'collapsed', value: false })
      			.show();
		}
		
		pendingEditors[$allSections.eq(i).find('textarea').attr('id')] = states[i];
	}
	
	CKEDITOR && CKEDITOR.on('instanceReady', function(e){
		var editor = e.editor;		
		if(pendingEditors[editor.name]){
			var height = pendingEditors[editor.name].height;		
			editor.resize('100%', height, true);		
			delete pendingEditors[editor.name];
		}
	});
		
	// Mantains correct skins for the selected champion & updates ability field
	function onChampionInputChange(championId) {
		$skinInput.prop('disabled', true);			
		$skinInput.empty();
		
		if(championId){
			if(skinOptionCache[championId]){
				 appendSkinOptions(skinOptionCache[championId]);
			} else {				
				$.get(Drupal.settings.basePath + 'lolguides/js/championSkinOptions/' + championId)
				.then(function(options){
					skinOptionCache[championId] = options;
					appendSkinOptions(options);					
				}, function(xhr, err){
					console.log(err);
				});
			}					
		}
			
		// update champion abilities & points
		abilitiesField.setChampion(championId? championData[championId].championName : '');	
		
		selfSite.loldata.view(championData[championId].abilityIds, 'icon_label').done(function(abilities){
			abilitiesField.setAbilities(abilities);
		});
	}
	
	function appendSkinOptions(skin) {
		$skinInput.append(skin);
		$skinInput[0].selectedIndex = 0;
		$skinInput.trigger('change');
		$skinInput.prop('disabled', false);	
	}
		
	function editorCollapseToggle(e){		
		var $this = $(this),
			editor = CKEDITOR && CKEDITOR.instances[$this.find('textarea').attr('id')];
			
		if(!e.value){ // Not collapsing					
			// The editor width sometimes is set to 0 in chrome	
			editor && editor.document && $(editor.document.getWindow().$.frameElement).width('100%');
			$this.queue(function(next){
				setTimeout(function(){					
					editor && editor.focus();
					next();
				}, 1);				
			});			
		} else { // Collapsing
			$this.data('last-height', editor && editor.ui.space('contents').getStyle('height'));
		}
	};	
		
	function moveSection($section, where) {		
		if(where == 'up'){	
			var $other = $section.prevAll('.lolguide-user-section').filter(':visible').first();
			animateMoveSection($section, $other, $section.height(), -$other.height(), function(){
				$section.insertBefore($other);
			});			
		} else {
			var $other = $section.nextAll('.lolguide-user-section').filter(':visible').first();
			animateMoveSection($section, $other, -$section.height(), $other.height(), function(){ 
				$section.insertAfter($other);	
			});
		}			
	}
	
	function animateMoveSection($section, $other, $otherOffset, $sectionOffset, insert){
		var $parent = $section.parent(),
			$editor = $section.find('.field-name-field-lolguide-section-body');
		
		$section.css({
			height: $section.height(),
			zIndex: 3
		});	
		
		$other.css('zIndex', 2);
		
		$editor.slideUp(450);	
		
		// Disable section actions while animating
		var $actions = $sections.find('.lolguide-section-actions > button').prop('disabled', true);
		$sortableSections.sortable('option', 'disabled', true);
	
		$other.animate({ top: $otherOffset }, 450);			
		$section.animate({ top: $sectionOffset }, {
			duration: 450,
			complete: function(){				
				Drupal.detachBehaviors($section[0], Drupal.settings, 'move');	
				insert();
				$section.css('top', '');
				$other.css('top', '');								
				Drupal.attachBehaviors($section[0], Drupal.settings);	
				
				CKEDITOR && CKEDITOR.instances[$section.find('textarea').attr('id')].once('afterSetData', function(){
					$editor.slideDown(200);	
					setTimeout(function(){ $section.css('height', ''); }, 800);			
				});
				
				$actions.prop('disabled', false);
				$sortableSections.sortable('option', 'disabled', false);
				
				updateSectionDOM();		
			}
		});
	}
		
	// Mantains invariants after dom changes to the user defined section
	function updateSectionDOM() {
		updateToC();
		updateSectionWeights();
		updateSectionActions();
	}
	
	// Regenerates the ToC
	function updateToC() {
		$toc.empty()
		.append($fixedSectionsToC.clone(true))
		.append($sections.find('.lolguide-user-section').filter(':visible').find('> .lolguide-user-section-id-holder')
			.map(makeToCItem));
	}
		
	// Generates a ToC item from a section
	function makeToCItem(){
		var $this = $(this),
			$item = $('<li><a href="#' + this.id + '">' + getFieldsetTitleText($this.find('.fieldset-title')) + '</a></li>');

		$item.data('section', $this);
		$this.data('tocItem', $item.find('a'));
		
		return $item[0];
	}
	
	function getFieldsetTitleText($fieldsetTitle){
		return $.trim($fieldsetTitle.contents().filter(function(){ return this.nodeType === 3; }).text());
	}
	
	// Set correct field weights based on DOM position
	function updateSectionWeights(){
		var $allSections = $sections.find('.lolguide-user-section'),
			$selects = $allSections.find('.lolguide-section-weight'),
			count = Math.floor(($allSections.filter(':visible').length) / 2),
			i = 0;
			
		$selects.each(function(){
			var $this = $(this),
				value = $this.closest('.lolguide-user-section').is(':visible')? -count+(i++) : count+1;
			
			if(!$this.find('[value="' + value + '"]').length){
				$('<option value="' + value + '">' + value + '</option>').prependTo($this);
			}
			
			$this.val(value);
		});
	}
	
	// Toggles up/down button
	function updateSectionActions(){
		$sections.find('.lolguide-section-actions').each(function(){
			var $this = $(this),
				$section = $this.closest('.lolguide-user-section');
				
			$this.find('.up').prop('disabled', $section.prevAll('.lolguide-user-section').filter(':visible').length == 0);
			$this.find('.down').prop('disabled', $section.nextAll('.lolguide-user-section').filter(':visible').length == 0);
		});	
	}
	
	function allSections() {
		return $fixedSections.add($sections.find('.lolguide-user-section').filter(':visible'));
	}
	
	function saveScrollPosition() {
		try{
			sessionStorage.setItem(savedPageScrollKey, $(document).scrollTop());
		} catch(e){
			console.log(e);
		}
	}
	
});})(jQuery);