<?php

/**
 * Implements hook_views_api().
 */
function latamgg_views_api() {
  return array(
    'api' => 3
  );
}

/**
 * Implements hook_menu().
 */
function latamgg_menu() {
  $items['latamgg403'] = array(
    'page callback' => 'latamgg_page_access_denied',
    'access callback' => TRUE,
  );

  $items['latamgg404'] = array(
    'page callback' => 'latamgg_page_not_found',
    'access callback' => TRUE,
  );

  return $items;
}

function latamgg_page_access_denied() {
  if (user_is_anonymous()) {
    if (empty($_POST)) {
      drupal_set_message('Acceso denegado. Inicia sesión para ver esta página.', 'error');
    }
    // Handle redirection to the login form.
    // using drupal_goto() with destination set causes a recursive redirect loop
    $login_path = 'user/login';
    $code = 302;
    // The code in drupal_get_destination() doesn't preserve any query string
    // on 403 pages, so reproduce the part we want here.
    $path = $_GET['destination'];
    $query = drupal_http_build_query(drupal_get_query_parameters(NULL, array('q', 'destination')));
    if ($query != '') {
      $path .= '?' . $query;
    }
    $destination = array('destination' => $path);
    header('Location: ' . url($login_path, array('query' => $destination, 'absolute' => TRUE)), TRUE, $code);
    drupal_exit();
  } else {
    // Check to see if we are to redirect the user.
    $redirect = '';
    if (empty($redirect)) {
      // Display the default access denied page.
      drupal_set_title(t('Access denied'));
      return '<p>' . t('You are not authorized to access this page.') . '</p>';
    } else {
      // Custom access denied page for logged in users.
      header('Location: ' . url($redirect, array('absolute' => TRUE)));
      drupal_exit();
    }
  }
}

function latamgg_page_not_found() {
  $keys = latamgg_get_url_keys();

  if($keys) {
    drupal_set_message('¡Lo sentimos! No hemos encontrado la página que buscabas, en su lugar te mostramos algunas ' .
      'páginas similares a la que intentabas acceder.', 'warning');
    $_GET['query'] = $keys;
  } else {
    drupal_set_message('¡Lo sentimos! No hemos encontrado la página que buscabas, intenta usando el buscador debajo.', 'error');
  }

  module_load_include("inc", "page_manager", "plugins/tasks/page");
  return page_manager_page_execute('site_search');
}

function latamgg_get_url_keys() {
  // Try to get keywords from the search result (if it was one)
  // that resulted in the 404 if the config is set.
  $keys = latamgg_search_engine_query();

  // If keys are not yet populated from a search engine referer
  // use keys from the path that resulted in the 404.
  if (!$keys) {
    $keys = $_GET['destination'];
  }

  // Abort query on certain extensions, e.g: gif jpg jpeg png
  $extensions = explode(' ', 'gif jpg jpeg bmp png');
  $extensions = trim(implode('|', $extensions));
  if (!empty($extensions) && preg_match("/\.($extensions)$/i", $keys)) {
    return '';
  }

  $regex_filter = '';
  if (!empty($regex_filter)) {
    $keys = preg_replace("/" . $regex_filter . "/i", '', $keys);
  }

  // Ignore certain extensions from query.
  $extensions = explode(' ', 'htm html php');
  $extensions = trim(implode('|', $extensions));
  if (!empty($extensions)) {
    $keys = preg_replace("/\.($extensions)$/i", '', $keys);
  }

  $keys = preg_split('/[' . PREG_CLASS_UNICODE_WORD_BOUNDARY . ']+/u', $keys);

  // Sanitize the keys
  foreach ($keys as $a => $b) {
    $keys[$a] = check_plain($b);
  }

  return trim(implode(' ', $keys));
}

function latamgg_search_engine_query() {
  $engines = array(
    'altavista' => 'q',
    'aol' => 'query',
    'google' => 'q',
    'bing' => 'q',
    'lycos' => 'query',
    'yahoo' => 'p',
  );
  $parsed_url = parse_url($_SERVER['HTTP_REFERER']);
  $remote_host = !empty($parsed_url['host']) ? $parsed_url['host'] : '';
  $query_string = !empty($parsed_url['query']) ? $parsed_url['query'] : '';
  parse_str($query_string, $query);

  if (!$parsed_url === FALSE && !empty($remote_host) && !empty($query_string) && count($query)) {
    foreach ($engines as $host => $key) {
      if (strpos($remote_host, $host) !== FALSE && array_key_exists($key, $query)) {
        return trim($query[$key]);
      }
    }
  }

  return '';
}

/**
 * Implements hook_entity_info_alter().
 */
function latamgg_entity_info_alter(&$entity_info) {
  // Adds custom view mode `thumbnail` for nodes, used in places where teaser would be too big.
  $entity_info['node']['view modes']['thumbnail'] = array(
    'label' => t('Thumbnail'),
    'custom settings' => TRUE
  );

  // View mode used for search results (not yet, but we declare it anyway)
  $entity_info['node']['view modes']['search_api_result'] = array(
    'label' => t('Search API result'),
    'custom settings' => TRUE
  );

  // Disable rendering of redirect widget on scald_atoms
  $entity_info['scald_atom']['redirect'] = FALSE;
}

// Currently we dont use this, as we embed a form directing to mailchimp singup page
///**
// * Modify the `newsletter_subscribe` singup block:
// *  * Hide list selection, as we use our only list (LatamGG Newsletter)
// *  * Hide the group selection and select all of them (this until we get a better UX to select them)
// * @param $form
// */
//function latamgg_form_mailchimp_signup_subscribe_block_newsletter_subscribe_form_alter(&$form) {
//  $singup = mailchimp_signup_load('newsletter_subscribe');
//  $lists = mailchimp_get_lists($singup->mc_lists);
//
//  if(!empty($lists)){
//    $list = reset($lists);
//    $wrapper_key = 'mailchimp_' . $list['web_id'];
//    $list_elem = &$form['mailchimp_lists'][$wrapper_key];
//
//    $list_elem['subscribe'] = array(
//      '#type' => 'value',
//      '#value' => 1
//    );
//
//    if (!empty($list_elem['interest_groups'])) {
//      $fixed_groups = array();
//      foreach (element_children($list_elem['interest_groups']) as $group_id) {
//        $fixed_groups[$group_id] = array(
//          '#type' => 'value',
//          '#value' => $list_elem['interest_groups'][$group_id]['#options']
//        );
//      }
//      $list_elem['interest_groups'] = $fixed_groups;
//      // Temporary fixes #2394399
//      $form['mailchimp_lists']['interest_groups'] = $fixed_groups;
//    }
//  }
//}

function latamgg_search_api_autocomplete_types() {
  $types['latamgg'] = array(
    'name' => 'Búsquedas de LatamGG',
    'description' => 'Búsquedas del modulo <em>LatamGG</em>.',
    'list searches' => 'latamgg_list_autocomplete_searches',
    'create query' => 'latamgg_create_autocomplete_query',
  );
  return $types;
}

/**
 * Returns a list of searches for the given index.
 *
 * All searches returned must have a unique and well-defined machine name. The
 * implementing module for this type is responsible for being able to map a
 * specific search always to the same distinct machine name.
 * Since the machine names have to be globally unique, they should be prefixed
 * with the search type / module name.
 *
 * Also, name and machine name have to respect the length constraints from
 * search_api_autocomplete_schema().
 *
 * @param SearchApiIndex $index
 *   The index whose searches should be returned.
 *
 * @return array
 *   An array of searches, keyed by their machine name. The values are arrays
 *   with the following keys:
 *   - name: A human-readable name for this search.
 *   - options: (optional) An array of options to use for this search.
 *     Type-specific options should go into the "custom" nested key in these
 *     options.
 */
function latamgg_list_autocomplete_searches(SearchApiIndex $index) {
  switch ($index->machine_name) {
    case 'default_node_index':
      return [
        'latamgg_search_inline' => ['name' => 'Búsqueda rápida del Sitio']
      ];
  }
  return [];
}

/**
 * Create the query that would be issued for the given search for the complete keys.
 *
 * @param SearchApiAutocompleteSearch $search
 *   The search for which to create the query.
 * @param $complete
 *   A string containing the complete search keys.
 * @param $incomplete
 *   A string containing the incomplete last search key.
 *
 * @return SearchApiQueryInterface
 *   The query that would normally be executed when only $complete was entered
 *   as the search keys for the given search.
 *
 * @throws SearchApiException
 *   If the query couldn't be created.
 */
function latamgg_create_autocomplete_query(SearchApiAutocompleteSearch $search, $complete, $incomplete) {
  $query = search_api_query($search->index_id);
  if ($complete) {
    $query->keys($complete);
  }
  if (!empty($search->options['custom']['extra'])) {
    list($f, $v) = explode('=', $search->options['custom']['extra'], 2);
    $query->condition($f, $v);
  }
  if (!empty($search->options['custom']['user_filters'])) {
    foreach (explode("\n", $search->options['custom']['user_filters']) as $line) {
      list($f, $v) = explode('=', $line, 2);
      $query->condition($f, $v);
    }
  }
  return $query;
}

/**
 * Inline site-wide search form.
 */
function latamgg_search_inline_form($form, &$form_state) {
  $form['#action'] = url('buscar');
  $form['#method'] = 'get';
  $form['#token'] = FALSE;
  $form['#attributes']['class'] = ['search-inline'];

  $form['#after_build'][] = '_latamgg_search_inline_form_after_build';

  $form['query'] = [
    '#type' => 'searchfield',
    '#title' => 'Buscar en LatamGG',
    '#title_display' => 'invisible',
    '#placeholder' => 'Buscar',
    '#attributes' => ['class' => ['search-inline-input']],
    // If we dont override the default, it clashes with other inputs named "query" (in particular the one
    // in the site-wide search page) rendering them unusable for autocomplete, it appears to be
    // a form API bug, or maybe it has to do with caching, whatever the cause, this fixes it.
    '#id' => 'lgg-search-inline-query'
  ];

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Go')
  );

  $search = search_api_autocomplete_search_load('latamgg_search_inline');
  if (!empty($search->enabled)) {
    $search->alterElement($form['query']);
  }

  return $form;
}

/**
 * Remove form_id and form_build_id since these are not needed, and if not removed they:
 *  1) clutter the get request
 *  2) Make the in-page search input have an empty value after submit
 */
function _latamgg_search_inline_form_after_build(&$form) {
  $form['form_build_id']['#access'] = FALSE;
  $form['form_id']['#access'] = FALSE;
  unset($form['submit']['#name']);
  return $form;
}

function latamgg_search_api_solr_query_alter(&$call_args, SearchApiQuery $query) {
  $params = &$call_args['params'];

  switch($query->getOption('search id')) {
    case 'search_api_autocomplete:latamgg_search_inline':
    case 'search_api_autocomplete:search_api_views_site_search':
      //$params['spellcheck.count'] = 5;
      //$params['spellcheck.onlyMorePopular'] = 'true';
      break;

    case 'search_api_views:site_search:panel_pane_1':
      if ($query->getOption('search_api_spellcheck') && !$query->getOption('latamgg_query_altered')) {
        $params['spellcheck.accuracy'] = 0.7;
        $params['spellcheck.collate'] = 'true'; //
        $params['spellcheck.collateExtendedResults'] = 'true';
        $params['spellcheck.onlyMorePopular'] = 'false';
        $params['spellcheck.maxCollationTries'] = 10;
        $query->setOption('latamgg_query_altered', TRUE);
      }
      break;
  }
}

function latamgg_search_api_solr_search_results_alter(&$results, SearchApiQueryInterface $query, $response) {
  switch($query->getOption('search id')) {
    case 'search_api_views:site_search:panel_pane_1':
      // If the user query returned 0 results, try the collated one. See latamgg_views_pre_render for the user
      // reporting.
      if ($results['result count'] == 0 && !empty($response->spellcheck->suggestions->collation->collationQuery)
          && !$query->getOption('latamgg_results_altered')) {
        $query->setOption('latamgg_results_altered', TRUE);
        $query->keys($response->spellcheck->suggestions->collation->collationQuery);
        $results = $query->execute();
        $query->setOption('latamgg_corrections', (array)$response->spellcheck->suggestions->collation->misspellingsAndCorrections);
      }
      break;
  }
}