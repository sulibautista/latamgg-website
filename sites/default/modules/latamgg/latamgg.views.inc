<?php

//
//function latamgg_views_pre_execute(&$view) {
//  switch($view->name){
//  }
//}
//
//
//function latamgg_views_post_execute(&$view) {
//
//}

/**
 * Implementation of hook_views_pre_render()
 */
function latamgg_views_pre_render(&$view) {
  switch($view->name){
    case 'site_search':
      if ($view->result) {
        $count = _latamgg_view_result_count($view);
        $view->attachment_before = sprintf('<p class="search-result-count">%s</p>', $count);

        $opts = $view->query->getOptions();
        if (!empty($opts['latamgg_results_altered'])) {
          $corrections = $opts['latamgg_corrections'];
          $corrected = preg_replace(
            // Only replace full words
            array_map(function($c) { return '/\b'. preg_quote($c) .'\b/u'; }, array_keys($corrections)),
            // With their corrected counter part
            array_values($corrections),
            // In the user search query
            $view->get_exposed_input()['query']
          );
          $view->attachment_before .=
            sprintf('<p class="search-corrections"><span>%s <b>%s</b></span><br><span>%s <b>%s</b></span></p>',
              'Se muestran resultados de',
              l($corrected, 'buscar', ['query' => ['query' => $corrected]]),
              'No se han encontrado resultados para',
              check_plain($view->get_exposed_input()['query'])
            );
        }
      }
      break;
  }
}

/**
 * Implements hook_views_post_render().
 */
function latamgg_views_post_render(&$view) {
  switch($view->name){
    case 'site_search':
      // Somehow this is the only hook where setting the title works 100% (both on normal exec and validation errors)
      $input = $view->get_exposed_input();
      $title = !empty($input['query'])?
        sprintf('Resultados para "%s"', check_plain($input['query'])) : 'Resultados de Búsqueda';
      $view->set_title($title);
      break;
  }
}

function _latamgg_view_result_count($view) {
  $pager = $view->query->pager;
  $total = $pager->total_items;
  $items_per_page = $pager->options['items_per_page'];

  if ($total > $items_per_page && $pager->plugin_name != 'load_more') {
    $current_page = $pager->current_page;
    $start = ($items_per_page * $current_page) + 1;
    $end = min($items_per_page * ($current_page+1), $total);

    return format_string('Viendo !start - !end de !total resultados', [
      '!start' => $start,
      '!end' => $end,
      '!total' => $total
    ]);
  }

  return format_string('Viendo !total !results_label', [
    '!total' => $total,
    '!results_label' => $total == 1? 'resultado' : 'resultados'
  ]);
}