(function($) {

	$.notify = function() {
		var config = $.notify.config('default') || {};

		var defaults = {
			delay : 3000,
			content : '',
			css : {
				"background" : "#FFF",
				"color" : "#222",
				"display" : "none",
				"font" : "22px/50px 'Trebuchet Ms'",
				"left" : "25%",
				"letter-spacing" : "-1px",
				"opacity" : "0",
				"position" : "fixed",
				"text-align" : "center",
				"top" : "130px",
				"width" : "50%",
				"padding" : "0 10px",
				"z-index" : "100000",
				"border-radius" : "6px",
				"box-shadow" : "2px 2px 2px #000",
				"text-shadow" : "1px 1px 1px rgba(0, 0, 0, 0.6)"
			}

		};

		if ( typeof arguments[0] == 'string') {

			if ( typeof arguments[1] == 'string') {

				config = $.notify.config(arguments[1]) || {};

				if ( typeof arguments[2] == 'object') {

					config = $.extend(true, config, arguments[2]);

				}

			} else if ( typeof arguments[1] == 'object') {

				config = arguments[1];

			}

			config.content = arguments[0];

		} else if ( typeof arguments[0] == 'object') {

			config = arguments[0];

		}

		var options = $.extend(true, defaults, config);

		$('#notified').remove();

		$('body').append('<div id="notified"></div>');

		$('#notified').css(options.css).html(options.content).show().animate({
			opacity : 0.9
		});

		setTimeout(function() {

			$('#notified').animate({
				opacity : 0
			}, function() {
				$(this).css('display', 'none');
			});

		}, options.delay);

	};

	$.notify.configs = {};
	$.notify.config = function() {

		if (arguments[1]) {
			$.notify.configs[arguments[0]] = arguments[1];
			return true;
		}

		return $.notify.configs[arguments[0]];

	};
})(jQuery); 

jQuery.notify.config('error', { delay: 4000, css: { background: '#AA0000', color: '#FFF', textShadow: '1px 1px 1px rgba(0, 0, 0, 0.8)' } });
jQuery.notify.config('success', { delay: 4000, css: { background: '#528800', color: '#FFF', textShadow: '1px 1px 1px rgba(0, 0, 0, 0.8)' } });