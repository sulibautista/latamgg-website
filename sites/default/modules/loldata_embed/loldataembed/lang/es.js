CKEDITOR.plugins.setLang( 'loldataembed', 'es', {
	passiveAbilityIndex: 'pasivo pasiva',
	searchPlaceholder: 'Cualquier cosa..',
	searchTitle: 'Buscar elementos de LoL',
	viewModeLabel: 'Modo de visualización',
	viewModeTitle: 'Modo de visualización de los elementos de LoL',
	viewSizeLabel: 'Tamaño del ícono',
	viewSizeTitle: 'Tamaño del ícono de los elementos de LoL'
});