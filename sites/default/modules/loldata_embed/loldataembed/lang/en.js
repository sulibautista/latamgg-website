CKEDITOR.plugins.setLang( 'loldataembed', 'en', {
	passiveAbilityIndex: 'passive',
	searchPlaceholder: 'Anything..',
	searchTitle: 'Search LoL elements',
	viewModeLabel: 'View mode',
	viewModeTitle: 'LoL\'s elements view mode',
	viewSizeLabel: 'Icon Size',
	viewSizeTitle: 'LoL\'s elements icon size'
});