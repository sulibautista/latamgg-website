(function($){
"use strict";

var pluginRoot = Drupal.settings.basePath + Drupal.settings.loldataembed.path,
	dataLoadingImg = pluginRoot + 'images/loading_inline.gif',
	
	defaultViewOptionTypeKey = 99999999,
	loldata = {99999999: { type:'_default' }},
	viewCache = {},	
	cacheBaseKey = '_ss.lde.' + selfSite.loldata.getUserLang() + '.',
	
	initInstanceCount = 0,
	initInstanceLoadedCount = 0,
	initRequestBuffer = new ViewRequestBuffer(),
	
	index = new selfSite.loldata.SearchIndex('loldataembed', true),
	doIndex = !index.isFromCache,
	
	viewOptionsMap = compileViewOptions(Drupal.settings.loldataembed.viewOptions),
	viewControlValues = {},
	
	linkableData = {
		champion: 1, item: 1, ability: 1, mastery: 1, spell: 1, rune: 1, itemcategories: 1
	},
	
	// TODO remove ui stylesheet and add configurable one
	panelDefinition = {
		css: ['http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css', pluginRoot + 'loldataembed.css']
	},
	
	panelCssOverrides = {
		backgroundColor: 'transparent',
		boxShadow: 'none',
		border: 'none',
		overflow: 'visible',
		minWidth: '140px'
	},
		
	autocompleteConfig = {
		delay: 50,
		autoFocus: true,
		source: function(req, callback){	
			var results = index.search(req.term);
			
			if(results.length > 30) {
				results.length = 30;
			}			
			
			callback(results);
		}
	},
	
	dummyContainer = $('<div>'),
	embeddedViewRegExp = /\[\[loldata:([\d+,]+)\|(\w+)\|?([\s\S]*?)\]\]/gi,	
	
	viewsToEmbeddedOutputRules = {
		elements: {
			img: transformViewToEmbedded
		}	
	},
	
	searchBoxHeight = 0;

selfSite.loldata.load(linkableData).done(function(data){	
	$.each(data, function(dataType, values){	
		if(dataType != 'itemcategories' && dataType in linkableData){
			$.each(values, function(id, object){
				loldata[id] = object;		
				
				if(doIndex){					
					selfSite.loldata.indexObject(index, object);
				}
			});	
		}
	});	
	
	selfSite.loldata.commitIndex(index);
});

function compileViewOptions(data){	
	var	viewOptions = {},
		values = {
			size: prepareViewOptions(data.sizes),
			mode: prepareViewOptions(data.modes)
		},
		
		baseType = processViewType(data.base, data, values);
		
	for(var type in data.types){
		var typeConfig = data.types[type];
		if(typeConfig === true){
			viewOptions[type] = baseType;
		} else if(typeof typeConfig == 'object'){
			viewOptions[type] = processViewType(typeConfig);
		}
	}
	
	return viewOptions;	
	
	function prepareViewOptions(input){
		var data = {};
		for(var key in input){
			data[key] = [key, input[key], input[key]];
		}
		return data;
	}
	
	function processViewType(typeConfig){
		var type = {};
		
		if($.isArray(typeConfig.modes)){
			for(var i = 0, len = typeConfig.modes.length; i < len; ++i){
				type[typeConfig.modes[i]] = generateOptionSet(typeConfig.modes, 'mode');
			}
		}
		
		if($.isArray(typeConfig.sizes)){
			var optionSet = generateOptionSet(typeConfig.sizes, 'size');
			for(var mode in type){
				type[mode].childOptionsMap.size = true;
				type[mode].size = optionSet;				
			}
		} else {
			for(var mode in typeConfig.sizes){
				type[mode].childOptionsMap.size = true;
				type[mode].size = generateOptionSet(typeConfig.sizes[mode] === true? 
					data.base.sizes : typeConfig.sizes[mode], 'size');
			}
		}
		
		return type;
	}
	
	function generateOptionSet(options, optionType){
		var valueMap = {},
			insertValues = [],
			vals = values[optionType];
		
		for(var i = 0, len = options.length; i < len; ++i){
			var opt = options[i];
			valueMap[opt] = true;
			insertValues.push(vals[opt]);
		}
	
		valueMap.length = insertValues.length;
		
		return {
			valueMap: valueMap,
			insertValues: insertValues,
			optionType: optionType,
			childOptionsMap: {}
		};
	}
};

function ViewRequestBuffer(){
	var requestCacheSet = {},
		requestCache = [],
		requestKeyCache = [],
		requestHandlers = [],
		
		execRequestHandlers = function(){
			for(var i = 0, len = requestHandlers.length; i < len; ++i){
				requestHandlers[i]();
			}
		};
			
	$.extend(this, {
		enqueue: function(editor, placeholderId, options){			
			var optionsKey = createOptionsKey(options);
			
			if(!inViewCache(optionsKey) && !requestCacheSet[optionsKey]){
				requestCache.push(options);
				requestKeyCache.push(optionsKey);
				requestCacheSet[optionsKey] = true;
			}
				
			requestHandlers.push(function(){		
				renderView(editor, viewCache[optionsKey], placeholderId);
			});
		},
		
		execute: function(){
			if(requestCache.length == 0){
				execRequestHandlers();
			} else {				
				selfSite.loldata.viewAll(requestCache).done(function(views){
					var $views = $(views).css({
						'float': 'left',
						clear: 'both',
						marginBottom: 3
					});			
					
					var $el = $('<div class="loldataembed-wrapper">').append($views).css({
						display: 'inline-block',
						position: 'fixed',
						top: 0,
						left:0,
						zIndex: -9999,
						lineHeight: 'normal',
						maxWidth: 720		
					}).appendTo(document.body);
					
					html2canvas($el[0], {
						background: undefined,
						
						onrendered: function(canvas){	
							var newCanvas = document.createElement('canvas'),
								ctx = newCanvas.getContext('2d');
							
							$el.children().each(function(i){
								var $this = $(this),
									pos = $this.position(),
									w = $this.width(),
									h = $this.height();
									
								newCanvas.width = Math.ceil(w);
						        newCanvas.height = Math.floor(h);			        			        
								ctx.clearRect(0, 0, w, h);									
						        ctx.drawImage(canvas, pos.left, pos.top, w, h-1, 0, 0, w, h);			
										
								cacheView(requestKeyCache[i], newCanvas);
							});
							
							$el.remove();
							execRequestHandlers();
						}
					});			
				});
			}
		}
	});
}

function generateViewPlaceholder(options) {
	var placeholderId = nextPlaceholderId();

	return {
		id: placeholderId,
		html: $('<img>')		
			.attr('src', dataLoadingImg)
			.attr('id', placeholderId)
			.attr('data-cke-realelement', true)
			.attr('data-cke-loldataembed', JSON.stringify(options))
			.prop('outerHTML')
	};
}

function nextPlaceholderId() {
	return 'loldataembed-' + CKEDITOR.tools.getNextId();
}

function serializeOptions(options){
	var ret = [];
	for(var key in options){
		if(key != 'mode' && key != 'ids'){
			ret.push(key + ':' + options[key]);
		}
	}
	return ret.join('|');
}

function unserializeOptions(options){
	var ret = {};
	if(options){
		for(var i = 0, ops = options.split('|'), len = ops.length; i < len; ++i){
			var op = ops[i].split(':');
			ret[op[0]] = op[1];
		}
	}
	return ret;
}

function displayView(editor, options, placeholderId){
	var optionsKey = createOptionsKey(options);
	
	if(inViewCache(optionsKey)) {
		return renderView(editor, viewCache[optionsKey], placeholderId);
	}

	selfSite.loldata.view(options.ids, options.mode, options).done(function(view){				
		processView(editor, view, placeholderId, optionsKey);
	});
}

function processView(editor, view, placeholderId, optionsKey) {
	var placeholder = editor.document.getById(placeholderId);
		
	if(placeholder){
		var $view = $('<div class="loldataembed-wrapper">').css({
			display: 'inline-block',
			position: 'fixed',
			top: 0,
			left:'50%',
			zIndex: -9999,
			lineHeight: 'normal'
		}).html(view).appendTo(document.body);
		
		html2canvas($view[0], {
			background: undefined,
			onrendered: function(canvas){
				$view.remove();				
				cacheView(optionsKey, canvas);
				renderView(editor, viewCache[optionsKey], placeholderId);	
			}
		});	
	}
}

function createOptionsKey(options){
	var keys = Object.keys(options).sort(),
		ret = [];
		
	for(var i = 0, len = keys.length; i < len; ++i){
		var key = keys[i];
		ret.push(key + ':' + options[key].toString());
	}
	
	return ret.join('|');
};

function cacheView(optionsKey, canvas){
	var dataUrl = canvas.toDataURLHD?  canvas.toDataURLHD("image/png") : canvas.toDataURL("image/png");
		
	viewCache[optionsKey] = dataUrl;
	
	try{
		var countKey = 'selfSite.loldataembed.viewCount',
			count = parseInt(sessionStorage.getItem(countKey), 10) || 0,
			cacheKey = cacheBaseKey + optionsKey,
			currItem = sessionStorage.getItem(cacheKey),
			itemLen = dataUrl.length * 2;
			
		if(currItem || (count+itemLen < 1024 * 1024 * 2)){ // Don't exced 2MB~
			sessionStorage.setItem(cacheKey, dataUrl);
			sessionStorage.setItem(countKey, itemLen + count);
		}						
	} catch(e){
		console.log(e.message);
	}
}

function inViewCache(optionsKey){
	if(typeof viewCache[optionsKey] != 'undefined'){
		return true;
	}
	
	var dataUrl = sessionStorage.getItem(cacheBaseKey + optionsKey);
	if(dataUrl !== null){
		viewCache[optionsKey] = dataUrl;
		return true;
	}
	
	return false;
}

function renderView(editor, dataUrl, placeholderId) {
	var placeholder = editor.document.getById(placeholderId);
															
	if(placeholder)	{			
		editor.fire('lockSnapshot');
		
		$(placeholder.$).attr({
			'src': dataUrl,
			'data-cke-saved-src': dataUrl
		})
		// IE 
		.removeAttr('width')
		.removeAttr('height')
		.removeAttr('style'); 
		
		editor.fire('unlockSnapshot');
		
		editor.fire('loldataViewRendered');
		editor.execCommand('autogrow');
	}
}

function viewControlInit(control, defaultViewOptions, controlDependency){	
	var viewKeys = getViewOptionDependencyPath(control.dataOption, controlDependency),
		viewOptions = viewOptionsMap,
		addedOptions = {},
		next,
		valueCacheLoaded = !!viewControlValues[control.dataOption];
		
	if(!valueCacheLoaded){
		viewControlValues[control.dataOption] = {};
	}
	
	while(viewKeys.length > 0){        		
		next = [];
		for(var key in viewOptions){
			for(var key2 in viewOptions[key]){
				var option = viewOptions[key][key2];
				if($.isPlainObject(option) && option.optionType){
					next.push(option);	
				}
			}        			
		}
		viewOptions = next;
		viewKeys.pop();
	} 
	
	control.startGroup(control.label);
	
	for(var key in viewOptions){
		var option = viewOptions[key];       
		for(var i = 0, len = option.insertValues.length; i < len; ++i){
			var value = option.insertValues[i];
			if(!addedOptions[value[0]]){            				 				
				control.add.apply(control, value);
				addedOptions[value[0]] = true;
				
				if(!valueCacheLoaded){	    					
					viewControlValues[control.dataOption][value[0]] = value[1];
				}
			}
		}
	}
	
	control.commit();
		
	// Overrides setValue
	control.setValue = setViewControlValue;
	control.setValue(defaultViewOptions[control.dataOption]);
	
	updateAvailableViewOptions(control, defaultViewOptions, controlDependency);
}
		
function setViewControlValue(value){
    var text = value? viewControlValues[this.dataOption][value] : '...'; 
	CKEDITOR.ui.richCombo.prototype.setValue.call(this, value, text);
}

function getViewOptionDependencyPath(option, controlDependency){
	var viewProperties = [];	
		
	do{
		option = controlDependency[option];
		viewProperties.push(option);					
	} while(controlDependency[option]);		
		
	return viewProperties;
}

function updateAvailableViewOptions(control, viewOptions, controlDependency){
	try{
	var items = control._.list._.items,
		doc = control._.list.element.getDocument(),
		valueMap;					
		
	if($.isArray(viewOptions)){
		valueMap = $.extend({}, getViewOptionValueMap(viewOptions[0], control.dataOption, controlDependency));
		for(var i = 1, len = viewOptions.length; i < len; ++i){
			var tempMap = getViewOptionValueMap(viewOptions[i], control.dataOption, controlDependency);
			for(var value in valueMap){
				if(tempMap[value] === undefined){
					delete valueMap[value];
					--valueMap.length;							
				}
			}
		}
	} else {							
		valueMap = getViewOptionValueMap(viewOptions, control.dataOption, controlDependency);
	}
	
	for(var value in items) {
		doc.getById(items[value]).setStyle('display', valueMap[value]? '' : 'none');
	}

	valueMap.length > 0? control.enable() : control.disable();
	}catch(e){
		console.log(e.message);
	}
}

function getViewOptionValueMap(viewOptions, option, controlDependency){
	var valueMap = getViewOptionDefinition(viewOptions, option, controlDependency).valueMap;
	return valueMap || {};
}

function getViewOptionDefinition(viewOptions, option, controlDependency){
	var viewProperties = getViewOptionDependencyPath(option, controlDependency),				
		value = viewOptionsMap;

	while(viewProperties.length > 0){
		var key = viewProperties.pop();
		
		if(key == 'type'){
			value = value[loldata[viewOptions.ids[0]].type];
		} else{
			value = value[viewOptions[key]];
		}			
	}
	
	if(typeof value[option] != 'undefined'){
		return value[option];
	} else if(typeof value[viewOptions[option]] != 'undefined'){
		return value[viewOptions[option]];
	} else {
		return {};
	}
}

function viewControlOnSelectionChange(selection, viewControls, defaultViewOptions, controlDependency){
	var views = getViewsInSelection(selection),
    	firstViewOptions,
    	nextViewOptions;
    
    if(views.length > 0){
    	firstViewOptions = views.first().data('ckeLoldataembed');
    } 
	
	for(var i = 0, len = viewControls.length; i < len; ++i){
		var control = viewControls[i];	
													
		if(views.length == 1){
    		control.setValue(firstViewOptions[control.dataOption]);
    		nextViewOptions = firstViewOptions;
    	
        } else if(views.length > 1){
    		var currOption = firstViewOptions[control.dataOption],
        		dependantViewOptions = [],
        		diffOption = false;
        	
        	for(var k = 0, klen = views.length; k < klen; ++k){
        		var viewOptions = views.eq(k).data('ckeLoldataembed');
        		
        		dependantViewOptions.push(viewOptions);
        		
        		if(!diffOption && viewOptions[control.dataOption] != currOption){
        			diffOption = true;
        		}
        	}
        	
        	control.setValue(diffOption? '' : currOption);
        	nextViewOptions = dependantViewOptions;      	
        	
		} else {
    		control.setValue(defaultViewOptions[control.dataOption]);
			nextViewOptions = defaultViewOptions;
		}
	
		updateAvailableViewOptions(control, nextViewOptions, controlDependency);
   }
}

function getViewsInSelection(selection){
	var ranges = selection.getRanges(),
		views = [],
		walker,
		next;
		
	for(var i = 0, len = ranges.length; i < len; ++i){
		walker = new CKEDITOR.dom.walker(ranges[i]);
		walker.evaluator = CKEDITOR.dom.walker.nodeType(CKEDITOR.NODE_ELEMENT);
		
		while(next = walker.next()){
			var $view = $(next.$);
			if($view.data('ckeLoldataembed')){
				views.push(next.$);
			}
		}
	}
	
	return $(views);      	
}

function viewControlOnOpen(){			
	var panel = $(this._.panel.element.$),
		list = $(this._.list.element.$);
		
	panel.css({
		maxHeight: list.outerHeight() + 5,
		maxWidth: this.panelWidth 
	});
}

function updateViewOptions(editor, views, option, newValue, defaultViewOptions, controlDependency) {
	var buffer = new ViewRequestBuffer();
	
	editor.focus();
    editor.fire('saveSnapshot');
           			
	for(var i = 0, len = views.length; i < len; ++i){
		var view = views.eq(i),
			options = view.data('ckeLoldataembed');
		
		if(options[option] != newValue){
			options[option] = newValue;
			cleanViewOptions(options, defaultViewOptions, controlDependency);
			buffer.enqueue(editor, view.attr('id'), options);
			
			view.attr('data-cke-loldataembed', JSON.stringify(options));
		}
	}
	
	editor.fire('saveSnapshot');
	
	buffer.execute();
}

function cleanViewOptions(options, defaultViewOptions, controlDependency){
	var validOptions = getViewOptionDefinition(options, 'mode', controlDependency).childOptionsMap;

	for(var optKey in options){
		if(optKey != 'ids' && optKey != 'mode' && !validOptions[optKey]){
			delete options[optKey];
		}
	}
	
	for(var option in validOptions){
		if(!options[option]){
			options[option] = defaultViewOptions[option];
		}
	}
}

function makeViewsIdsUnique(evt) {
	var doc = evt.editor.document;	
	if(doc){
		evt.data.dataValue.forEach(function(element){
			var attr = element.attributes;			
			if(attr && attr['data-cke-loldataembed']){
				var currId = attr['id'] ;
				if(!currId || doc.getById(currId)){
					attr['id'] = nextPlaceholderId();
				}
			}
		}, CKEDITOR.NODE_ELEMENT);
	}
}

function parseEmbeddedViews(evt) {
	var editor = evt.editor,
		pendingInitRequest = initInstanceCount != initInstanceLoadedCount,
		request = pendingInitRequest? initRequestBuffer : new ViewRequestBuffer();

	evt.data.dataValue = evt.data.dataValue.replace(embeddedViewRegExp, function(match, ids, viewMode, optionStr){
		try{
			var options = $.extend(unserializeOptions(optionStr), {ids: ids.split(','), mode: viewMode}),												
				placeholder = generateViewPlaceholder(options);

			request.enqueue(editor, placeholder.id, options);			
			
			return placeholder.html;
		} catch(e){
			console.log(e);
			return '';
		}
	});
	
	pendingInitRequest || setTimeout(function() { request.execute(); }, 0);
}

function transformViewToEmbedded(element){
	var attrs = element.attributes,
		loldataembed = attrs && attrs['data-cke-loldataembed'];
						
	if(loldataembed) {
		var data = $.parseJSON(dummyContainer.html(loldataembed).text()),
			opts = serializeOptions(data),				
			output = '[[loldata:' + data.ids + '|' + data.mode  +  (opts? '|' + opts : '') + ']]';

		return new CKEDITOR.htmlParser.text(output);
	}
}

function tryExecuteInitRequest() {
	if(initInstanceCount == ++initInstanceLoadedCount){
		initRequestBuffer.execute();
		
		// Cleanup, init request after the first are generated whenever theres a ckeditor detach/attach,
		// like those generated for reordering of the editors
		initInstanceCount = initInstanceLoadedCount = 0;				
		initRequestBuffer = new ViewRequestBuffer();
	}	
}

function renderAutocompleteItem(ul, result) {
	var item = loldata[result.ref];

	return $('<li>' +
		'<a>' +
			'<span class="loldataembed-data-icon" style="background-image: url(' +
				 Drupal.settings.basePath + item.icon + ')"></span>' +
			'<span class="loldataembed-data-type"></span>' +
			'<span class="loldataembed-data-title">' + item.title + '</span>' + 
		'</a>' +
	'</li>')
		.appendTo(ul);
}

CKEDITOR.plugins.add('loldataembed', {
	requires: 'floatpanel,richcombo',	
	icons: 'league',
	lang: 'en,es',
	
	init: function(editor){		
		var lang = editor.lang.loldataembed;
		
		// When inserting rendered views via editor.insertHtml (e.g. copy/paste) all our relevant attrs are
		// protected because we prefixed them with data-cke-*. However the "id" attr is lost if we don't have
		// it white-listed in config.allowedContent, and even if it is withe-listed, we need to make sure the id
		// is unique, so we add a new id wherever necessary
		editor.on('toHtml', makeViewsIdsUnique, null, null, 11);
				
		// Create views from [[loldata:1231|view_mode|OPTIONS]] tags
		// This kicks in when the editor data is intialized or whenever the user pastes a tag manually
		editor.on('toHtml', parseEmbeddedViews, null, null, 16);
		
		// Transfors views to [[loldata]] tags on output
		editor.dataProcessor.htmlFilter.addRules(viewsToEmbeddedOutputRules);
		
		// We use 'cke-realelement' attr so image module won't mess with our views, but we need to remove
		// the attribute before any html filtering procedures begin, otherwise the fakeelement module will
		// strip our placeholder entirely
		editor.restoreRealElement && editor.on('beforeGetData', function(){
			$(editor.document.$).find('[data-cke-loldataembed]').removeAttr('data-cke-realelement');
		}, null, null, 1);
		
		editor.restoreRealElement && editor.on('getData', function(event){
			$(editor.document.$).find('[data-cke-loldataembed]').attr('data-cke-realelement', 1);
		});
					
		// Increment the instance counter, we use this to fire initRequestBuffer.execute() after all requests
		// have been queued	
		++initInstanceCount;
				
		editor.on('instanceReady', tryExecuteInitRequest);
							
		// Create the panel and remove preset styles
		var panel = new CKEDITOR.ui.floatPanel(editor, CKEDITOR.document.getBody(), panelDefinition),
			$panel = $(panel.element.$);
		
		$panel.css(panelCssOverrides);	
				
		panel.onEscape = function(key){
			// allow using of left arrow for going back
			if(key != 37 || $searchBox.val().length == 0){
				hidePanel();
			}		
		};	
		
		panel.onHide = function(){
			if(!okToHide){
				return true;
			}
			deleteDummyElement();
			$searchBox.autocomplete('close');
		};
		
		// Create block
		var blockId = CKEDITOR.tools.getNextId(),
			block = new CKEDITOR.ui.panel.block(panel._.panel.getHolderElement(), {
				attributes: { 'aria-label': lang.searchTitle } 
			});
			
		panel.addBlock(blockId, block);
		
		block.onKeyDown = function(key){
			 //enter, backspace on empty input means close
			if((key == 13 || key == 8) && $searchBox.val().length == 0){
				hidePanel();
			}
		};
		
		var $block = $(block.element.$);
		$block.html('<div id="loldataembed-search-container">' +
			'<input id="loldataembed-search-box" maxlength="32" placeholder="'+ lang.searchPlaceholder +'" />' +
		'</div>');
													
		// Autocomplete config	
		var $searchBox = $block.find('input');	
		var $autocompleteWidget =  $searchBox.autocomplete(autocompleteConfig)		
		.on('autocompleteselect', function(event, ui){			
			embedData(loldata[ui.item.ref]);	
			// IE note: hide must be after embedding so all content gets inside our dummy element
			hidePanel();		
			return false;
		})
		.on('autocompletefocus', function(){
			return false; // cancel text replacement
		})
		.on('autocompleteopen autocompleteclose autocompletecreate autocompletesearch', updatePanelSize)
		.autocomplete('widget');
		
		// Customize item rendering
		$searchBox.data( "ui-autocomplete" )._renderItem = renderAutocompleteItem;		
		
		// Reposition of autocomplete on different events
		// This is currently bugged (widget starts blinking indefinitely), comment out until we find a solution
		//editor.on('resize', updatePosition, this, null, -1);
		editor.on('loldataViewRendered', updatePosition);
		
		// Compute the searchbox height when possible
		if(!searchBoxHeight){
			editor.on('instanceReady', function(){
				if(!searchBoxHeight){
					$panel.css('opacity', 0);
					panel.showBlock(blockId, CKEDITOR.document.getBody(), 1, 0, 0);
					searchBoxHeight = $searchBox.height();
					panel.hide(1);	
					$panel.css('opacity', 1);
				}	
			});	
		}
		
		// Create command
		var wordFragment, dummyElement, okToHide = true;
		
		editor.addCommand('loldata_isearch', {
			canUndo: false,
			exec: function(editor){		
				var selection = editor.getSelection(),
				range = selection && selection.getRanges()[0],
				startContainer = range && range.startContainer,
				startBefore = range && range.getPreviousNode();
				
				if(!range || !range.collapsed)
					return;
				
				/*wordFragment = '';
					
				if(startBefore) {
					if(startBefore.type == CKEDITOR.NODE_ELEMENT && CKEDITOR.dtd.$inline[startBefore.getName()]){
						wordFragment = startBefore.getText();
					} else if(startContainer.type == CKEDITOR.NODE_TEXT){
						wordFragment = startContainer.getText().substr(0, range.startOffset);
					} else if(CKEDITOR.dtd.$block[startContainer.getName()]){ 
						wordFragment = startContainer.getText();
					} else {
						wordFragment = startBefore.getText();
					}
						
					wordFragment = /(\w+)$/.exec(wordFragment); 
					
					if(wordFragment){
						wordFragment = wordFragment[1];
					} else {
						wordFragment = '';
					}
				}*/	
				
				// try to form word and when done, save range
				// range -> display:false
				// if success on adding new val destroy range first
				// else show again, until then ->
				$searchBox.val(''); 
				
				showPanel();						
			}	
		});
		
		// Ctrl + Space
		editor.setKeystroke(CKEDITOR.CTRL + 32, 'loldata_isearch');
		// Shift + Space (some Windows machines have reserved the keystroke Ctrol + Space)
		editor.setKeystroke(CKEDITOR.SHIFT + 32, 'loldata_isearch');
				
		// Holds the default view options for this editor, whenever the user changes to a non-loldataview context
		// we display these.
		var defaultViewOptions = {ids: [defaultViewOptionTypeKey], mode: 'icon_label', size: '32'},
			viewControlCount = 0,
			viewControls = [],			
			controlDependency = {};
				
		// View mode
		addViewControl('LoldataViewMode', {
			label: lang.viewModeLabel,
            title: lang.viewModeTitle,
            toolbar: 'loldata,1',
            dataOption: 'mode',
            dataDependantOption: 'type',
            panelWidth: 130,
            comboWidth: 90
		});
		
		// Icon size
		addViewControl('LoldataSize', {
			label: lang.viewSizeLabel,
            title: lang.viewSizeTitle,
            toolbar: 'loldata,2',
            dataOption: 'size',
            dataDependantOption: 'mode',
            panelWidth: 120,
            comboWidth: 30
		});
		
		function addViewControl(name, config){
			editor.ui.addRichCombo(name, $.extend({ 
				panel: {
					css: [CKEDITOR.skin.getPath('editor')].concat(editor.config.contentsCss),
					multiSelect: false,
					attributes: { 'aria-label': config.title }
				},
				       
	            onRender: viewControlOnRender,	          
	            onClick: viewControlOnClick,
	            onOpen: viewControlOnOpen
			}, config));
			
			++viewControlCount;
						
			controlDependency[config.dataOption] = config.dataDependantOption;
		}
							
		function viewControlOnRender(){
			viewControls.push(this);
			
			this.createPanel(editor);
			viewControlInit(this, defaultViewOptions, controlDependency);
			
			// Adjust combo width
			var self = this;
			editor.once('instanceReady', function(){ 				
				$('#cke_' + self.id).find('.cke_combo_text').css({
		    		width: self.comboWidth
		    	});
			});
			
        	if(viewControls.length == viewControlCount){        		
        		var timeout,
        			onSelectionChange = function(){
	        			if(timeout){
	        				clearTimeout(timeout);
	        			}
	        			
	        			timeout = setTimeout(function(){
	        				var sel = editor.getSelection();
	        				if(sel){
	        					viewControlOnSelectionChange(sel, viewControls, defaultViewOptions, controlDependency);
	        				}
	        			}, 10);
	        		};
        		
        		editor.on('selectionChange', onSelectionChange);
        		       
        		// selectionChange is fired only when the element path changes, that is, the herarchy of elements. However, we
        		// need to update views on every physical selection change
        		editor.once('instanceReady', function(){ 
					var editable = editor.editable(),
						rawTimeout,
						rawOnSelectionChange = function(){
							if(rawTimeout){
								clearTimeout(rawTimeout);
							}
							
							rawTimeout = setTimeout(onSelectionChange, 200);
					};
										
        			editable.attachListener(CKEDITOR.env.ie ? editable : editor.document.getDocumentElement(), 
        				'mouseup', rawOnSelectionChange);
        			// this below adds 99% coverage on selection detection, but its slower, its very rare for a user to select
        			// stuff with they keyboard so we disable it
        			//editable.attachListener(editable, 'keyup', rawOnSelectionChange);
				});
        	}     
        }
        					
		function viewControlOnClick(value, diff){
			if(diff){				
				var views = getViewsInSelection(editor.getSelection());
            	
	        	if(views.length == 0){
	        		defaultViewOptions[this.dataOption] = value;
	        	} else {	        		
	        		updateViewOptions(editor, views, this.dataOption, value, defaultViewOptions, controlDependency);
	        		editor.fire('saveSnapshot');
	        	}
	        	
	        	this.setValue(value);	        	
			}			
			
			editor.focus();
		}		
		       		
		function showPanel(){
			// Inserting an empty span may cause a change in height when using autogrow plugin, this causes the panel 
			// to hide, so we block the hidding until all resizing is done (100ms with current autogrow)
			okToHide = false;
			
			// Position the panel with a dummy element	
			dummyElement = editor.document.createElement('span');
				
			editor.fire('lockSnapshot');
			editor.insertElement(dummyElement);	
			editor.fire('unlockSnapshot');	
				
			var scroll = editor.document.getWindow().getScrollPosition();
			panel.showBlock(blockId, dummyElement, 4, -scroll.x, -searchBoxHeight -scroll.y);
			updatePanelSize();
			
			setTimeout(function(){ $searchBox.focus();  }, 30);		
				
			// Ensure full focus on all browsers
			setTimeout(function(){ 				
				$searchBox.trigger('click');
				$searchBox.focus(); 				
				$searchBox.autocomplete('search'); 
				okToHide = true;
				
				setTimeout(function(){ $searchBox.focus();  }, 200);	
			}, 101);
			
			// Fix for failed key caputere						
			$searchBox.one('keyup', function(){
				$searchBox.autocomplete('search');
			});
		}
				
		function hidePanel(){	
			panel.hide(1);
		}
		
		function deleteDummyElement(){
			if(dummyElement){				
				editor.fire('lockSnapshot');
				dummyElement.remove(true);
				editor.fire('unlockSnapshot');	
				dummyElement = null;	
			}
		}
		
		function updatePanelSize() {
			$panel.height($autocompleteWidget.height() + $searchBox.parent().height() + 20);
			$panel.width(1000);
			$panel.width($autocompleteWidget.width() + 10);
		}
		
		function updatePosition(evt){
			setTimeout(function(){
				if(panel.visible && !updatePosition.supress){
					if(updatePosition.timeout){
						clearTimeout(updatePosition.timeout);
					}
					
					updatePosition.timeout = setTimeout(function(){
						if(panel.visible && !updatePosition.supress){
							updatePosition.supress = true;
							deleteDummyElement();
							showPanel();
							updatePosition.supress = false;
						}						
					}, 150);
				}
			}, 50);
		}
				
		function embedData(data) {
			var options = $.extend({}, defaultViewOptions, {ids: [data.id]}),
				placeholder = generateViewPlaceholder(options);
			
			// Add a placeholder element to the editor
			editor.insertHtml(placeholder.html, 'unfiltered_html');
			
			// Async load the view
			displayView(editor, options, placeholder.id);
		}
	}	
});	
	
})(jQuery);
