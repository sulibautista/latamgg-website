<?php

function loldata_embed_view_options_config(){
  return array(
    'modes' => array(
      'icon' => t('Icon'),  
      'icon_label' => t('Icon + Label'),
      'embedded' => t('Incrustado')
    ),
    
    'sizes' => array(
      32 => t('32px'),
      64 => t('64px')
    ),
    
    'base' => array(
      'modes' => array('icon', 'icon_label'),
      'sizes' => array(32, 64)
    ),
    
    'types' => array(
      '_default' => TRUE,
      'item' => TRUE,
      'champion' => TRUE,
      'mastery' => TRUE, 
      'spell' => TRUE, 
      'rune' => TRUE,
      'ability' => array(
        'modes' => array('icon', 'icon_label', 'embedded'),
        'sizes' => array(
          'icon' => TRUE,
          'icon_label' => TRUE
        ),
      )
    )
  );
}
