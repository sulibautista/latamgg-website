<?php
namespace FieldDependency;

interface Controller {
  public function setDependee($dependee, &$context = NULL);
  public function isAvailable();
  public function value();
  public function singleValue();
}

class BaseController implements Controller{
  protected $dependee;
  protected $context;
  
  public function setDependee($dependee, &$context = NULL){
    $this->dependee = $dependee;
    $this->context = &$context;
  }
  
  public function isAvailable(){
    return false;
  }
  
  public function value() {
    if($this->isAvailable()){
      return $this->_value();
    }
    
    return NULL;
  }  
  
  public function singleValue(){
    return $this->value();
  }
}

class EmptyController extends BaseController {}

class FieldFormController extends BaseController {
  public function isAvailable() {
    return isset($this->context['values'][$this->dependee]['und']);
  }
  
  protected function _value(){
    return $this->context['values'][$this->dependee]['und'];
  }
  
  public function singleValue() {
    $val = $this->value();
    return $val && is_array($val)? (is_array($val[0]) && isset($val[0]['target_id'])? $val[0]['target_id'] : $val[0]) : $val;
  }
}

class FieldEntityController extends BaseController{
  public function isAvailable() {    
    return isset($this->context->{$this->dependee}['und']);
  }
      
  protected function _value(){
    return $this->context->{$this->dependee}['und'];
  }
  
  public function singleValue() {
    $val = $this->value();
    return $val? $val[0]['target_id'] : $val;
  }
}