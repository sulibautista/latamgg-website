(function($){$(document).ready(function () {	
	var viewport = $('.masterypage-page');  
	
	function changePageTitle() {
		var title = prompt('Set the mastery panel title', $(this).text());
		if(title)
			$(this).text(title);
	}	
	
	var dialog = '<div title="Edit mastery">' +
		'<label for="loldata-id">Base mastery</label>' +
		'<input type="text" name="loldata-id" id="loldata-id" /> ' +		
		'<label for="max-points">Total available points</label>' +
		'<input type="text" name="max-points" id="max-points">' +		
		'<input type="checkbox" id="has-parent">Has parent?</input>' +
	'</div>';	
	
	function editCellOptions() {		
		var d = $(dialog);
		var cell = $(this).closest('.masterypage-cell');
		
		var loldataInput = d.find('#loldata-id').val(cell.data('loldata-id'));
		var maxPointsInput = d.find('#max-points').val(cell.data('max-points'));
		var hasParentInput = d.find('#has-parent').prop('checked', cell.data('has-parent'));
		
		d.dialog({
			autoOpen: true,
			height: 450,
			width: 250,
			modal: true,
			buttons: {
				'Save' : function() {
					cell.data('loldata-id', loldataInput.val());
					cell.data('max-points',  parseInt(maxPointsInput.val()));
					cell.data('has-parent', hasParentInput.prop('checked'));
					$(this).dialog('close');	
				},				
				'Cancel' : function() {
					$(this).dialog('close');
					d.remove();
				}
			}			
		});
	}
	
	// Add cell widget
	var addCellWidget= $('<div class="masterypage-edit-cell"></div>');
	
	// Empty cell
	var emptyCell = $('<div class="masterypage-cell-empty"><div class="masterypage-edit-opts-mastery-cell"><div class="masterypage-edit-del-mastery-cell-empty">Delete</div></div></div>');
	emptyCell.find('.masterypage-edit-del-mastery-cell-empty').button().click(function(){
		$(this).closest('.masterypage-cell-empty').detach(); // mem leak, we dont care		
	});
	var addEmptyCellBtn = $('<div class="masterypage-edit-add-empty-cell">Empty</div>');
	addEmptyCellBtn.button().click(function(){		
		$(this).parent().before(emptyCell.clone(true, true));
	});
	
	// Mastery cell opionts
	var masteryCellOpts = $('<div class="masterypage-edit-opts-mastery-cell"><div class="masterypage-edit-del-mastery-cell">Delete</div><div class="masterypage-edit-edit-mastery-cell">Edit</div></div>');
	masteryCellOpts.children('.masterypage-edit-del-mastery-cell').button().click(function(){
		$(this).closest('.masterypage-cell').detach();			
	});
	masteryCellOpts.children('.masterypage-edit-edit-mastery-cell').button().click(editCellOptions);
	
	// Mastery cell
	var masteryCell = $('<div class="masterypage-cell"><div class="masterypage-cell-icon"></div><div class="masterypage-cell-rank">0/0</div></div>');	
	var addMasteryCellBtn = $('<div class="masterypage-edit-add-mastery-cell">Mastery</div>'); 
	addMasteryCellBtn.button().click(function(){
		var cell = masteryCell.clone();
		cell.append(masteryCellOpts.clone(true, true));
		$(this).parent().before(cell);	
	});
	
	addCellWidget.append(addEmptyCellBtn.clone(true, true), addMasteryCellBtn.clone(true, true));
	
	// Add row btn
	var panelRow = $('<div class="masterypage-row"></div>');
	var addPanelRowBtn = $('<div class="masterypage-edit-row">+ Row</div>');	
	addPanelRowBtn.button().click(function(){
		$(this).before(panelRow.clone().append(addCellWidget.clone(true, true)));		
	});	
	
	// Add panel btn
	var masteryPanel = $('<div class="masterypage-panel"><div class="masterypage-panel-masteries"></div><div class="masterypage-panel-title"><span class="masterypage-panel-title-text">Page title</span>: 0</div></div>');	
	var addPanelBtn = $('<div class="masterypage-edit-panel">+</div>');
	addPanelBtn.button().click(function(){
		var panel = masteryPanel.clone();
		panel.children('.masterypage-panel-masteries').append(addPanelRowBtn.clone(true));
		panel.find('.masterypage-panel-title-text').click(changePageTitle);
		$(this).before(panel);
	});		
	
	// Attach event handlers to current mastery page items, if any	  
	viewport.find('.masterypage-panel-title-text').click(changePageTitle);
	viewport.find('.masterypage-panel-masteries').append(addPanelRowBtn.clone(true));
	viewport.find('.masterypage-cell').append(masteryCellOpts.clone(true, true));
	viewport.find('.masterypage-row').append(addCellWidget.clone(true, true));
	
    viewport.append(addPanelBtn);
    
});})(jQuery);
