<?php

/**
 * The Controller for MasteryPage entities
 */
class MasteryPageController extends EntityAPIController {
  public function __construct($entityType) {
    parent::__construct($entityType);
  }

  public function create(array $values = array()) {
    $values += array( 
      'masterypage_id' => 0,
      'title' => '',
      'available_points' => '',
      'points_per_row' => '',
      'active' => 0,
      'image' => 0,
      'panels' => array()    
    );
    
    return parent::create($values);
  }
  
  public function save($entity, DatabaseTransaction $transaction = NULL) {
    //TODO implement save    
    return parent::save($entity, $transaction);
  }
  
  
  public function attachForeignProperties(array $entities) {    
    foreach($entities as $page) {
      $page->panels = db_select('masterypage_panel', 'p')
        ->fields('p')
        ->condition('masterypage_id', $page->masterypage_id)
        ->orderBy('position', 'ASC')
        ->execute()
        ->fetchAll();
      
      $page->total_masteries = 0;
      foreach($page->panels as $pos => $panel) {
        $page->panels[$pos]->masteries = array();
        $m = &$page->panels[$pos]->masteries;
        $masteries = db_select('masterypage_mastery', 'm')
          ->fields('m')
          ->condition('masterypage_panel_id', $panel->masterypage_panel_id)
          ->orderBy('row', 'ASC')
          ->orderBy('`column`', 'ASC')
          ->execute()
          ->fetchAll();
        
        foreach($masteries as $mastery) {          
          foreach(loldata_load($mastery->loldata_id) as $key => $value) {
            $mastery->$key = $value;
          }
          $m[] = $mastery;
          ++$page->total_masteries;
        }      
      }      
    }  
  }
  
  public function delete($ids, DatabaseTransaction $transaction = NULL) {
    //TODO implement delete  
    return parent::delete($ids, $transaction);
  }
  /**
   * View modes:
   *  full: Title, all attributes, no point rendering
   *  embedded_view: No attributes, point rendering
   *  embedded_edit : All attributes, no point rendering
   */
  public function buildContent($entity, $view_mode = 'full', $langcode = NULL) {
    $viewModeFull = $view_mode == 'full';
    $viewModeEmbeddedView =  $view_mode == 'embedded_view';
    $viewModeEmbeddedEdit = $view_mode == 'embedded_edit';
    
    $content = array(
      '#type' => 'container',
      '#simple' => TRUE,
      '#attributes' => array(
        'class' => array('masterypage-page masterypage-page-' . $entity->masterypage_id)        
      ),
      '#attached' => array('css' => array(drupal_get_path('module', 'masterypage') . '/css/masterypage.css'))
    );
        
    if($viewModeEmbeddedEdit || $viewModeFull) {
      $content['#attributes'] +=  array(
        'data-available_points' => $entity->available_points,
        'data-points_per_row' => $entity->points_per_row,        
      );
    }    
    
    $cellIndex = 0;
    foreach($entity->panels as $panelIdx => $panel) {
      $rows = array(
        '#type' => 'container',
        '#simple' => TRUE,
        '#attributes' => array('class' => array('masterypage-panel-masteries')),
        '#weight' => $panel->position
      );
      
      $row = array();
      $currentCol = -1;
      $current_row = -1;         
      $pointCount = 0;
      foreach($panel->masteries as $masteryIdx => $mastery) {
        if(!isset($mastery->points))
          $mastery->points = 0;

        $pointCount += $mastery->points;
        
        if($mastery->row != $current_row) {
          $rows['r_' . $current_row] = $row;
          $current_row = $mastery->row;
          $currentCol = -1;     
          $row = array(
            '#type' => 'container',
            '#simple' => TRUE,
            '#attributes' => array('class' => array('masterypage-row masterypage-row-' . $current_row)),
            '#weight' => $current_row
          );
          
          if($viewModeEmbeddedEdit || $viewModeFull){
            $row['#attributes']['data-row_index'] = $current_row;
          }
        }
        
        // Always fill left columns
        while($currentCol++ < $mastery->column-1) {
          $row['c_' . $currentCol] = array(
            '#type' => 'container',
            '#simple' => TRUE,
            '#attributes' => array('class' => array('masterypage-cell-empty')),
            '#weight' => $currentCol,
            '#_is_empty' => TRUE 
          );
        }
        
        $cell = array(
          '#type' => 'container',
          '#simple' => TRUE,
          '#attributes' => array('class' => array('masterypage-cell')),
          '#weight' => $currentCol,
          'icon' => array(
            '#type' => 'container',
            '#simple' => TRUE,
            '#attributes' => array('class' => array('masterypage-cell-icon')),
            'icon' => array(
              '#theme' => 'loldata_icon', 
              '#entities' => array($mastery),
              '#size' => 50
            )
          ),
          'points' => array(
            '#type' => 'container',
            '#simple' => TRUE,
            '#attributes' => array('class' => array('masterypage-cell-rank')),
            'points' => array(
              '#markup' => $mastery->points . '/' . $mastery->max_points
            )
          ) 
        );
        
        if($viewModeEmbeddedEdit || $viewModeFull) {
          $data_attr = array(
            'data-cell_index' => $cellIndex++,
            'data-mastery_id' => $mastery->masterypage_mastery_id,
            'data-max_points' => $mastery->max_points
          );
          
          if($mastery->parent){            
            $data_attr['data-parent'] = $mastery->parent;
            $data_attr['data-parent_points'] = $mastery->parent_points;
          }
          
          $cell['#attributes'] += $data_attr;
        }        
        
        
        if($mastery->points == $mastery->max_points) {
          $cell['#attributes']['class'][] = 'masterypage-cell-max-rank';
        } else if($mastery->points == 0) {
          $cell['#attributes']['class'][] = 'masterypage-cell-no-rank';
        } 
                
        if($mastery->parent) {
          $cell['#attributes']['class'][] = 'masterypage-cell-child';
          
          $parent_row = $current_row - 1;
          do {
            $parent = &$rows['r_' . $parent_row]['c_' . $currentCol];
          } while(!empty($parent['#_is_empty']) && $parent_row--);
          
          $classes = array('masterypage-cell-parent', 'masterypage-cell-parent-' . ($current_row - $parent_row));          
          if($mastery->points == $mastery->max_points){
            $classes[] = 'masterypage-cell-parent-full';
          }
          
          $parent['#attributes']['class'] = array_merge($parent['#attributes']['class'], $classes);
        }
        
        $row['c_' . $currentCol] = $cell;
        //TODO add inline tooltips here
      }       
      // Last row
      $rows['r_' . $current_row] = $row;
              
      $content['p_' . $panelIdx] = array(
        '#type' => 'container',
        '#simple' => TRUE,
        '#attributes' => array('class' => array('masterypage-panel masterypage-panel-' . $panel->position)),
        'm' => $rows,
        't' => array(
          '#weight' => 10,
          '#markup' => '<div class="masterypage-panel-title"><span class="masterypage-panel-title-text">'
            . $panel->title .'</span>: <span class="masterypage-panel-title-points">'. $pointCount  .'</span></div>'
        )
      );
    }     
    
    return parent::buildContent($entity, $view_mode, $langcode, $content);
  }
}


/**
 * Implements hook_masterypage_load(). 
 */
function masterypage_masterypage_load(array $entities) {
  entity_get_controller('masterypage')->attachForeignProperties($entities);
}


