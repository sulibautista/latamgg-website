(function($){$(document).ready(function () {
	"use strict";
	
		// Config
	var allowObsolete = false,
		
		// Data storage
	  	runeData,
	  	runeListCache = {},		
	  	allListItemResults = [],
	
		$runeTemplate = $('<div class="lol-runes-1-rune">' +
		  	'<div class="lol-runes-1-icon"></div>' +
		  	'<div class="lol-runes-1-count"></div>' +
		  	'<div>' + 
				'<span class="lol-runes-1-description"></span>' +
				'<div class="lol-runes-1-title"></div>' +
 			'</div>' +
	    '</div>'),
		
		$runeOpsTemplate = $('<div class="btn-group-vertical btn-group-xs runefield-picker-ops">' +
			'<button type="button" class="btn btn-default btn-s add"><i class="glyphicon glyphicon-plus"></i></button>' +
			'<button type="button" class="btn btn-default btn-s subtract"><i class="glyphicon glyphicon-minus"></i></button>' +
			'<button type="button" class="btn btn-danger btn-s delete"><i class="glyphicon glyphicon-remove"></i></button>' +
		'</div>'),
		
		$listRuneTemplate = $('<li><a><div class="runefield-picker-list-rune">'+
			'<div class="lol-runes-1-icon"></div>' +
			'<div>' + 
				'<span class="lol-runes-1-description"></span>' +
				'<div class="lol-runes-1-title"></div>' +
 			'</div>' +
		'</div></a></li>'),
	
		index = new selfSite.loldata.SearchIndex('runefield', ['rune']);
			
	selfSite.loldata.load({
		rune: true, 
		//runecategories: true
	}).then(function(data){
		runeData = data.rune;
		
		var runeIds = [],
			doIndex = !index.isFromCache;
		
		$.each(runeData, function(runeId, rune){		
			if(rune.obsolete && !allowObsolete) return;
			
			runeIds.push(runeId);	
			
			// Add to index
			if(doIndex){
				selfSite.loldata.indexObject(index, rune);
			}
			
			// Add to view
			var rune = $listRuneTemplate.clone()
			.data('rune', rune)
			.find('.lol-runes-1-description')
				.html(rune.description)
			.end()
			.find('.lol-runes-1-title')
				.text(rune.title)
			.end();	
			
			runeListCache[runeId] = rune;
			allListItemResults.push({ref: runeId});	
		});
		
		selfSite.loldata.commitIndex(index);
				
		selfSite.loldata.view(runeIds, 'icon').done(function(icons){
			$(icons).each(function(){
				var $this = $(this);
				$this.appendTo(runeListCache[$this.data('loldata')].find('.lol-runes-1-icon'));
			});			
		});	
		
	}, function(xhr, err){
		console.log(err);
	});
	
	function createRune(data, icon) {
		var rune = runeData[data.id];
					
		var $rune = $runeTemplate.clone()
		.data('rune', data)
		.find('.lol-runes-1-icon')			
			.html(icon)
		.end()
		.find('.lol-runes-1-description')
			.html(rune.description)
		.end()
		.find('.lol-runes-1-title')
			.text(rune.title)
		.end()
		.append($runeOpsTemplate.clone());
		
		updateRuneCount($rune, data);
		
		return $rune;
	}
		
	function getRuneFromChild(child){
		return $(child).closest('.lol-runes-1-rune');
	}
	
	function addRuneAmountFromChild(child, delta){
		addRuneAmount(getRuneFromChild(child), delta);
	}
	
	function addRuneAmount($rune, delta) {
		var data = $rune.data('rune');
		data.count += delta;
		data.count = Math.min(Math.max(data.count, 1), 9);
		updateRuneCount($rune, data);
	}
	
	function updateRuneCount($rune, data) {
		$rune.find('.lol-runes-1-count')
			.text(data.count + 'x')
		.end()
		.find('.add')
			.prop('disabled', data.count > 8)
		.end()
		.find('.subtract')
			.prop('disabled', data.count < 2);
	}
		
	$('div.field-widget-runefield-rune-picker').each(function(){
		var $field = $(this),
			$viewport = $field.find('.runefield-picker-container'),
			$valueInput = $field.find('input.runefield-picker-value'),
			$searchCont = $field.find('.runefield-picker-search'),
			$searchInput = $searchCont.find('.runefield-picker-searchbox'),
			$typeInput = $searchCont.find('input[name=runefield-picker-type]'),			
			initData = $.parseJSON($valueInput.val()),
			type = $typeInput.filter(':checked').val(),
			$allRunes = $();

		// Initial procedures
		$viewport.find('.lol-runes-1-rune').each(function(i){			
			$(this)
				.data('rune', initData[i])
				.append($runeOpsTemplate.clone());
			
			updateRuneCount($(this), initData[i]);
		});
				
		// Type change
		$typeInput.change(function(){
			type = $(this).val();
			// Lang hack until we implement rune categories
			if(selfSite.loldata.getUserLang() == 'es'){
				type = type == 'mark'? 'marca' :
					(type == 'glyph'? 'glifo':
					(type == 'seal'? 'sello' : type));
			}
			$searchInput.focus();
		});
		
		// Rune search
		$searchInput.on('focus click', function(){
			$searchInput.autocomplete('search');
		});
		
		$searchInput.autocomplete({
			autoFocus: true,
			delay: 0,
			minLength: 0,
			source: function(req, callback){
				var typeIsAll = type == 'all';
				
				if($.trim(req.term) === '' && typeIsAll){
					callback(allListItemResults);
				} else {
					var results = index.search(req.term + (typeIsAll? '':' '+type));
					callback(results);
				}
			}
		})
		.on('autocompletefocus', function(){
			return false; // cancel text replacement
		})		
		// Add selected rune
		.on('autocompleteselect', function(event, ui){
			var id = parseInt(ui.item.ref, 10),
				$listRune = runeListCache[id],
				
				// Find existing rune in viewport
				$currentRune = $viewport.find('.lol-runes-1-rune').filter(function(){
					return $(this).data('rune').id == id;
				}),
				
				$highlight;
				
			if($currentRune.length){
				addRuneAmount($currentRune, +1);	
				$highlight = $currentRune;							
			} else {
				$highlight = createRune({id: id, count: 1}, $listRune.find('.lol-runes-1-icon').html())
					.appendTo($viewport);
			}
			
			$highlight.effect("highlight", {}, 700);
		});
		
		// Copy item from cache
		$searchInput.data( "ui-autocomplete" )._renderItem = function( ul, result) {			
			return runeListCache[result.ref].clone().appendTo(ul); 	
		};
		
		$searchInput.autocomplete('widget').addClass('runefield-picker-results');
		
		// Sortable on viewport
		$viewport.sortable({
			tolerance: 'pointer'
		});		
		
		// Increment rune count
		$viewport.on('click', '.add', function(){
			addRuneAmountFromChild(this, +1);
		});
		
		// Decrement rune count
		$viewport.on('click', '.subtract', function(){
			addRuneAmountFromChild(this, -1);
		});
		
		// Remove rune
		$viewport.on('click', '.delete', function(){
			getRuneFromChild(this).remove();
		});		
				
		$valueInput.closest('form').submit(function(){
			var data = $viewport.find('.lol-runes-1-rune').map(function(){
				return $(this).data('rune');	
			}).get();
			
			$valueInput.val(JSON.stringify(data));
		});
	});	
	
});})(jQuery);