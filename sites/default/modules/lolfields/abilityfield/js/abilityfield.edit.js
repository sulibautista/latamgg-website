if(!window.selfSite) selfSite = {};
selfSite.abilityField = [];

(function($){$(document).ready(function () {
	"use strict";
	
	var totalPoints = 18,
	    maxPointsPerAbility = 5;
	
	var pointCheckOverrides = {
		'Udyr': function(abilityLevels, index, abilityN, totalCount, upToCount){
			return totalCount < maxPointsPerAbility && 
				checkRowRequirements(abilityLevels, index, abilityN, totalCount, upToCount);
		} 
	};
	
	function checkRowRequirements(abilityLevels, index, abilityN, totalCount, upToCount) {
		if(Math.ceil((index+1)/2) < upToCount+1){
			return false;
		}
		
		var levels = abilityLevels[abilityN];
		for(var i = totalCount-1; i >= 0 && levels[i] > index; --i){
			if(Math.ceil((levels[i]+1)/2) < i+2){
				return false;
			}								
		}
		
		return true;
	}
	
	$('div.field-widget-abilityfield-ability-picker').each(function(){
		var $this = $(this),
			$viewport = $this.find('div.abilityfield-picker-container'),
			$valueInput = $this.find('input.abilityfield-picker-value'),
			$lastSelected,
			lastSetPointIndex = -1,
			initPoints = $valueInput.val().split(''),
			points = initPoints,
			championId = $valueInput.data('champion');
		
		selfSite.abilityField[$this.attr('id')] = {
			'setChampion': function(id) {
				if(championId == id)
					return;
				
				var lastChampion = championId;
				championId = id;
				
				// Reset view if different point distribution
				if(pointCheckOverrides[championId] || pointCheckOverrides[lastChampion]){
					initPoints = points.slice();
					$viewport.find('td.enabled, td.selected, td.last-selected').attr('class', '').text('');
					restartView();
				}									
			},
			
			'setAbilities': function(abilities) {
				var $abilities = $(abilities);
								
				$viewport.find('tbody th').each(function(i){
					$(this).empty().append($abilities[i]);
				});
			}	
		};
		
		// Update viewport using current values
		points.length = totalPoints;
		for(var i = 0; i < totalPoints; ++i){
			points[i] = points[i]? parseInt(points[i], 10) : 0;	
			
			if(points[i]){
				lastSetPointIndex = i;
			}	
		}				
		markLastSelected(lastSetPointIndex+2);
		updateEnabled();
				
		// Click handlers
		$viewport.on('click', 'td.enabled', function(e){
			var $this = $(this),
				nextPoint = $this.index();
			$viewport.find('td.selected:nth-child(' + (nextPoint+1) + ')').removeClass('selected').text('');
			$this.removeClass('enabled').addClass('selected').text(nextPoint);	
			
			if(!points[nextPoint]){
				$lastSelected.removeClass('last-selected');	
				$lastSelected = $this.addClass('last-selected');
			}
			
			setPoint($this, true);
			updateEnabled();	
			
			e.preventDefault();
		});
		
		$viewport.on('click contextmenu', 'td.last-selected', function(e){
			var $this = $(this),
				index = $this.index();
				
			$this.removeClass('selected last-selected').text('');	
			
			markLastSelected(index);
			
			setPoint($this, false);
			updateEnabled();
			
			e.preventDefault();
		});
		
		// Form handling
		$valueInput.closest('form').submit(function(){
			$valueInput.val(points.join(''));
		});	
		
		function restartView() {			
			markLastSelected(0);
			
			points.length = totalPoints;
			for(var i = 0; i < totalPoints; ++i){
				points[i] = 0;
			}
			
			updateEnabled();
					
			for(var i = 0; i < totalPoints; ++i){
				if(initPoints[i]){	
					var point = parseInt(initPoints[i], 10);	
					$viewport.find('tr:nth-child(' + point + ') td.enabled:nth-child(' + (i+2) + ')').trigger('click');
				} else{
					break;
				}
			}
		}
		
		function markLastSelected(index){
			$lastSelected = index <= 1? $viewport.find('th:nth-child(1):first') : 
					$viewport.find('td.selected:nth-child('+ index +')').addClass('last-selected');
		}	
		
		function setPoint($td, set) {
			points[$td.index() - 1] = set? $td.parent().index() + 1 : 0;	
		}
		
		function updateEnabled(){
			var limitReach = points[totalPoints - 1] != 0,
				indexSize = $lastSelected.index(),
				abilityCount = [, 0, 0, 0, 0, 0],
				abilityLevels = [, [], [], [], [], []];
							
			for(var i = 0; i < indexSize; ++i){
				++abilityCount[points[i]];
				abilityLevels[points[i]].push(i);
			}
			
			$viewport.find('tr:not(:first-child) td:not(.selected)').each(function(){	
				var good = true;
					
				if(limitReach){
					good = false;
				} else {
					var $this = $(this),
						index = $this.index() - 1;
					
					if(index > indexSize){
						good = false;
					} else {
						var abilityN = $this.parent().index() + 1;
						var totalCount = abilityCount[abilityN],
							upToCount = 0;
						
						for(var i = 0; i < index; ++i){
							if(points[i] == abilityN)
								++upToCount;
						}
						
						if(pointCheckOverrides[championId]){ 
							good = pointCheckOverrides[championId](abilityLevels, index, abilityN, totalCount, upToCount);
						} else if(abilityN == 5){ 
							if(totalCount == 3 || upToCount+1 > index/5){
								good = false;								
							} else {
								var levels = abilityLevels[abilityN];
								for(var i = totalCount-1; i >= 0 && levels[i] > index; --i){
									if(i+2 > levels[i]/5 ){										
										good = false;
										break;
									}								
								}
							}
						} else if(totalCount == maxPointsPerAbility || 
								!checkRowRequirements(abilityLevels, index, abilityN, totalCount, upToCount)){
							good = false;
						} 				
					}
				}
				
				if(good)								
					this.setAttribute('class', 'enabled');
				else					
					this.removeAttribute('class');
			});			
		}
	});	
	
	
});})(jQuery);