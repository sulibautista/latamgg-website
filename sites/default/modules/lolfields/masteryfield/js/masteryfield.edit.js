(function($){$(document).ready(function () {
	"use strict";
		
	function unlockCells(cells){
		cells.removeClass('masterypage-cell-no-rank').each(function(){
			$(this).data('masteryfield').unlocked = true;
		});
	}
	
	function lockCells(cells){
		cells.addClass('masterypage-cell-no-rank').each(function(){
			$(this).data('masteryfield').unlocked = false;
		});
	}
		
	function tryUnlockChild(child, childData, parentData){
		if(childData.parentPoints){
			if(parentData.currentPoints >= childData.parentPoints){
				unlockCells(child);
			}
		} else if(parentData.currentPoints === parentData.maxPoints){
			unlockCells(child);
		}
	}
	
	function toggleMaxed(cells, state){
		cells.each(function(){
			var $this = $(this),
				thisData = $this.data('masteryfield');
				
			if(thisData.unlocked && thisData.currentPoints === 0){
				$this.toggleClass('masterypage-cell-no-rank', state);
			}
		});
	}
	
	function updateMasteryPoints(cellData, panelData, delta, setPoints){
		// Cell updates
		cellData.currentPoints += delta;
		cellData.rank.text(cellData.currentPoints + '/' + cellData.maxPoints);
		setPoints[cellData.cellIndex] = cellData.currentPoints;
		
		// Panel updates
		panelData.usedPoints += delta;
		panelData.titlePoints.text(panelData.usedPoints);			
	}
			
	$('.field-widget-masteryfield-page-widget').each(function(){
		var viewport = $('.masterypage-page', this),		
			pointsInput = $('.masteryfield-points-input', this),
			setPoints = [],
			cells = viewport.find('.masterypage-cell'),
			availPoints = viewport.data('available_points'),	
			pointsPerRow = viewport.data('points_per_row'),	
			usedPoints = 0;	

		// Initialize data
		viewport.find('.masterypage-panel-masteries').each(function(){	
			var panel = $(this),
				rows = panel.find('.masterypage-row');
				
			panel.data('masteryfield', {
				usedPoints: 0,
				titlePoints: panel.parent().find('.masterypage-panel-title-points'),
				rows: rows
			});
			
			rows.each(function(){
				var row = $(this),
					cells = row.find('.masterypage-cell'),
					isUnlocked = row.index() === 0; // Unlock cell if it belongs to first row of its panel
				
				row.data('masteryfield', {
					cells: cells,
					unlockableCells: cells.not('.masterypage-cell-child')
				});
				
				cells.each(function(){
					var cell = $(this),
						data = cell.data(),
						masteryId = data.mastery_id,
						isChild = data.parent,
						isParent = cell.hasClass('masterypage-cell-parent');
									
					cell.data('masteryfield', {
						masteryId: masteryId,
						panel: panel,
						row: row,
						rank: cell.find('.masterypage-cell-rank'),
						unlocked: isUnlocked, 
						currentPoints: 0,
						cellIndex: data.cell_index,
						maxPoints: 	data.max_points,
						parent: isChild && panel.find('[data-mastery_id="' + data.parent + '"]'),
						child: isParent && panel.find('[data-parent="' + masteryId + '"]')
					});
					
					if(isUnlocked){
						cell.removeClass('masterypage-cell-no-rank');
					}
				});
			});
		});
				
		// Add point
		cells.click(function(e) {
			var $this = $(this),
				data = $this.data('masteryfield');				
				
			e.preventDefault();	
						
			if(e.which === 1 && data.unlocked && usedPoints < availPoints && data.currentPoints < data.maxPoints){ 
				var row = data.row,
					panel = data.panel,
					panelData = panel.data('masteryfield');
	
				updateMasteryPoints(data, panelData, +1, setPoints);						
				++usedPoints;
								
				// Add classes for fully ranked masteries				
				if(data.currentPoints === data.maxPoints){
					$this.addClass('masterypage-cell-max-rank');		
					
					if(data.parent){
						data.parent.addClass('masterypage-cell-parent-full');
					}		
				}
				
				// Look if child can be unlocked
				if(data.child){
					var childData = data.child.data('masteryfield');
					if(panelData.usedPoints >= childData.row.index() * pointsPerRow){
						tryUnlockChild(data.child, childData, data);
					}				
				}
				
				// Unlock next row if reached point threshold				
				var nextRowIndex = ~~(panelData.usedPoints / pointsPerRow);
				if(nextRowIndex != row.index() && nextRowIndex < panelData.rows.length){			
					var unlockedRow = panelData.rows.eq(nextRowIndex);	
					unlockCells(unlockedRow.data('masteryfield').unlockableCells);
					unlockedRow.find('.masterypage-cell-child.masterypage-cell-no-rank').each(function(){
						var child = $(this),
							childData = child.data('masteryfield');
						tryUnlockChild(child, childData, childData.parent.data('masteryfield'));					
					});
				}
				
				// Visually disable every other cell when we have no points left
				if(usedPoints === availPoints) {
					toggleMaxed(cells, true);
				}									
			}		
		});
		
		// Remove point
		cells.on('contextmenu', function(e){
			var $this = $(this),
				data = $this.data('masteryfield');
				
			e.preventDefault();			
			
			if(data.unlocked && data.currentPoints > 0) { 
				var panel = data.panel,
					panelData = panel.data('masteryfield'), 
					row = data.row,
					child = data.child;
				
				// If child has any points, we cant remove any points from its parent
				if(child && child.data('masteryfield').currentPoints > 0){
					return;
				}
				
				// Determine if the state of the panel will be valid after removing the point
				var pointCount = -1, // Add delta
					skip = false,
					rows = panelData.rows;
					
				rows.slice(0, row.index() + 1).each(function(){
					$(this).data('masteryfield').cells.each(function(){
						pointCount += $(this).data('masteryfield').currentPoints;
					});
				});
				
				rows.slice(row.index() + 1).each(function(){
					var morePoints = 0,
						row = $(this),
						rowCells = row.data('masteryfield').cells,
						neededPointsInThisRow = row.index() * pointsPerRow;				
						
					rowCells.each(function(){
						morePoints += $(this).data('masteryfield').currentPoints;
					});
									
					if(morePoints > 0){
						if(neededPointsInThisRow <= pointCount){
							pointCount += morePoints;
						} else {
							skip = true;
							return false;
						}
					} else if(neededPointsInThisRow > pointCount){					
						lockCells(rowCells);					
						return false;
					}
				});
				
				if(skip) {
					return;
				}	
					
				// Safe to remove
				updateMasteryPoints(data, panelData, -1, setPoints);
				--usedPoints;
				
				// Restore visually disabled cells when we reach N-1 points from N
				if(usedPoints === availPoints-1){
					toggleMaxed(cells, false);
				}
					
				if(child){
					lockCells(child);
				}			
				
				$this.removeClass('masterypage-cell-max-rank');	
				
				if(data.parent){
					data.parent.removeClass('masterypage-cell-parent-full');
				}
			}
		});
		
		// Initialize current points
		var pointsInCell = pointsInput.val().split('');
		for(var i = 0; i < cells.length; ++i){	
			var points = i < pointsInCell.length? parseInt(pointsInCell[i], 10) : 0;
			if(isNaN(points))
				points = 0;
				
			setPoints[i] = 0;
			for(var k = 0; k < points; ++k){
				cells.eq(i).trigger({type:'click', which: 1});
			}
		}	
		
		// Form handler
		pointsInput.closest('form').submit(function(){
			pointsInput.val(setPoints.join(''));
		});						
	});
	
});})(jQuery);