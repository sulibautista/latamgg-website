(function($){$(document).ready(function () {
	"use strict";
		
	var $addMoreBtnTemplate = $('<div class="summoner-spell-picker-add-more">' + 
			'<button type="button" class="btn btn-primary">'+ '+ Hechizos' + '</button>' +
		'</div>'),
			
		$spellSetTemplate = $('<div class="lol-summoner-spells-pair-1 summoner-spell-picker-pair">' +			
			'<button type="button" class="summoner-spell-picker-delete close">x</button>' +
		'</div>'),
			
		$dialog = $(
		'<div class="modal">' +
		  '<div class="modal-dialog summoner-spell-picker-dialog">' +
		    '<div class="modal-content">' +
		      '<div class="modal-header">' +
		        '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' +
		        '<h4 class="modal-title">Selecciona un par de hechizos</h4>' +
		      '</div>' +
		      '<div class="modal-body">' +
		        '<div class="summoner-spell-picker-dialog-spells"></div>' +
		      '</div>' +
		    '</div>' +
		  '</div>' +
		'</div>'),
	
		// Lazy initialized
		$dialogSpells = $(),
		selectedSpells = [];
	
	// Load the spell index
	selfSite.loldata.load({spell: true})
	.then(function(data){		
		var spellIds = $.map(data.spell, function(spell, id){ return id; });		
		
		selfSite.loldata.view(spellIds, 'icon', {size: 64}).done(function(spells){
			$dialog.find('.summoner-spell-picker-dialog-spells').append(spells);
			
			$dialogSpells = $dialog.find('[data-loldata]').attr('supress-tt', true);
			
			$dialog.on('click', '[data-loldata]', function(){			
				if($(this).hasClass('selected')){
					selectedSpells = [];
					$(this).removeClass('selected');
				} else if(selectedSpells.length == 1){
					$(this).addClass('selected');
					selectedSpells.push($(this));			
					$dialog.modal('hide');
				} else {
					selectedSpells.push($(this));
					$(this).addClass('selected');
				}
			});	
		});		
			
	}, function(xhr, err){
		console.log(err);
	});
			
	$dialog.appendTo(document.body).modal({
		show: false
	})
	
	.on('hide.bs.modal', function(){
		$dialogSpells.removeClass('selected');
		
		if(selectedSpells.length == 2){
			$dialog.data('callback')([selectedSpells[0].data('loldata'), selectedSpells[1].data('loldata')]);	
		}
							
		selectedSpells = [];	
	});
	
	function selectSpellsDialog(callback) {				
		$dialog.data('callback', callback);
		$dialog.modal('show');				
	}
		
	function createSpellSet(spells) {
		var $spellSet = $spellSetTemplate.clone();
			
		selfSite.loldata.view(spells, 'icon', {size: 64}).done(function(content){
			$spellSet.prepend(content).find('[data-loldata]').attr('supress-tt', true);
		});	
		
		$spellSet.data('spells', spells)			
		.click(function(){
			selectSpellsDialog(function(spells){
				createSpellSet(spells).replaceAll($spellSet);
			});				
		})
		
		.find('.summoner-spell-picker-delete')
		.click(function(){
			$spellSet.remove();
		});
		
		return $spellSet;
	}
		
	// Multiple spell-picker fields
	$('div.field-widget-summoner-spellfield-spell-picker div.summoner-spell-container').each(function(){	
		var $viewport = $(this);
		
		$viewport.sortable({
			cancel: '.summoner-spell-picker-delete',
			delay: 150,
			tolerance: 'pointer',
			helper: function(event, item) {
		      var $item = $(item);
		      return $item.clone().width($item.outerWidth()+.3).height($item.outerHeight());
		   	},		
		   	start: function(event, ui){
		   		ui.placeholder.height(ui.item.outerHeight());
		   		ui.placeholder.width(ui.item.outerWidth());
		   	}
		});
			
		// Initialize data
		var $valueInput = $viewport.find('input.summoner-spell-picker-hidden-input'),
		 	currentSpells = $.parseJSON($valueInput.val());
				
		// Render current elements
		for(var i = 0; i < currentSpells.length; ++i) {
			renderSpells(currentSpells[i]);
		}
				
		// Add more button
		$addMoreBtnTemplate.clone()
		.appendTo($viewport.parent())
		.find('button')
		.click(function(){
			selectSpellsDialog(renderSpells);
		});
						
		function renderSpells(spells){
			createSpellSet(spells).appendTo($viewport);
		}
		
		// Form handler
		$viewport.closest('form').submit(function(){
			var spells = [];
			$viewport.find('.summoner-spell-picker-pair').each(function(){
				spells.push($(this).data('spells'));
			});
			
			$valueInput.val(JSON.stringify(spells));		
		});	
		
	});

});})(jQuery);