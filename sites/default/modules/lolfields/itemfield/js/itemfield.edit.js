(function($){$(document).ready(function () {
	"use strict";
		
		// Config
	var maxItemsPerSet = 20,
		maxRepeatedItems = 5,
		maxLabelLength = 256,
		maxItemSetsPerCategory = 20,
		allowObsolete = false,
	
		// Item database, lazy loaded
		itemData,
	
		// Useful template elements
		$itemSetAddBtnTemplate = $('<button type="button" class="btn btn-primary btn-block">' + 
			'+ Objetos' //Drupal.t('+ Items') 
		+ '</button>'),	
	
		itemSetSeqClasses = ['lol-itemset-1-items-nonsequential', 'lol-itemset-1-items-sequential'],
	
		$itemSetTemplate = $('<div class="lol-itemset-1">' +		
			'<div class="lol-itemset-1-items"></div>' +
		  	'<div class="lol-itemset-1-label"></div>' +
		'</div>'),
	
		$itemSetDelBtnTemplate = $('<button type="button" class="close itemfield-picker-delset-btn">x</button>'),
	
		$tagTemplate = $('<div class="itemfield-picker-dialog-tag">' +
			'<label></label>' +
			'<input type="checkbox">' +		
		'</div>'),
	
		$listItemTemplate = $('<div class="itemfield-picker-dialog-list-item loldata-view-list">' +
			'<span class="itemfield-picker-dialog-item-icon"></span>' +
			'<div class="loldata-title"></div>' +
			'<div class="loldata-price"></div>' +
		'</div>'),
	
		$pickedItemTemplate = $('<img class="loldata-view-icon loldata-icon itemfield-picker-dialog-item" />'),
	
		$dialog = $('<div id="itemfield-picker-dialog" class="modal" role="dialog" aria-hidden="true">' + 
		  '<div class="modal-dialog">' +
		    '<div class="modal-content">' +
		      '<div class="modal-header">' +
		        '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' +
		        '<h4 class="modal-title">' + 'Selecciona los objetos del grupo' + '</h4>' +
		      '</div>' +
		      '<div class="modal-body clearfix">'+
		        '<div id="itemfield-picker-dialog-finder-container">' +
					'<div id="itemfield-picker-dialog-search">' +
						'<input id="itemfield-picker-dialog-searchbox" type="search" class="form-control input-lg" placeholder="' + 
						Drupal.t('Search Items') + '" />' +
						'<a id="itemfield-picker-dialog-clear" class="close" href="#">X</a>' +
					'</div>' +				
					'<div id="itemfield-picker-dialog-tags">' +				
					'</div>' +
					'<div id="itemfield-picker-dialog-item-list">' +				
					'</div>' +
				'</div>' +
				'<div id="itemfield-picker-dialog-editor-container">' +
					'<h5>' + Drupal.t('Selected items') + 
						' <a href="#" data-toggle="tooltip" title="' + 
							'Busca objetos en la lista de la izquierda, para agregarlos haz click sobre ellos, para ' +
							'removerlos haz click en los íconos de abajo.' +
						'"><i class="glyphicon glyphicon-question-sign"></i></a>' +
					'</h5>' +							
					'<div id="itemfield-picker-dialog-items"></div>' +
					'<h5>' + Drupal.t('Label') + 
						' <a href="#" data-toggle="tooltip" title="' + 
							'Usa este espacio para poner una descripción corta de los objetos seleccionados, por ejemplo: ' +
							'<b>Primera compra</b>, <b>Acarreando</b>, <b>Línea difícil</b>, ' +
							'<b>Opciones de resistencia Mágica</b>, <b>Siempre compra</b>.' +
						'"><i class="glyphicon glyphicon-question-sign"></i></a>' +
					'</h5>' +
					'<textarea id="itemfield-picker-dialog-label" class="form-control" maxlength="'+ maxLabelLength + '"' + 
						' placeholder="' + Drupal.t('Add a short description about these items') + '">' +
					'</textarea>' +						
					'<h5>Tipo' +
						' <a href="#" data-toggle="tooltip" title="' + 
							'Si lo deseas, puedes etiquetar al grupo de objetos para darle un efecto resaltado.' +
						'"><i class="glyphicon glyphicon-question-sign"></i></a>' +
					'</h5>' +		
					'<div id="itemfield-picker-dialog-type-container" class="btn-group btn-group-xs" data-toggle="buttons">' +				
						'<label class="btn btn-default">' + 
							'<input type="radio" value="neutral" checked="checked" name="itemfield-picker-dialog-type" />' 
							+ Drupal.t('Normal') +
						'</label>' +				
						'<label class="btn btn-danger">' +
							'<input type="radio" value="offensive" name="itemfield-picker-dialog-type" />' + Drupal.t('Offensive') +
						'</label>' +			
						'<label class="btn btn-success">' +
							'<input type="radio" value="defensive" name="itemfield-picker-dialog-type" />' + Drupal.t('Defensive') + 
						'</label>' +				
						'<label class="btn btn-warning">' +
							'<input type="radio" value="negative" name="itemfield-picker-dialog-type" />' + Drupal.t('Negative') + 
						'</label>' +
					'</div>' +	
					'<h5>Opciones</h5>' + 
					'<ul class="list-inline">' +
						'<li class="btn-group" data-toggle="buttons">' +
							'<label class="btn btn-default btn-sm">' +
								'<input type="checkbox" id="itemfield-picker-dialog-sequential">' +
								'<a href="#" data-toggle="tooltip" title="' + 
									'Marca esta casilla si los objetos seleccionados deberían ser comprados en orden.' +
								'"><i class="glyphicon glyphicon-question-sign"></i></a> ' +
								Drupal.t('Sequential') +
							'</label>' +
						'</li>' +
						'<li class="btn-group" data-toggle="buttons">' +
							'<label class="btn btn-default btn-sm">' +
								'<input type="checkbox" id="itemfield-picker-dialog-highlighted">' +
								'<a href="#" data-toggle="tooltip" title="' + 
									'Si marcas esta opción los objetos seleccionados serán mostrados en la vista rapida de tu guía, ' +
									'al igual que en los listados. Solo un grupo de objetos puede ser marcado como «Objetos finales» ' +
									'dentro la misma guía.' +
								'"><i class="glyphicon glyphicon-question-sign"></i></a> ' +
								'Objetos finales' +
							'</label>' +
						'</li>' +	
					'</ul>' +
				'</div>' +
		      '</div>' +
		      '<div class="modal-footer">' +
		        '<button id="itemfield-picker-dialog-cancelbtn" type="button" class="btn btn-default" data-dismiss="modal">' +
					 Drupal.t('Cancel') + '</button>' +
				'<button id="itemfield-picker-dialog-savebtn" type="button" class="btn btn-primary">' +
					 Drupal.t('OK') + '</button>' +
		      '</div>' +
		    '</div>' +
		  '</div>' +
		'</div>'),			

		// Cache elements of interest
		$dialogSearchBox = $dialog.find('#itemfield-picker-dialog-searchbox'),
		$dialogSearchClear = $dialog.find('#itemfield-picker-dialog-clear'),
		$dialogTagsCont = $dialog.find('#itemfield-picker-dialog-tags'),
		$dialogAllItemsCont = $dialog.find('#itemfield-picker-dialog-item-list'),
		$dialogItemsCont = $dialog.find('#itemfield-picker-dialog-items'),
		$dialogSequential = $dialog.find('#itemfield-picker-dialog-sequential'),
		$dialogHighlighted = $dialog.find('#itemfield-picker-dialog-highlighted'),
		$dialogLabel = $dialog.find('#itemfield-picker-dialog-label'), 
		$dialogType = $dialog.find('#itemfield-picker-dialog-type-container'),
		$dialogSaveBtn = $dialog.find('#itemfield-picker-dialog-savebtn'),
			
		// Lazy initialized
		$dialogTags = $(),
		$dialogAllItems = $(),
	
		// Search index definition
		index = new selfSite.loldata.SearchIndex('itemfield', ['item']);

	// Get item db after document has been fully loaded, we can do this because this widget is almost enterily 
	// rendered as a dialog and doesn't need to be rendered instantaneously
	$(window).load(function(){
		selfSite.loldata.load({
			item: true,
			itemcategories: true
		}).then(function(data){
			itemData = data.item;
			var tagData = data.itemcategories;	
			
			for(var i = 0, len = tagData.ordered.length; i < len; ++i) {
				var tag = tagData.ordered[i],
					tagId = tag.id,
					level = 0,
					$parent = $dialogTagsCont;
				
				if(!tag.visible) continue;							
				
				if(tag.parent){
					$parent = $dialogTagsCont.find('#itemfield-picker-dialog-tag-' + tag.parent.id);
					
					if($parent.length == 0) {
						continue;
					}
					
					level = $parent.data('tagLevel') + 1;
				}
				
				$tagTemplate.clone()
				.attr('id', 'itemfield-picker-dialog-tag-' + tagId)
				.addClass('itemfield-picker-dialog-tag-level-' + level)	
				.data('tagLevel', level)
				.find('label')
					.attr('for', 'itemfield-picker-dialog-tag-input-' + tagId)
					.text(tag.title)
				.end()
				.find('input')
					.attr('id', 'itemfield-picker-dialog-tag-input-' + tagId)
					.data('tagId', tagId)	
				.end()
				.appendTo($parent);
			}
					
			$dialogTags = $dialogTagsCont.find('input');		
			
			var itemIds = [],
				itemListCache = {},
				doIndex = !index.isFromCache;
				
			$.each(itemData, function(itemId, item){		
				if(item.obsolete && !allowObsolete) return;
				
				itemIds.push(itemId);	
							
				// Adds to index
				if(doIndex){
					selfSite.loldata.indexObject(index, item);
				}
							
				// Adds to view
				itemListCache[itemId] = $listItemTemplate.clone()			
				.data('item', item)	
				.find('img')
					.attr('src', Drupal.settings.basePath + item.icon)
				.end()			
				.find('.loldata-title')
					.text(item.title)
				.end()			
				.find('.loldata-price')
					.text(item.price)
				.end()
				.appendTo($dialogAllItemsCont);			
			});
			
			$dialogAllItems = $dialogAllItemsCont.children();
			
			selfSite.loldata.commitIndex(index);
			
			selfSite.loldata.view(itemIds, 'icon').done(function(icons){
				$(icons).each(function(){
					var $this = $(this);
					$this.appendTo(itemListCache[$this.data('loldata')].find('.itemfield-picker-dialog-item-icon'));
				});			
			});
			
		}, function(xhr, err){
			console.log(err);
		});
	});	
	
	$dialog.find('[data-toggle="tooltip"]').tooltip({
		 container: '#itemfield-picker-dialog > .modal-dialog',
		 html: true,
		 placement: 'right'
	 });

	$dialogItemsCont.sortable({
		delay: 150,
		tolerance: 'pointer',
		helper: 'clone'
	});	
	
	$dialogSaveBtn.click(function(){
		if($dialogItemsCont.children().length == 0){
			$.notify(Drupal.t('Select at least one item.'), 'error');
		} else {
			$dialog.data('save', true);	
			$dialog.modal('hide');		
		}
	});
		
	// Add new items to the item set
	$dialogAllItemsCont.on('click', '.itemfield-picker-dialog-list-item', function(){
		var $currentItems = $dialogItemsCont.find('img');
		
		if($currentItems.length < maxItemsPerSet){
			var thisItemId = $(this).data('item').id;
			var itemCount = {};
			
			$currentItems.each(function(){
				var itemId = $(this).data('itemId');
				typeof itemCount[itemId] != 'undefined'?  ++itemCount[itemId] : (itemCount[itemId]=1);
			});
			
			if(typeof itemCount[thisItemId] == 'undefined' || itemCount[thisItemId] < maxRepeatedItems) {
				renderPickedItem(thisItemId);
			} else {
				$.notify(Drupal.t('You can only select a maximum of !n items of the same type!', 
					{"!n": maxRepeatedItems}), 'error');
			}
			
		} else {
			$.notify(Drupal.t('This item set can\'t hold more items. Sorry.'), 'error');
		}
	});
	
	// Remove items from the item set
	$dialogItemsCont.on('click contextmenu', '.itemfield-picker-dialog-item', function(){
		$(this).remove();
		return false;
	});
	
	// Item search by text
	$dialogSearchBox.on('input keyup', function(){
		var timeout = $dialogSearchBox.data('timeout');
		
		if(timeout){
			clearTimeout(timeout);
		}
		
		$dialogSearchBox.data('timeout', setTimeout(function(){
			$dialogTags.prop('checked', false);
			var val = $.trim($dialogSearchBox.val());
			
			if(!val){
				$dialogAllItems.show();
				return;
			}
			
			var results = index.search(val);	
			
			if(results.length == 0){
				$dialogAllItems.hide();
			} else {			
				var foundIds = {};						
				for(var i = 0, len = results.length; i < len; ++i){
					foundIds[results[i].ref] = true;	
				}
			
				$dialogAllItems.each(function(){
					var $this = $(this);
					$this.toggle(!!foundIds[$this.data('item').id]);
				});			
			}
			
		}, 1));
	});
	
	// Clear searchbox
	$dialogSearchClear.click(function(e){
		e.preventDefault();
		$dialogSearchBox.val('').focus().trigger('input');
	});
	
	// Item search by tags
	var MODE_AND = 1, MODE_OR = 2;
	$dialogTagsCont.on('change', 'input', function(e){		
		var $input = $(this),
			isCheck = $input.prop('checked'),
			$tag = $input.closest('.itemfield-picker-dialog-tag'),
			mode,
			tags;
			
		$dialogSearchBox.val('');
		
		// Tag level 0 is treated as the 'category' of a tag, if user checks it, we use an OR combination
		// of its child tags and itself
		if($tag.data('tagLevel') === 0){
			if(isCheck){
				$dialogTags.prop('checked', false);			
				$input.prop('checked', true);				
				tags = $tag.find('input').map(getTagId).get();
				mode = MODE_OR;
			} else {
				tags = [];
			}			
		} else {
			isCheck && $dialogTagsCont.find('.itemfield-picker-dialog-tag-level-0 > input').prop('checked', false);			
			tags = $dialogTags.filter(':checked').map(getTagId).get();
			mode = MODE_AND;
		}
				
		var tagsLength = tags.length;
		
		if(tagsLength === 0){
			$dialogAllItems.show();
		} else {
			$dialogAllItems.each(function(){
				var $this = $(this);
				var itemTags = $this.data('item').categoryIds;
				
				if(mode === MODE_AND){
					if(tagsLength > itemTags.length){ 
						return $this.hide();
					} 
									
					for(var i = 0; i < tagsLength; ++i){						
						if($.inArray(tags[i], itemTags) === -1){
							return $this.hide();
						}		
					}
					
					$this.show();
				} else {
					for(var i = 0; i < tagsLength; ++i){						
						if($.inArray(tags[i], itemTags) !== -1){
							return $this.show();
						}		
					}
					
					$this.hide();
				}												
			});
		}
	});
	
	// Dialog operations
	$dialog.appendTo(document.body).modal({
		show: false
	})
		
	.on('shown.bs.modal', function(){
		$dialogSearchBox.focus();
	})
	
	.on('hide.bs.modal', function(){
		if(!$dialog.data('save'))
			return;
		
		$dialog.data('save', false);
		
		// Extract new values and send to callback
		var newValues = itemSetDefaultValues();
		
		newValues.type = $dialogType.find('input:checked').val() || newValues.type;
		newValues.label = $dialogLabel.val();
		newValues.sequential = $dialogSequential.prop('checked');				
		$dialogItemsCont.children().each(function(){
			newValues.items.push($(this).data('itemId'));
		});		
		
		if($dialogHighlighted.prop('checked')){
			newValues.highlighted = true;
		}
						
		$dialog.data('callback')(newValues);
	})
	
	.on('hidden.bs.modal', function(){
		// Reset interface for the next time we open the dialog
		$dialogSearchBox.val('');
		$dialogTags.prop('checked', false);
		$dialogAllItems.show();
		$dialogItemsCont.empty();	
	});
	
	// Shows the dialog
	function selectItemsDialog(values, callback) {
		// Set current values
		if(!values.type) values.type = itemSetDefaultValues().type;
		 
		var $type = $dialogType.find('input[value="' + values.type + '"]').parent();
		$type.hasClass('active') || $type.button('toggle'); 	
		$dialogSequential.prop('checked') === !!values.sequential || $dialogSequential.trigger('click');	
		$dialogHighlighted.prop('checked') === !!values.highlighted || $dialogHighlighted.trigger('click');
		$dialogLabel.val(values.label);
		$.each(values.items, function(i, itemId){ renderPickedItem(itemId); });					
					
		// Save the callback
		$dialog.data('callback', callback);
		
		$dialog.modal('show');	
	}
	
	function itemSetDefaultValues(){
		return {
			type: 'neutral',
			items: [],
			sequential: false,
			label: ''
		};
	}
	
	function renderPickedItem(itemId) {
		var item = itemData[itemId];
		
		$pickedItemTemplate.clone()
		.data('itemId', itemId)
		.attr('src', Drupal.settings.basePath + item.icon)
		.appendTo($dialogItemsCont);
	}
	
	function updateItemSetValues($itemSet, values) {
		$itemSet.attr('class', 'lol-itemset-1  lol-itemset-1-type-' + values.type);
		$itemSet.find('.lol-itemset-1-items').removeClass(itemSetSeqClasses.join(' '))
			.addClass(itemSetSeqClasses[+values.sequential]);
		$itemSet.find('.lol-itemset-1-label').text(values.label);	
		
		var $itemContainer = $itemSet.find('.lol-itemset-1-items');
		$itemContainer.empty();
		
		selfSite.loldata.view(values.items, 'icon').done(function(items){
			$itemContainer.append(items);
		});
		
		$itemSet.data('itemSet', values);
	}
	
	function getTagId(i, tag){
		return $(tag).data('tagId');
	}

	// Handling of the actual fields	
	$('.field-widget-itemfield-item-picker').each(function(){
		var $viewport = $(this).find('.itemfield-picker-container'),
			$valueInput = $(this).find('.itemfield-picker-value'),
			$categories = $viewport.find('.lol-itemsets-view-1-category-container').wrapInner('<div>').find('> div'),
			data = $.parseJSON($valueInput.val()),
			currHighlighted;
		
		// Ensure enough space for all categories		
		for(var i = data.length; i < $categories.length; ++i){
			data.push([]);
		}		
	
		$categories.each(function(categoryIndex){
			var $thisCategory = $(this),
				categoryData = data[categoryIndex];
							
			// Add '+ Items' button to each category
			var $itemSetAddBtn = $itemSetAddBtnTemplate.clone()
			.insertAfter($thisCategory)	
			.click(function(){	
				if(categoryData.length == maxItemSetsPerCategory){
					$.notify(Drupal.t('This item category can\'t hold more items. Sorry.'), 'error');
					return;	
				}				
				
				selectItemsDialog(itemSetDefaultValues(), function(itemSetValues){
					categoryData.push(itemSetValues);
					createNewItemSet(itemSetValues);
					checkHighlighted(itemSetValues);
				});				
			});
			
			// Editing itemsets
			$thisCategory
			.on('click', '.lol-itemset-1', function(){
				var $thisItemSet = $(this);
				
				selectItemsDialog(categoryData[$thisItemSet.index()], function(itemSetValues){	
					categoryData[$thisItemSet.index()] = itemSetValues;			
					updateItemSetValues($thisItemSet, itemSetValues);
					checkHighlighted(itemSetValues);					
				});				
			})
			
			// Deleting itemsets
			.on('click', '.itemfield-picker-delset-btn', function(e){
				var $itemSet = $(this).closest('div.lol-itemset-1');
				categoryData.splice($itemSet.index(), 1);
				$itemSet.remove();
				
				e.stopPropagation();
			})
			
			.sortable({
				delay: 150,
				items: '> .lol-itemset-1',
				tolerance: 'pointer',
				//containment: $viewport,	
				connectWith: $categories,
				opacity: 0.7,
				helper: 'clone',
				
				start: function(event, ui){
					ui.placeholder.height(ui.helper.outerHeight());	
					ui.placeholder.width(ui.helper.outerWidth());	
				},
				
				update: function(event, ui){
					categoryData.length = 0;
					$('.lol-itemset-1', $thisCategory).each(function(i){
						categoryData.push($(this).data('itemSet'));
					});
				}			
			});
			
			// Initial item sets			
			for(var i = 0, len = categoryData.length; i < len; ++i){
				createNewItemSet(categoryData[i]);
				checkHighlighted(categoryData[i]);
			}
			
			function createNewItemSet(itemSetValues) {												
				updateItemSetValues(
					$itemSetTemplate.clone()
					.prepend($itemSetDelBtnTemplate.clone())
					.appendTo($thisCategory)
				, itemSetValues);
			}
			
			function checkHighlighted(itemSetValues) {
				if(itemSetValues.highlighted && currHighlighted !== itemSetValues){
					if(currHighlighted) delete currHighlighted.highlighted;
					currHighlighted = itemSetValues;										
				}
			}
		});		
		
		// Commit data on submit		
		$valueInput.closest('form').submit(function(){
			$valueInput.val(JSON.stringify(data));
		});
	});
	
});})(jQuery);