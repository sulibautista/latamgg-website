<?php
/**
 * Implements hook_field_info(). 
 */
function itemfield_field_info() {
  return array(
    'item' => array(
      'label' => t('LoL Items'),
      'description' => t('League of Legends guides items field.'),
      'instance_settings' => array(
        'accept_obsolete' => false
      ),
      'default_widget' => 'itemfield_item_picker',
      'default_formatter' => 'itemfield_items_formatter',
      'no_ui' => FALSE    
    )  
  );
}

/**
 * Implements hook_field_schema().
 */
function itemfield_field_schema($field) {
  $schema = array(
    'columns' => array(
      'data_version' => array(
        'type' => 'int',
        'not null' => TRUE,
        'unsigned' => TRUE
      ),
      'value' => array(
        'type' => 'text',
        'size' => 'normal'
      )
    )
  ); 
  
  return $schema;
}

function itemfield_theme($existing, $type, $theme, $path) {
  return array(
    'lol_itemset_1' => array(
      'variables' => array(
        'label' => '',
        'items' => array(),
        'type' => 'neutral', // neutral, defensive, offensive, negative
        'sequential' => FALSE,
        'reduce' => TRUE     
      )
    ),
    
    'lol_itemsets_view_1' => array(
      'variables' => array(
        'attributes' => array(),
        'itemsets' => array(),
        'show_empty' => FALSE
      ),
    )
  );
}

function theme_lol_itemset_1($vars) {
  $ret = '<div class="lol-itemset-1 lol-itemset-1-type-'. check_plain($vars['type']) .'">' .
    '<div class="lol-itemset-1-items lol-itemset-1-items-'. ($vars['sequential']? 'sequential' : 'nonsequential') . '">' .
      theme('loldata_icon', array('ids' => $vars['items'], 'reduce' => $vars['reduce'])) .
    '</div>' .
    '<div class="lol-itemset-1-label">' . check_plain($vars['label']) . '</div>'. 
  '</div>';
 
  return $ret;
}

function theme_lol_itemsets_view_1($vars) {
  $vars['attributes']['class'][] = 'lol-itemsets-view-1-container';
  
  $ret = '<div' . drupal_attributes($vars['attributes']) . '>';
  $headers = array(t('Starting'), t('Essential'), t('Situational'), t('Negative'));   
  
  foreach($headers as $idx => $header) {
    if(!empty($vars['itemsets'][$idx]) || $vars['show_empty']){    
      $ret .= '<div class="lol-itemsets-view-1-category">' .
        '<h3 class="lol-itemsets-view-1-category-header">' . $header . '</h3>' .
        '<div class="lol-itemsets-view-1-category-container">';
        
        if(!empty($vars['itemsets'][$idx])){
          foreach($vars['itemsets'][$idx] as $itemset) {
            $ret .= theme('lol_itemset_1', $itemset);
          }      
        }
      
      $ret .= '</div></div>';
    } 
  }
  
  $ret .= '</div>';
  
  return $ret;
}

function itemfield_field_is_empty($item, $field) {
  if(empty($item['data_version']) || !isset($item['value']) || !json_decode($item['value']))
    return TRUE;
    
  return FALSE;
}

function itemfield_field_is_valid_data_version($version) {
  return $version == 1; 
} 

/**
 * Implements hook_field_widget_info().
 */
function itemfield_field_widget_info() {
  return array(
    'itemfield_item_picker' => array(
      'label' => t('Items picker'),
      'field types' => array('item'),
      'descritpion' => t('Interactive editor for LoL items.'),      
    )
  ); 
}

/**
 * Implements hook_field_widget_form().
 */
function itemfield_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) { 
  $data_version = isset($items[$delta]['data_version'])? $items[$delta]['data_version'] : 1;
  $json_value = isset($items[$delta]['value'])? $items[$delta]['value'] : json_encode(array());
  
  $element['items'] = array(
    '#theme' => 'lol_itemsets_view_1',
    '#attributes' => array('class' => array('itemfield-picker-container')),
    '#show_empty' => TRUE
    //'#itemsets' => json_decode($json_value, true)
  );
  
  $element['data_version'] = array(
    '#type' => 'value',
    '#value' => $data_version
  );

  $element['value'] = array(
    '#type' => 'hidden',
    '#attributes' => array(
      'class' => array('itemfield-picker-value'),
      'data-data_version' => $data_version
    ),    
    '#default_value' => $json_value,    
    '#attached' => array(
      'js' => array(drupal_get_path('module', 'itemfield') . '/js/itemfield.edit.js'),
      'css' => array(
        drupal_get_path('module', 'itemfield') . '/css/itemfield.view.css',
        drupal_get_path('module', 'itemfield') . '/css/itemfield.edit.css'        
      ),
      'library' => array(
        array('system', 'ui.sortable'),
        array('notifyjs', 'notify'),
        array('loldata', 'loldata'),
        array('loldata', 'loldata.lunr')
      )
    ),
  );
    
  return $element;
}

/**
 * Implements hook_validate().
 * Validates the field supplied values. We don't set many meaningful error messages as the item picker should limit
 * the user input to valid values. Just take care of possible malicious data.
 */
function itemfield_field_validate($entity_type, $entity, $field, $instance, $langcode, $items, &$errors) {
  foreach ($items as $delta => $item) {
    $version = empty($item['data_version'])? false : $item['data_version'];
    $value = empty($item['value'])? false : json_decode($item['value'], false, 6);
    $m = NULL;
    
    // Allow full emptiness
    if(!$version && $value === false){
      continue;    
    }
    
    if(!$version || !$value){
      $e = 'missing_data'; goto err;
    }
    
    if(!itemfield_field_is_valid_data_version($version)){
      $e = 'invalid_version'; goto err;
    }
    
    if(!is_array($value)) {
      $e = 'invalid_value_type'; goto err;
    }    
    
    if(count($value) != 4){
      $e = 'invalid_category_count'; goto err;
    }
    
    $valid_types = array('neutral', 'offensive', 'defensive', 'negative');
    $highlighted_set = NULL;

    $all_items = array();
    foreach($value as $category){
      if(!is_array($category)){
        $e = 'invalid_category_type'; goto err;
      }
      
      if(count($category) > 20) {
        $e = 'invalid_category_length'; goto err;
      }
      
      foreach($category as $set) {
        if(!is_object($set)){
          $e = 'invalid_itemset_type'; goto err;
        }
        
        // Set
        if(empty($set->type) || !is_string($set->type) || !in_array($set->type, $valid_types)){
          $e = 'invalid_itemsettype_value'; goto err;
        }
        
        // Order
        if(!isset($set->sequential) || !is_bool($set->sequential)){
          $e = 'invalid_sequential_value'; goto err;
        }
        
        // Label
        if(!isset($set->label) || !is_string($set->label) || drupal_strlen($set->label) > 256){
          $e = 'invalid_label_value'; goto err;
        }
        
        // Highlighted
        if(isset($set->highlighted)){
          if($set->highlighted !== TRUE || $highlighted_set){
            $e = 'invalid_highlighted_value'; goto err;
          } else {
            $highlighted_set = TRUE; // Only allow one highlighted set
          }
        }
        // Items
        if(empty($set->items) || !is_array($set->items) || count($set->items) > 20 ){
          $e = 'invalid_items_value'; goto err;         
        }
        
        foreach(array_count_values($set->items) as $item_id => $count) {
          if(!is_int($item_id) || $count > 5){
            $e = 'invalid_items_repeated_count'; goto err;  
          }
        }       
        
        $all_items = array_merge($all_items, $set->items);          
      }
    }
    
    list($valid_items, $obsolete) = lolfields_validate_obsolete('item', $all_items);
    
    if($valid_items){
      if($obsolete && !$instance['settings']['accept_obsolete']){
        $e = 'obsolete_items';
        $m = t('The items %items are obsolete and can\'t be used in the items section. Please remove them.', 
            array('%items' => loldata_concat_titles(loldata_load_multiple($obsolete))));
        goto err;
      }
    } else{
      $e = 'invalid_items_id'; goto err;  
    }
    
    // All good
    continue;

    err:
    lolfields_field_error($entity_type, $entity, $field, $langcode, $errors, $delta, 'itemfield', $e, $m);
  }
}

/**
 * Impelements hook_field_formatter_info().
 */
function itemfield_field_formatter_info() {
  return array(
    'itemfield_items_formatter' => array(
      'label' => t('Items formatter'),
      'field types' => array('item')
    ),
    'itemfield_highlighted_formatter' => array(
      'label' => t('Highlighted Item set'),
      'field types' => array('item')
    )
  );
}

/**
 * Implements hook_field_formatter_view().
 */
function itemfield_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();
  
  switch ($display['type']) {
    case 'itemfield_items_formatter':
      foreach($items as $delta => $item) {
        $element[$delta] = array(
          '#theme' => 'lol_itemsets_view_1',
          '#attributes' => array('class' => array('itemfield-formatter-container')),
          '#itemsets' => json_decode($item['value'], true),
          '#attached' => array('css' => array(drupal_get_path('module', 'itemfield') . '/css/itemfield.view.css')),
        );
      }
      break;
      
    case 'itemfield_highlighted_formatter':
      foreach($items as $delta => $item) {
        $value = json_decode($item['value']);
        $highlighted = array();
        
        foreach($value as $category) {
          foreach($category as $set) {
            if(!empty($set->highlighted)){
              foreach($set as $key => $value){
                $highlighted['#' . $key] = $value;
              }
              
              // FIXME This shouldn't be hardcoded
              $highlighted['#items'] = array_slice($highlighted['#items'], 0, 6);
              break;
            }
          }
        }
              
        $element[$delta] = array(
          '#theme' => 'lol_itemset_1',
          '#theme_wrappers' => array('container'),
          '#attributes' => array('class' => 'itemfield-highlighted-formatter'),
          '#reduce' => FALSE
        ) + $highlighted;
      }
      break;    
  }

  return $element;
}