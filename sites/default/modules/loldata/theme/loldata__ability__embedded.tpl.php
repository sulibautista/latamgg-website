<div class="<?php echo $classes; ?>"<?php print $attributes; ?>>
  <h3><?php echo render($content['field_loldata_title']); ?></h3>  
  <div class="content">
    <?php echo render($content['field_loldata_icon']); ?>
    <div>
      <?php echo render($content['field_loldata_effect']); ?>
      <?php if($content['field_loldata_ability_number']['#items'][0]['value'] != 1):
        echo render($content['field_loldata_ability_cost']);
        echo render($content['field_loldata_cooldown']); 
        echo render($content['field_loldata_range']); 
      endif ?>
    </div>
  </div>
</div>