<div class="<?php print $classes; ?>"<?php print $attributes; ?>>
  <h4><?php echo $title; ?></h4>  
  <div class="content">
    <?php echo render($content['field_loldata_description']); ?> 
    <span class="loldata-tt-price">
      <?php echo render($content['field_loldata_price']), render($content['field_loldata_upgrade_price']); ?>
    </span> 
  </div>
</div>