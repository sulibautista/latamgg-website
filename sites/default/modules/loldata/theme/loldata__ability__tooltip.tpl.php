<div class="<?php echo $classes; ?>"<?php print $attributes; ?>>
  <h4><?php echo $title; ?></h4>  
  <div class="content">
    <?php echo render($content['field_loldata_effect']); ?> 
    <?php if($content['field_loldata_ability_number']['#items'][0]['value'] != 1): ?>
      <span class="loldata-tt-cost"><?php echo render($content['field_loldata_ability_cost']); ?></span> 
      <span class="loldata-tt-cooldown"><?php echo render($content['field_loldata_cooldown']); ?></span>
      <span class="loldata-tt-range"><?php echo render($content['field_loldata_range']); ?></span>
    <?php endif ?>
  </div>
</div>
