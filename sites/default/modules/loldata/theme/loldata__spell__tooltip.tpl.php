<div class="<?php echo $classes; ?>"<?php print $attributes; ?>>
  <h4><?php echo $title; ?></h4>  
  <div class="content">
    <?php echo render($content['field_loldata_description']); ?> 
    <span class="loldata-tt-cooldown"><?php echo render($content['field_loldata_cooldown']); ?></span>
    <span class="loldata-tt-range"><?php echo render($content['field_loldata_range']); ?></span>
  </div>
</div>
