<?php
/**
 * @file
 * Class for the Panelizer loldata entity plugin.
 */

/**
 * Panelizer Entity node plugin class.
 *
 * Handles loldata specific functionality for Panelizer.
 */
class LoLDataEntityLoLData extends PanelizerEntityDefault {
  public $supports_revisions = TRUE;
  public $entity_admin_root = 'admin/structure/loldata/manage/%';
  public $entity_admin_bundle = 4;
  public $views_table = 'loldata';
  public $uses_page_manager = TRUE;

  public function entity_access($op, $entity) {
    return loldata_access($op, $entity);
  }

  public function entity_save($entity) {
    entity_get_controller('loldata')->save($entity);
  }

  public function entity_identifier($entity) {
    return t('This LoL Data');
  }

  public function entity_bundle_label() {
    return t('LoLData type');
  }

  function get_default_display($bundle, $view_mode) {
    return parent::get_default_display($bundle, $view_mode);
  }
}
