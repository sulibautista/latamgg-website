<?php
/**
 * @file
 * Definition of the loldata plugin.
 */

$plugin = array(
  'handler' => 'LoLDataEntityLoLData',
  'entity path' => 'loldata/%loldata',
  'uses page manager' => TRUE,
  'hooks' => array(
    'menu' => TRUE,
    'admin_paths' => TRUE,
    'permission' => TRUE,
    'panelizer_defaults' => TRUE,    
    'form_alter' => TRUE,    
    'views_data_alter' => TRUE,
    //'page_alter' => TRUE,
    //'default_page_manager_handlers' => TRUE,
    //'views_plugins_alter' => TRUE,
  ),
);
