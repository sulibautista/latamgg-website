<?php

/**
 * @file
 * Handle the 'loldata view' override task.
 *
 * This plugin overrides loldata/%loldata and reroutes it to the page manager, where
 * a list of tasks can be used to service this request based upon criteria
 * supplied by access plugins.
 */

/**
 * Specialized implementation of hook_loldata_panels_task_tasks(). See api-task.html for
 * more information.
 */
function loldata_loldata_view_page_manager_tasks() {
  return array(
    // This is a 'page' task and will fall under the page admin UI
    'task type' => 'page',

    'title' => t('LoLData template'),

    'admin title' => t('LoLData template'),
    'admin description' => t('When enabled, this overrides the default behavior for displaying LoL Data elements at <em>loldata/%loldata</em>. If you add variants, you may use selection criteria such as loldata type or language or user access to provide different views of elements. If no variant is selected, the default LoL Data view will be used. This page only affects LoL Data elements viewed as pages, it will not affect loldatas viewed in lists or at other locations.'),
    'admin path' => 'loldata/%loldata',

    // Menu hooks so that we can alter the loldata/%loldata menu entry to point to us.
    'hook menu' => 'loldata_loldata_view_menu',
    'hook menu alter' => 'loldata_loldata_view_menu_alter',

    'handler type' => 'context',
    'get arguments' => 'loldata_panels_loldata_view_get_arguments',
    'get context placeholders' => 'loldata_panels_loldata_view_get_contexts',

    // Allow this to be enabled or disabled:
    'disabled' => variable_get('loldata_panels_loldata_view_disabled', TRUE),
    'enable callback' => 'loldata_panels_loldata_view_enable',
    'access callback' => 'loldata_panels_loldata_view_access_check',
  );
}

/**
 * Callback defined by loldata_panels_loldata_view_loldata_panels_tasks().
 *
 * Alter the loldata view input so that loldata view comes to us rather than the
 * normal loldata view process.
 */
function loldata_loldata_view_menu_alter(&$items, $task) {
  if (variable_get('loldata_panels_loldata_view_disabled', TRUE)) {
    return;
  }
  
  // Override the loldata view handler for our purpose.
  $items['loldata/%loldata']['page callback'] = 'loldata_panels_loldata_view_page';
  $items['loldata/%loldata']['file path'] = $task['path'];
  $items['loldata/%loldata']['file'] = $task['file'];
  
  // TODO remove once we dont use entity operations
  $items['loldata/%loldata']['page arguments'] = array(1);
}

/**
 * Entry point for our overridden loldata view.
 *
 * This function asks its assigned handlers who, if anyone, would like
 * to run with it. If no one does, it passes through loldata view.
 */
function loldata_panels_loldata_view_page($loldata){
  // Load my task plugin
  $task = page_manager_get_task('loldata_view');

  // Load the loldata into a context.
  ctools_include('context');
  ctools_include('context-task-handler');
  
  drupal_set_title(entity_label('loldata', $loldata)); 
  $uri = entity_uri('loldata', $loldata);
 
  $contexts = ctools_context_handler_get_task_contexts($task, '', array($loldata));
  $output = ctools_context_handler_render($task, '', $contexts, array(entity_id('loldata', $loldata)));
  if ($output != FALSE) {
    return $output;
  }
  
  // Otherwise, fall back.
  return loldata_view(array($loldata), 'full', NULL, TRUE);  
}

/**
 * Callback to get arguments provided by this task handler.
 *
 * Since this is the loldata view and there is no UI on the arguments, we
 * create dummy arguments that contain the needed data.
 */
function loldata_panels_loldata_view_get_arguments($task, $subtask_id) {
  return array(
    array(
      'keyword' => 'loldata',
      'identifier' => t('LoLData element being viewed'),
      'id' => 1,
      'name' => 'entity_id:loldata',
      'settings' => array(),
    ),
  );
}

/**
 * Callback to get context placeholders provided by this handler.
 */
function loldata_panels_loldata_view_get_contexts($task, $subtask_id) {
  return ctools_context_get_placeholders_from_argument(loldata_panels_loldata_view_get_arguments($task, $subtask_id));
}

/**
 * Callback to enable/disable the page from the UI.
 */
function loldata_panels_loldata_view_enable($cache, $status) {
  variable_set('loldata_panels_loldata_view_disabled', $status);

  // Set a global flag so that the menu routine knows it needs
  // to set a message if enabling cannot be done.
  if (!$status) {
    $GLOBALS['loldata_panels_enabling_loldata_view'] = TRUE;
  }
}

/**
 * Callback to determine if a page is accessible.
 *
 * @param $task
 *   The task plugin.
 * @param $subtask_id
 *   The subtask id
 * @param $contexts
 *   The contexts loaded for the task.
 * @return
 *   TRUE if the current user can access the page.
 */
function loldata_panels_loldata_view_access_check($task, $subtask_id, $contexts) {
  $context = reset($contexts);
  return loldata_access('view', $context->data);
}
