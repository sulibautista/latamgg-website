<?php

/**
 * Returns the Field instances for each bundle of the LoLData entity
 */
function _loldata_fields_per_bundle(){
  return array(
    'champion' => array(
      'title',
      'icon', //
      'champion_name',
      'champion_title',      
      'portrait', 
      'splash', 
      'description',
      'champion_tag',
      'colloq',     
      'range'
    ),
    'item' => array(
      'title',
      'icon', //      
      'price', 
      'upgrade_price',
      'sellsfor',
      'description',   
      'recipe',
      'item_category',
      'colloq',
      //maps
    ),
    'ability' => array(
      'title',
      'icon', //
      'ability_number',
      'champion',            
      'description',
      'effect',
      'ability_cost',
      'cooldown',   
      'range',  
      //ranks 
    ),          
    'mastery' => array(
      'title',
      'icon',
      'description',
      //'ranks',
      //'parent',
    ),
    'spell' => array(
      'title',
      'icon',
      'description',
      'cooldown',
      'range',
      //type
      //level
      //map_category   
    ),
    'rune' => array(
      'title',
      'icon',
      'description',
      'rune_category'
    ),
    'skin' => array(
      'title',
      'champion',
      'skin_name',
      'portrait',
      'splash',
    ),
    //profileicon
  );
}

/**
 * Implements hook_install().
 */
function loldata_install() {
  if (module_exists('entitycache')) {
    $entity_type = 'loldata';
    $table = 'cache_entity_' . $entity_type;
    if (!db_table_exists($table)) {
      $schema = drupal_get_schema_unprocessed('system', 'cache');
      $schema['description'] = 'Cache table used to store' . $entity_type . ' entity records.';
      db_create_table($table, $schema);
    }
  }

  // Create item category vocabulary
  $categoriesVocabulary = (object)array(
    'name' => 'LoLData Item Categories',
    'machine_name' => 'loldata_item_categories',
    'description' => st('Item categories like movement, health, etc.'),
    'module' => 'loldata'
  );  
  taxonomy_vocabulary_save($categoriesVocabulary);
  
  // Add lol_id / show field 
  field_create_field(array(
    'field_name' => 'field_loldata_itemcat_lolid',
    'type' => 'number_integer',
  ));
  
  field_create_field(array(
    'field_name' => 'field_loldata_itemcat_show',
    'type' => 'list_boolean',
    'settings' => array(
      'allowed_values' => array(0, 1),    
    )
  ));

  field_create_instance(array(
    'label' => 'LoL Id',
    'field_name' => 'field_loldata_itemcat_lolid',
    'entity_type' => 'taxonomy_term',
    'bundle' => 'loldata_item_categories'  
  ));
  
  field_create_instance(array(
    'label' => 'Visible in item search',
    'field_name' => 'field_loldata_itemcat_show',
    'entity_type' => 'taxonomy_term',
    'bundle' => 'loldata_item_categories'  
  ));
  
  // Add file directory
  $directory = file_default_scheme() . '://loldata';
  file_prepare_directory($directory, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS);
  
  // Create all the fields we are adding to our bundles
  foreach (_loldata_installed_fields() as $field) {
    field_create_field($field);
  }

  // Create all the instances for our fields
  $instances = _loldata_installed_instances();
  foreach (_loldata_fields_per_bundle() as $bundle => $field_names) {
    foreach($field_names as $field_name){
      $instances[$field_name]['entity_type'] = 'loldata';
      $instances[$field_name]['bundle'] = $bundle;
      field_create_instance($instances[$field_name]);      
    }    
  }
}

/**
 * Implements hook_uninstall().
 */
function loldata_uninstall() {
  taxonomy_vocabulary_delete(taxonomy_vocabulary_machine_name_load('loldata_item_categories')->vid);
    
  
  foreach(_loldata_installed_fields() as $field) {
    field_delete_field($field['field_name']);
  }

  field_purge_batch(1000);
  
  // field instances are not going to be deleted on uninstall @see node/1199946, 
  drupal_set_message(t('You need to remove remaining field instances for loldata entities in field_config_instance table.'));
}


/**
 * Implements hook_schema().
 */
function loldata_schema() {
  $schema = array();

  $schema['loldata'] = array(
    'description' => 'The base entity that stores LoL data.',
    'fields' => array(
      'loldata_id' => array(
        'description' => 'Primary key.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'lol_id' => array(
        'description' => 'The internal LoL client id for this data.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => FALSE,
      ),
      'vid' => array(
        'description' => 'The version of the data.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE
      ),
      'type' => array(
        'description' => 'The type of data.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),      
      'created' => array(
        'description' => 'The Unix timestamp when the data was added.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'changed' => array(
        'description' => 'The Unix timestamp when the data was most recently changed.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'updated' => array(
        'description' => 'The Unix timestamp when the data was most recently automatically updated.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'obsolete' => array(
        'description' => 'A boolean indicating if the data is no longer relevant.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0
      ),
      'lolversion' => array(
        'description' => 'The LoL version this data is from.',
        'type' => 'varchar',
        'length' => '10',
        'not null' => FALSE,
      )
    ),
    'primary key' => array('loldata_id'),
    'indexes' => array(
      'type' => array('type'),
      //'lol_id' => array('lol_id'),
      'vid' => array('vid'),
      'obsolete' => array('obsolete')
    ),
  );
  
  
  $schema['loldata_revision'] = array(
    'description' => 'The base entity that stores LoL data revisions.',
    'fields' => array(
      'loldata_id' => array(
        'description' => 'The data this version belongs to.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'lol_id' => array(
        'description' => 'The internal LoL client id for this data.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'vid' => array(
        'description' => 'The primary identifier for this version.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE
      ),
      'type' => array(
        'description' => 'The type of data.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'created' => array(
        'description' => 'The Unix timestamp when the data was added.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      )
    ),
    'primary key' => array('vid'),
    'indexes' => array(
      'loldata_id' => array('loldata_id'),
      'lol_id' => array('lol_id')
    ),
  );
  
  return $schema;
}

/**
 * Returns the module's custom Fields.
 */
function _loldata_installed_fields() {
  return array(  
    array(
      'field_name' => 'field_loldata_title',
      'settings' => array(
        'max_length' => '255',
      ),
      'type' => 'text',
      'translatable' => '1'
    ),
  
    array(
      'field_name' => 'field_loldata_icon',      
      'settings' => array(
        'uri_scheme' => 'public',
        'default_image' => 0,
      ),
      'type' => 'image',
    ),
    
    array(
      'field_name' => 'field_loldata_champion_name',
      'settings' => array(
        'max_length' => '255',
      ),
      'type' => 'text'
    ),
    
    array(
      'field_name' => 'field_loldata_champion_title',
      'settings' => array(
        'max_length' => '255',
      ),
      'type' => 'text',
      'translatable' => '1',
    ),
    
    array(
      'field_name' => 'field_loldata_description',          
      'type' => 'text_long',
      'translatable' => '1',
    ),  
    
    array(
      'field_name' => 'field_loldata_champion_tag',
      'translatable' => '1',      
      'settings' => array(
        'max_length' => '255',
      ),            
      'type' => 'text',
    ),
    
    array(
      'field_name' => 'field_loldata_price',
      'type' => 'number_integer',   
    ),
    
    array(  
      'field_name' => 'field_loldata_recipe',    
      'settings' => array(
        'target_type' => 'loldata',
        'handler' => 'base',
        'handler_settings' => array(
          'target_bundles' => array(
            'item' => 'item',
          ),
          'sort' => array(
            'type' => 'none',
          ),
          'behaviors' => array(
            'test_field_behavior' => array(
              'status' => 0,
            ),
          ),
        ),
      ),       
      'type' => 'entityreference',
      'cardinality' => '-1',
    ),
   
    array(
      'field_name' => 'field_loldata_ability_number',
      'type' => 'number_integer',   
    ),
    
    array(
      'field_name' => 'field_loldata_champion',    
      'settings' => array(
        'target_type' => 'loldata',
        'handler' => 'base',
        'handler_settings' => array(
          'target_bundles' => array(
            'item' => 'champion',
          ),
          'sort' => array(
            'type' => 'none',
          ),
          'behaviors' => array(
            'test_field_behavior' => array(
              'status' => 0,
            ),
          ),
        ),
      ),       
      'type' => 'entityreference',
    ),   
    
    array(
      'field_name' => 'field_loldata_ability_cost',
      'settings' => array(
        'max_length' => '255',
      ),
      'type' => 'text',
      'translatable' => '1',
    ), 
    
    array(
      'field_name' => 'field_loldata_cooldown',
      'settings' => array(
        'max_length' => '255',
      ),
      'type' => 'text'
    ), 
    
    array(
      'field_name' => 'field_loldata_effect',          
      'type' => 'text_long',
      'translatable' => '1',
    ),  
    
    array(
      'field_name' => 'field_loldata_item_category',
      'cardinality' => FIELD_CARDINALITY_UNLIMITED,          
      'type' => 'taxonomy_term_reference',
      'settings' => array(
        'allowed_values' => array(
          array(
            'vocabulary' => 'loldata_item_categories',
            'parent' => '0',
          ),
        ),
      ),     
    ),
    
    array(
      'field_name' => 'field_loldata_upgrade_price',
      'type' => 'number_integer',   
    ),  
    
    array(
      'field_name' => 'field_loldata_colloq',
      'type' => 'text',
      'settings' => array(
        'max_length' => '255',
      ),
      'translatable' => '1'
    ),
    
    array(
      'field_name' => 'field_loldata_range',
      'type' => 'number_integer'  
    ),
    
    array(
      'field_name' => 'field_loldata_sellsfor',
      'type' => 'number_integer'  
    ),
    
    array(
      'field_name' => 'field_loldata_rune_category',
      'cardinality' => FIELD_CARDINALITY_UNLIMITED,          
      'type' => 'taxonomy_term_reference',
      'settings' => array(
        'allowed_values' => array(
          array(
            'vocabulary' => 'loldata_rune_categories',
            'parent' => '0',
          ),
        ),
      ),
    ),
    
    array(
      'field_name' => 'field_loldata_portrait',
      'settings' => array(
        'uri_scheme' => 'public',
        'default_image' => 0,
      ),
      'type' => 'image',
    ),
    
    array(
      'field_name' => 'field_loldata_splash',
      'settings' => array(
        'uri_scheme' => 'public',
        'default_image' => 0,
      ),
      'type' => 'image',
    ),
    
    array(
      'field_name' => 'field_loldata_skin_name',
      'settings' => array(
        'max_length' => '255',
      ),
      'type' => 'text',
      'translatable' => '1'
    ),
  );
}

/**
 * Returns the module's Field instances.
 */
function _loldata_installed_instances() {
  return array(
    'title' => array(
      'field_name' => 'field_loldata_title',
      'required' => 1,
      'display' => array(
        'default' => array(
          'label' => 'hidden'
        ),
      ),
    ),
  
    'icon' => array(
      'field_name' => 'field_loldata_icon',      
      'settings' => array(
        'file_directory' => '',
        'file_extensions' => 'png gif jpg jpeg',
        'max_filesize' => '',
        'max_resolution' => '',
        'min_resolution' => '',
        'alt_field' => 0,
        'title_field' => 0,
        'default_image' => 0,
        'user_register_form' => FALSE,
      ),
      'display' => array(
        'default' => array(
          'label' => 'hidden'
        ),
      ),
      //'required' => 1,
    ),
    
    'champion_name' => array(
      'field_name' => 'field_loldata_champion_name',
      'required' => 1,
      'display' => array(
        'default' => array(
          'label' => 'hidden'
        ),
      ),
    ),
    
    'champion_title' => array(
      'field_name' => 'field_loldata_champion_title',
      'required' => 1,
      'display' => array(
        'default' => array(
          'label' => 'hidden'
        ),
      ),
      'fences_wrapper' => 'no_wrapper',
    ),
    
    'description' => array(  
      'field_name' => 'field_loldata_description',            
      'required' => 1,
      'display' => array(
        'default' => array(
          'label' => 'hidden'
        ),
      ),
      'settings' => array(
        'text_processing' => '1',
      )
    ),  
    
    'champion_tag' => array(
      'field_name' => 'field_loldata_champion_tag',
      'required' => 1,
      'display' => array(
        'default' => array(
          'label' => 'hidden'
        ),
      ),
    ),
    
    'price' => array(
      'field_name' => 'field_loldata_price',
      'label' => 'Costo',
      'required' => 1,
      'display' => array(
        'default' => array(
          'label' => 'inline'
        ),
      ),
      'settings' => array(
        'min' => '0',
        'max' => '',
        'prefix' => '',
        'suffix' => '',
        'user_register_form' => FALSE,
      ),
      'fences_wrapper' => 'span'
    ),
    
    'recipe' => array(    
      'field_name' => 'field_loldata_recipe',
      'display' => array(
        'default' => array(
          'label' => 'hidden'
        ),
      ),
    ),
    
    'ability_number' => array(
      'field_name' => 'field_loldata_ability_number',      
      'required' => 1,
      'display' => array(
        'default' => array(
          'label' => 'hidden'
        ),
      ),
    ),
    
    'champion' => array(    
      'field_name' => 'field_loldata_champion',
      'required' => 1,
      'display' => array(
        'default' => array(
          'label' => 'hidden'
        ),
      ),
    ),
    
    'ability_cost' => array(
      'field_name' => 'field_loldata_ability_cost',
      'label' => 'Costo',
      'display' => array(
        'default' => array(
          'label' => 'inline'
        ),
      ),
      'fences_wrapper' => 'span'
    ),
    
    'cooldown' => array(
      'field_name' => 'field_loldata_cooldown',
      'label' => 'Enfriamiento',
      'display' => array(
        'default' => array(
          'label' => 'inline'
        ),
      ),
      'fences_wrapper' => 'span'
    ),
    
    'effect' => array(  
      'field_name' => 'field_loldata_effect',
      'display' => array(
        'default' => array(
          'label' => 'hidden'
        ),      
      ),
      'settings' => array(
        'text_processing' => '1',
      ),      
    ),  
    
    'item_category' => array(
      'field_name' => 'field_loldata_item_category',
      'display' => array(
        'default' => array(
          'label' => 'hidden'
        ),
      ),
    ),    
    
    'upgrade_price' => array(
      'field_name' => 'field_loldata_upgrade_price',
      'display' => array(
        'default' => array(
          'label' => 'hidden'
        ),
      ),
      'fences_wrapper' => 'span'
    ),      
    //////////////////////
    'splash' => array(
      'field_name' => 'field_loldata_splash',
      'display' => array(
        'default' => array(
          'label' => 'hidden'
        ),
      ),
    ), 
    
    'portrait' => array(
      'field_name' => 'field_loldata_portrait',
      'display' => array(
        'default' => array(
          'label' => 'hidden'
        ),
      ),
    ), 
    
    'colloq' => array(
      'field_name' => 'field_loldata_colloq',
      'display' => array(
        'default' => array(
          'label' => 'hidden'
        ),
      ),
    ), 
    
    'sellsfor' => array(
      'field_name' => 'field_loldata_sellsfor',
      'display' => array(
        'default' => array(
          'label' => 'hidden'
        ),
      ),
    ), 
    
    'rune_category' => array(
      'field_name' => 'field_loldata_rune_category',
      'display' => array(
        'default' => array(
          'label' => 'hidden'
        ),
      ),
    ), 
    
    'range' => array(
      'field_name' => 'field_loldata_range',
      'label' => 'Alcance',
      'display' => array(
        'default' => array(
          'label' => 'inline'
        ),
      ),
      'fences_wrapper' => 'span'
    ), 
    
    'skin_name' => array(
      'field_name' => 'field_loldata_skin_name',
      'display' => array(
        'default' => array(
          'label' => 'hidden'
        ),
      ),
    ), 
  );
}
