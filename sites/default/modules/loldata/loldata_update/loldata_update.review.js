(function($){$(document).ready(function () {
	"use strict";
	

	
	// Show an overview of field changes instead of full details
	$('td > .item-list').each(function(){
		var $this = $(this);
		
		var fields = $this.find('h3').map(function(){
			return $(this).text();
		}).get();
		
		$this.parent().prepend('<details><summary>' + fields.join(', ') + '</summary>' + $this.html() + '</details>');
		$this.remove();
	});

	// Define operations
	var
		opOutput = $('input[name="opchanges"]'),
		opsDef = {
			'entry': geneareOps('entry', 'Crear', 'Saltar', 'Saltar siempre'),
			'update': geneareOps('update', 'Actualizar', 'Saltar', 'Saltar siempre'),
			'obsolete': geneareOps('obsolete', 'Marcar como obsoleto', 'Saltar', 'Saltar siempre'),
			'skipped-entry': geneareOps('skipped-entry', 'Saltar', 'Crear y actualizar siempre', 'Crear y no actualizar'),
			'skipped-update': geneareOps('skipped-update', 'Saltar', 'Actualizar', 'Actualizar siempre'),
			'skipped-obsolete': geneareOps('skipped-obsolete', 'Marcar como obsoleto')
		},
		opActions = {
			'entry': [null,
				['skip'],
				['skip', {op: 'trait', trait: 'auto_update', value: false }]
			],
			'update': [null,
				['skip'],
				['skip', {op: 'trait', trait: 'auto_update', value: false }]
			],
			'obsolete': [null,
				['skip'],
				['skip', {op: 'trait', trait: 'ignore_obsolete', value: true }]
			],
			'skipped-entry': [null,
				['perform', {op: 'trait', trait: 'auto_update', value: true  }],
				['perform', {op: 'trait', trait: 'auto_update', value: false }]
			],
			'skipped-update': [null,
				['perform'],
				['perform', {op: 'trait', trait: 'auto_update', value: true }]
			],
			'skipped-obsolete': [null,
				['perform']
			]
		};

	function geneareOps(type, op1, op2) {
		var
			opN = 0,
			ops = Array.prototype.slice.call(arguments, 1);

		return function() {
			opN++;
			var ret = '';
			for(var i = 0; i < ops.length; ++i) {
				ret += '<label>' +
					'<input data-op-type="' + type + '" name="ldu-' + type + '-op-' + opN +
					'" type="radio" value="' + i + '"' + (i==0? 'data-default-op="true" checked' : '') + '>' +
				ops[i] + '</label>'
			}
			return ret;
		}
	}

	// Add operations
	$('table[class^="ldu-"]').each(function(){
		var
			table = $(this),
			type = table.attr('class').split(' ')[0].replace('ldu-', ''),
			typeFn = opsDef[type],
			opCells = $('tbody td:last-child', table);

		opCells.each(function(){
			$(this).append(typeFn());
		});
	});

	// Intercept form
	var $form = $('#loldata-update-review-form').submit(function(){
		var opLog = [];

		$('input[type="radio"]', $form).each(function(){
			var $this = $(this);
			if(this.checked && !$this.data('default-op')){
				var info = $this.closest('tr');
				opLog.push({
					type: info.data('type'),
					target: info.data('target'),
					id: info.data('id'),
					ops: opActions[$this.data('op-type')][this.value]
				});
			}
			$this.attr("disabled", "disabled");
		});

		opOutput.val(JSON.stringify(opLog));
		console.log(opLog);
		console.log(opOutput.val());
	});

});})(jQuery);