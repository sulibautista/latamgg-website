<?php
 /**
 * Implements hook_schema().
 */
function loldata_update_schema() {
  $schema = array();

  $schema['loldata_update'] = array(  
    'fields' => array(
      'id' => array(      
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'uid' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'created' => array(
        'description' => 'The Unix timestamp when the update was created.',
        'type' => 'int',
        'not null' => TRUE,
      ),
      'data' => array(
        'description' => 'The operations data array.',
        'type' => 'blob',
        'size' => 'big',
        'not null' => TRUE,
      ),      
      'applied' => array(
        'description' => 'The Unix timestamp when the data was added.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => '0',
      ),      
    ),
    'primary key' => array('id')
  );
  
  $schema['loldata_update_log'] = array(
    'description' => 'The base entity that stores LoL data revisions.',
    'fields' => array(
      'log_id' => array(
        'description' => '',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'update_id' => array(
        'description' => '',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'operation' => array(
        'description' => '',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'target_id' => array(
        'description' => '',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'target_type' => array(
        'description' => '',
        'type' => 'varchar',
        'length' => 15,
        'not null' => TRUE,
      ),
      'data' => array(
        'description' => '',
        'type' => 'blob',
        'size' => 'normal',
        'not null' => TRUE,
      )
    ),
    'primary key' => array('log_id'),
    'indexes' => array(
      'update_id' => array('update_id')
    ),
  );
    
  $schema['loldata_update_traits'] = array(
    'description' => '',
    'fields' => array(
      'trait_id' => array(
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'target_id' => array(
        'description' => '',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'target_type' => array(
        'description' => '',
        'type' => 'varchar',
        'length' => 15,
        'not null' => TRUE,
      ),
      'ignore_obsolete' => array(
        'description' => '',
        'type' => 'int',
        'not null' => TRUE,
        'default' => '0'
      ),
      'auto_update' => array(
        'description' => '',
        'type' => 'int',
        'not null' => TRUE,
        'default' => '1'
      ),
      'updated' => array(
        'description' => 'The Unix timestamp when the trait was last updated.',
        'type' => 'int',
        'not null' => TRUE
      )
    ),
    'primary key' => array('trait_id'),
    'indexes' => array(
      'target_id_type' => array('target_id', 'target_type')
    ),
  );
  
  return $schema;
}