(function($){$(document).ready(function () {
	var $frame = $('#block-loldata-user-lang .slider-frame'),
		$btn = $('> .slider-button', $frame);
		
	$frame.click(function(){
		var newLang;	
			
		if($frame.hasClass('es')) {
			$frame.removeClass('es');
			newLang = 'en';
		} else {
			$frame.removeClass('en');
			newLang = 'es';
		}
		
		$frame.addClass(newLang);
		$btn.text(newLang == 'es'? 'Español' : 'Inglés');
		
		selfSite.loldata.setUserLang(newLang, true);
	});
	
});})(jQuery);