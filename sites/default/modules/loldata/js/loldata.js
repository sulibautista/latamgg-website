if(!window.selfSite) selfSite = {};
if(!selfSite.loldata) selfSite.loldata = {};

(function(loldata, $){
	"use strict";
	
	var ongoingRequests = [],
		db = {},
		config = {},
		localWriteEnabled = false;
		
		
	/**
	 * Returns the language in which all the LoL related data is displayed. 
	 */			
	loldata.getUserLang = function() {
		return (typeof $.cookie !== 'undefined' && $.cookie('loldata_lang')) || 'es';
	};
	
	var cacheKey = 'selfSite.loldata.' + loldata.getUserLang() + '.';
	
	/**
	 * Sets the language in which all the LoL related data is displayed. 
	 */
	loldata.setUserLang = function(lang, reload) {
		$.cookie('loldata_lang', lang, { expires: 365, path: '/' });
		
		if(reload){
			location.reload();
		}
	};
		
	/**
	 * Returns the default view mode for this client. 
	 * @todo
	 */
	loldata.getDefaultViewMode = function() {
		return 'icon_label';
	};
	
	/**
	 * Generates the html specified by the view mode for the given LoLdata ids. 
	 */
	loldata.view = function(ids, viewMode, params){
		// single id
		if(!$.isArray(ids)){ 
			ids = [ids];
		}
		
		if(typeof params != 'object'){
			params = {};
		}
		
		return $.ajax({
			url: Drupal.settings.basePath + 'loldata/js/view',
			data: $.extend({ids: ids, mode: viewMode}, params)
		});
	};
	
	loldata.viewAll = function(data){
		return $.ajax({
			url: Drupal.settings.basePath + 'loldata/js/view_all',
			data: {q: JSON.stringify(data)},
			type: 'POST'
		});
	};
	
	/**
	 * Returns the current LoLDatabase.
	 * The dabase should not be modified in any way, it may contain no items at all during call time. 
	 */
	loldata.getDb = function(){
		return db;
	};
	
	/**
	 * Loads the specified loldata from the server.
	 * The returned value is a jQuery Deferred object, similar as when you call $.ajax(). done() callbacks will only 
	 * receive a single argument (the data you requested), since the actual data maybe available locally and therefore 
	 * no jqXHR object will exist.
	 * However, fail() callbacks will receive a complete argument set, same as with $.ajax().fail().
 	 * @param {Object} The data you want to be load, i.e. {champion: true, item: true, championdesc: true}
	 */
	loldata.load = function(data) {
		// If the data is already cached, just dispatch it		
		if(isCompatible(data)){
			var helper = new $.Deferred();
			helper.resolve(db);
			return helper;
		}		
		
		// If there're ongoing requests that fulfill the data requirements, return a $.when() wrapped deferred object
		// that will fire when every involved request finish.
		var promise;
		for(var i = 1; i <= ongoingRequests.length && !promise; ++i){
			combinations(ongoingRequests, i, function(ongoingCombination){
				promise = getCompatiblePromise(data, ongoingCombination);
				if(promise){
					return false;
				}
			});
		}
		
		
		if(promise){
			return promise;
		}	
		
		// No data is cached or currently being requested, start a new request asking only for data we don't have
		// available. This maybe further optimized for bandwith by checking ongoing requests asking for partial data
		// and generating a wrapper of deferred objects including this request, but is probably even more overkill.
		var missingData = {};
		for(var opt in data){
			if(typeof config[opt] == 'undefined'){
				missingData[opt] = true;
			}
		}
		
		var index = ongoingRequests.length;
		ongoingRequests.push({
			data: missingData,
			promise: $.ajax({
				url: Drupal.settings.basePath + 'loldata/json',
				dataType: 'json',
				converters: { 'text json': unpackDatabase },
				data: missingData								 
			}).done(function(){
				// Cleanup the request
				ongoingRequests.splice(index, 1);
				$.extend(config, missingData);
			})
		});
		
		return ongoingRequests[index].promise;
	};
	
	$(window).load(function(){
		localWriteEnabled = true;
		cacheDatabase();
	});

	// Load cached data at start
	(function(){
		try{
			var cachedConfig = getCachedConfig();
			if(cachedConfig){			
				config = cachedConfig;
				
				// While we could use a reviver here, IE appears to have some obscure bug that doesn't allow us
				db = JSON.parse(localStorage.getItem(cacheKey + 'db'));
								
				var objectIndex = {},
					delayed = [];
				
				var resolveCyclicReferences = function(key, value) {
					if($.isArray(value)){
						for(var i = 0, len = value.length; i < len; ++i){
							resolveCyclicReferences.call(value, i, value[i]);
						}
					} else if(value && typeof value === 'object'){
						if(value._ldo_ !== undefined){
							objectIndex[value._ldo_] = value;
							delete value._ldo_;
						}
						for(var key in value){
							resolveCyclicReferences.call(value, key, value[key]);
						}
					} else if(typeof value === 'string' && value.substring(0, 5) == '_ldo_'){
						var ref = parseInt(value.substring(5), 10);						
						if(!objectIndex[ref]){
							delayed.push({ref: ref, parentObject: this, key: key});
						} else {
							this[key] = objectIndex[ref];
						}		
					}
				};
				
				resolveCyclicReferences(null, db);
				
				for(var i = 0, len = delayed.length; i < len; ++i){
					var m = delayed[i];
					if(!objectIndex[m.ref]) console.log('missing: ' + m.ref);
					m.parentObject[m.key] = objectIndex[m.ref];
				}
			}
		} catch(e){
			db = {};
			config = {};		
			console.log(e);
		}	
	}());
		
	// This gets executed after every successful request, we enrich the dataset with useful properties/refs.
	function unpackDatabase(data) {		
		data = $.parseJSON(data);
		
		$.each(data, function(dataType, values){
			if(!db[dataType]){
				db[dataType] = {};
				
				$.each(values, function(strId, value){
					var id = parseInt(strId, 10),
						v;
					
					if(isNaN(id)){
						v = db[dataType][strId] = value;
					} else {					
						v = db[dataType][id] = $.extend(value, {
							id: id,
							type: dataType
						});
					}
					
					switch(dataType){
					case 'ability':
						v.key = "\0qwer"[parseInt(v.number, 10)-1];
						break;
					}
				});	
			} else {
				$.each(values, function(id, value){
					$.extend(true, db[dataType][parseInt(id, 10)], value);
				});				
			}									
		});
		
		// Extend db with useful references		
		if(data['item']){			
			$.each(db['item'], function(id, item){				
				item.recipe = [];
				item.recipeNames = [];
				for(var i = 0; i < item.recipeIds.length; ++i) {
					item.recipe.push(db.item[item.recipeIds[i]]);	
					item.recipeNames.push(db.item[item.recipeIds[i]].title);					
				}
			});
		}
		
		if(data['itemcategories']){
			var cat = db.itemcategories;
			cat.ordered = [];
			
			for(var i = 0, len = cat.order.length; i < len; ++i) {
				cat.ordered.push(cat[cat.order[i]]);
			}
			
			$.each(cat, function(id, category){
				if(category.parentId){
					category.parent = cat[category.parentId];
				}
			});
		}
			
		if(db['item'] && db['itemcategories'] && (data['item'] || data['itemcategories'])){
			$.each(db['item'], function(id, item){
				item.categories = [];
				item.categoryNames = [];				
				for(var i = 0; i < item.categoryIds.length; ++i) {
					item.categories.push(db.itemcategories[item.categoryIds[i]]);
					item.categoryNames.push(db.itemcategories[item.categoryIds[i]].title);					
				}
			});
		}
		
		if(db['ability'] && db['champion'] && (data['ability'] || data['champion'])){
			$.each(db['ability'], function(id, ability){
				var champion = db.champion[ability.championId];
				ability.champion = champion;
				
				if(typeof champion.abilities == 'undefined'){
					champion.abilities = new Array(5);
					champion.abilityIds = new Array(5);
				}
				
				champion.abilities[ability.number-1] = ability;
				champion.abilityIds[ability.number-1] = ability.id;
			});
		}

		if(db['skin'] && db['champion'] && (data['skin'] || data['champion'])){
			$.each(db['skin'], function(id, skin){
				var champion = db.champion[skin.championId];
				skin.champion = champion;
				
				if(typeof champion.skins == 'undefined'){
					champion.skins = [];
					champion.skinIds = [];
				}
				
				champion.skins.push(skin);
				champion.skinIds.push(skin.id);
			});
		}
		
		if(localWriteEnabled){
			cacheDatabase();
		}
				
		return db;
	}
	
	function cacheDatabase(){
		setTimeout(function(){								
			try{
				var cachedConfig = getCachedConfig();
				
				if(cachedConfig){					
					var configInOldConfig = true;
					
					// Only check to see if all keys in config exist in cached config, ignore any extra key on oldConfig
					for(var key in config){
						if(cachedConfig[key] !== config[key]){
							configInOldConfig = false;
							break;
						}
					}
					
					// Cache time is valid, and everything we have is already in the cache, exit
					if(configInOldConfig){
						return;
					}	
				}
				
				var objectIndex = [];
				
				var jsondb = JSON.stringify(db, function(key, value){
					if(value && $.isPlainObject(value)){
						var idx = objectIndex.indexOf(value);
						if(idx !== -1){				
							return '_ldo_' + idx;
						}						
						value._ldo_ = objectIndex.length;
						objectIndex.push(value);
					}
					return value;
				});		
				
				for(var i = 0, len = objectIndex.length; i < len; ++i){
					delete objectIndex[i]._ldo_;
				}
				
				localStorage.setItem(cacheKey + 'date', Date.now());
				localStorage.setItem(cacheKey + 'config', JSON.stringify(config));
				localStorage.setItem(cacheKey + 'db', jsondb);
				
			}catch(e){
				try{
					localStorage.removeItem(cacheKey + 'config');
					localStorage.removeItem(cacheKey + 'date');
					localStorage.removeItem(cacheKey + 'db');
				} catch(e1){
					console.log(e1);
				}
				console.log(e);
			}
		}, 1000);
	}
	
	function getCachedConfig(){
		var oldCreationDate = localStorage.getItem(cacheKey + 'date');	
		
		if(oldCreationDate){
			var hoursOld = (Date.now() - parseInt(oldCreationDate, 10)) / (1000 * 60 * 60);
			
			// Cache time: 1 day
			if(hoursOld < 24){
				var oldConfig = localStorage.getItem(cacheKey + 'config');
				if(oldConfig !== null){
					return JSON.parse(oldConfig);				
				}							
			}
		}
	}
	
	function isCompatible(data) {
		for(var opt in data){
			if(typeof config[opt] == 'undefined'){
				return false;
			}
		}
		return true;
	}
	
	function getCompatiblePromise(data, extraData) {
		var promises = [];
		for(var opt in data){
			if(typeof config[opt] == 'undefined'){
				var ok = false;
				
				for(var i = 0; i < extraData.length; ++i){
					if(typeof extraData[i].data[opt] != 'undefined'){
						promises.push(extraData[i].promise);
						ok = true;
						break;
					}
				}
				
				if(!ok){
					return false;
				}
			}
		}
		
		var helper = new $.Deferred();
		
		$.when.apply($, promises).then(function(){ 
			helper.resolve(db); 
		}, function(p1, p2, p3){ 
			if(promises.length == 1){
				helper.reject(p1, p2, p3);
			} else {
				helper.reject.apply(helper, p1[0]);
			}
		});
		
		return helper.promise();
	}	
	
	function combinations(numArr, choose, callback) {
	    var n = numArr.length;
	    var c = [];
	    
	    var inner = function(start, choose_) {
	    	if (choose_ == 0) {
	            if(callback(c) === false)
	            	return false;
	        } else {
	            for (var i = start; i <= n - choose_; ++i) {
	                c.push(numArr[i]);
	                if(inner(i + 1, choose_ - 1) === false)
	                	return false;
	                c.pop();
	            }
	        }
	    };
	    
	    inner(0, choose);
	}
	
})(selfSite.loldata, jQuery);