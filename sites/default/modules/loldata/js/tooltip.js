(function($){$(document).ready(function () {
	
var cache = {},
	loldata = $('[data-loldata]'),
	lastTarget,
	loadingTxt = Drupal.t("Loading.."),
	notAvailTxt = 'Información no disponible en este momento.';
	
$(document.body).qtip({
	position: {
        target: 'mouse',
        viewport: $(window),
        adjust: {
        	x: 10,
        	y: 10
        }
   	},
	style: {
		classes: 'loldata-tt'
	},
	show: {
		event: 'mouseenter',
		target: loldata
	},
	hide: {
		event: 'mouseleave',
		target: loldata
	},
	events: {
		show: function(event, api){
			var $this = $(event.originalEvent.target).closest('[data-loldata]'),
				id = $this.data('loldata'),
				value;

			if(lastTarget == id) return;

			lastTarget = id;

			if(cache[id]){
				value = cache[id];
			} else {
				var inline = $this.next('.loldata-hidden');

				if(inline.length){
					value = cache[id] = inline;
				} else {
					value = loadingTxt;

					selfSite.loldata.view(id, 'tooltip')
					.then(function(content){
						cache[id] = content;
						if(lastTarget == id)
							api.set('content.text', content);
					}, function(xhr, status, error){
						api.set('content.text', notAvailTxt);
						console.log(error);
					});
				}
			}

			api.set('content.text', value);
		}
	},
	content: ' '
});

});})(jQuery); 