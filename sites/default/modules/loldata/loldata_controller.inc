<?php

/**
 * The Controller for LoLData entities
 */
class LoLDataController extends EntityAPIController {
  public function __construct($entityType) {
    parent::__construct($entityType);
  }

  /**
   * Creates a LoLData object.
   * 
   * @param $values
   *   An array of values to set, keyed by property name.
   *
   * @return
   *   A LoLData object with all default fields initialized.
   */
  public function create(array $values = array()) {
    $values += array( 
      'loldata_id' => '',
      'vid' => '',
      'lol_id' => '',
      'is_new' => TRUE,
      'created' => '',
      'changed' => '',
      'updated' => '',
      'obsolete' => 0
    );
    
    return parent::create($values);
  }
  
  /**
   * 
   */
  public function save($entity, DatabaseTransaction $transaction = NULL) {
    if(!empty($entity->is_new) || empty($entity->{$this->idKey})) {
      $entity->created = REQUEST_TIME;
    }
    
    $entity->changed = REQUEST_TIME;
    
    return EntityAPIController::save($entity, $transaction);
  }
}