<?php
/**
 * @file
 * Displays a list of spellcheck suggestions
 *
 * - $suggestions: An array of $suggestion objects
 *   - $suggestion->link
 *   - $suggestion->text
 *   - $suggestion->submited_text
 *   - $suggestion->url
 *
 * @ingroup themeable
 */
?>
<p>
  <?php print t('Did you mean'); ?>
  <ul class="list-inline">
    <?php foreach ($suggestions as $s): ?>
      <li><?php print sprintf('<a href="%s" class="label label-primary">%s</a>', $s->url, check_plain($s->suggestion));?></li>
    <?php endforeach; ?>
  </ul>
</p>
