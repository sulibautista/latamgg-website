<?php
/**
 * @file
 * Displays a single spellcheck suggestion
 *
 * Available variables
 * - $suggestion
 *   - $suggestion->link
 *   - $suggestion->suggestion
 *   - $suggestion->original
 *   - $suggestion->url
 *
 * @ingroup themeable
 */
?>
<p>
  <?php print t('Did you mean !link',
    ['!link' => sprintf('<a href="%s" class="label label-primary">%s</a>', $suggestion->url, check_plain($suggestion->suggestion))]); ?>
</p>
