module.exports = {
  options: {
    prefix: 'shape-',
    cleanup: false,
    includedemo: 'svgdemo.html',
    formatting: {
      indent_size: 2
    },
    svg: {
      'xmlns': 'http://www.w3.org/2000/svg',
      'xmlns:xlink': 'http://www.w3.org/1999/xlink'
    }
  },
  default: {
    files: {
      'img/gen/default.svg': ['img/svg/*.svg']
    }
  }
};