module.exports = {
  options: {
    plugins: [
      { cleanupIDs: false },
      { removeViewBox: false },
      { removeUselessStrokeAndFill: false },
      { convertStyleToAttrs: false }
    ]
  },
  dist: {
    files: {
      '../img/default.svg': 'img/gen/default.svg',
      // Adds the svg to drupal root, so we can refer to sprites via `default.sv#sprite-xxx`
      // We can later on map this via nginx or other methods
      // '../../../../../default.svg': 'img/gen/default.svg'
    }
  }
};
