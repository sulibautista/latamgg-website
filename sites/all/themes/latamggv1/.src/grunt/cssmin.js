module.exports = {
  theme: {
    options: {
      processImport: false
    },
    files: [{
      expand: true,
      cwd: '../css/',
      dest: '../css/',
      src: ['**/*.css']
    }]
  }
};