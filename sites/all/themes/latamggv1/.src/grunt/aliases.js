module.exports = {
  default: 'watch',
  css: [
    'less:theme',
    'autoprefixer:theme',
    'cssmin:theme'
  ],
  svg: [
    'svgstore',
    'svgmin'
  ],
  sprites: [
    'clean:sprites',
    'image_resize',
    'sprite:corex1',
    'sprite:corex1_5',
    'sprite:corex2',
    'sprite:corex3',
    'less_imports:sprites'
  ],
  responsive_sprites: [
    'clean:responsive_sprites',
    'responsive_images',
    'sprite',
    'less_imports:sprites'
  ],
  deploy: [
    'bower',
    'clean', // clean:production
    'uglify',
    'svg',
    'sprites',
    'responsive_sprites',
    'css'
  ]
};