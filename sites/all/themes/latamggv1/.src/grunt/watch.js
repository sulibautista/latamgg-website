module.exports = {
  css: {
    options: {
      nospawn: true
    },
    files: ['less/**/*.less'],
    tasks: ['css']
  },

  js: {
    options: {
      nospawn: true
    },
    files: ['js/**/*.js', 'lib/**/*.js'],
    tasks: ['uglify']
  },

  svg: {
    files: ['img/svg/*.svg'],
    tasks: ['svg']
  },

  sprites: {
    files: ['img/core_sprite/*.*'],
    tasks: ['sprites']
  },

  resp_sprites: {
    files: ['img/resp_*/**/*.*'],
    tasks: ['responsive_sprites']
  }
};