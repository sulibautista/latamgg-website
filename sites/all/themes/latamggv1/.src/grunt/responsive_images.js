var _ = require('lodash');

var
  respSizes,
  respMultipliers;

module.exports = function(grunt, options){
  respSizes = options.spriteRespSizes;
  respMultipliers = options.spriteRespMultipliers;

  return {
    options: {
      engine: 'im',
      newFilesOnly: false
    }
    /*full_logo: {
      options: {
        sizes: makeRespSizes([160, 128], [192, 144], null, null, 'full_logo')
      },
      files: respFiles('resp_p', ['full-logo1.*'])
    },
    padding: {
      options: {
        sizes: makeRespSizes(64, 'padding')
      },
      files: respFiles('resp_p', [{src: ['padding/*.*'], expand: true}])
    }*/
  }
};

// Helper for generating responsive image sizes for each breakpoint & multiplier pair.
function makeRespSizes(){
  var
    i = 0,
    sizes = [],
    realSizes = [],
    args = arguments;

  for(; i < respSizes.length; ++i){
    var
      op = arguments[i],
      out = {
        name: respSizes[i],
        rename: false,
        aspectRatio: false,
        exactDimensions: true
      };

    if(Number.isFinite(op)){
      out.width = op;
    } else if(Array.isArray(op)){
      var moreOps = 2;

      out.width = op[0];

      if(Number.isFinite(op[1])){
        out.height = op[1];
      } else if(op[1]){
        moreOps = 1;
      }

      if(typeof op[moreOps] != 'undefined'){
        _.extend(out, op[moreOps]);
      }
    } else if(_.isPlainObject(op)){
      _.extend(out, op);
    } else if(!op || (i === args.length-1 && _.isString(op))){
      out = _.extend({}, sizes[i - 1], out);
    } else {
      throw new Error('unknown makeSizes() type arg ' + typeof op);
    }

    // Generate different sizes based on the multipliers
    _.forOwn(respMultipliers, function(mult, multSuffix){
      var size = _.extend({}, out, {
        name: out.name + '-' + multSuffix
      });

      if(size.width)
        size.width *= mult;
      if(size.height)
        size.height *= mult;

      realSizes.push(size);
    });

    sizes[i] = out;
  }

  // Ensure JPG dimensions are divisible by 8 (required by jpegtransmith)
  realSizes.forEach(function(size){
    if(size.width % 8 > 0){
      throw new Error("Image " + args[args.length - 1] + size.name + " width of " + size.width + " is not divisible by 8");
    }
    if(size.height % 8 > 0){
      throw new Error("Image " + args[args.length - 1] + size.name + " height of " + size.height + " is not divisible by 8");
    }
  });
  return realSizes;
}

// Helper for targeting responsive files in img/ folder
function respFiles(spriteBaseName, files){
  if(!Array.isArray(files)){
    files = [files];
  }

  files.forEach(function(file, i){
    if(!_.isPlainObject(file)){
      files[i] = file = { src: file };
    }
    file.src = 'img/' + spriteBaseName + '_sprite/' + file.src;
    file.custom_dest = 'img/gen/' + spriteBaseName + '/{%= name %}';
  });

  return files;
}