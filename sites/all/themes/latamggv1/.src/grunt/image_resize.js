module.exports = {
  core_spritex1: {
    options: {
      width: '33.33333333333333%',
      concurrency: 2
    },
    src: "img/core_sprite/*.*",
    dest: "img/gen/core_sprite/1x/"
  },
  core_spritex1_5: {
    options: {
      width: "50%",
      concurrency: 2
    },
    src: "img/core_sprite/*.*",
    dest: "img/gen/core_sprite/1_5x/"
  },
  core_spritex2: {
    options: {
      width: '66.66666666666667%',
      concurrency: 2
    },
    src: "img/core_sprite/*.*",
    dest: "img/gen/core_sprite/2x/"
  },
  core_spritex3: {
    options: {
      width: "100%",
      concurrency: 2
    },
    src: "img/core_sprite/*.*",
    dest: "img/gen/core_sprite/3x/"
  }
};