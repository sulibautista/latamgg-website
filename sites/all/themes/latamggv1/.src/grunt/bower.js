module.exports =  {
  install: {
    options: {
      install: true,
      verbose: true,
      targetDir: 'lib',
      cleanTargetDir: true,
      layout: 'byType'
    }
  }
};