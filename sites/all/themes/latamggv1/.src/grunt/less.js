module.exports = {
  theme: {
    options: {
      yuicompress: false
    },
    files: {
      "../css/style.css": "less/style.less",
      "../css/ie9.css": "less/ie9.less",
      "../css/editor-full-html.css": "less/components/editor-full-html.less"
    }
  }
};