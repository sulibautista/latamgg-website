module.exports = {
  options: {
    stripBanners: true,
    preserveComments: false,
    mangle: true,
    compress: {
      sequences: true,
      dead_code: true,
      conditionals: true,
      booleans: true,
      unused: true,
      if_return: true,
      join_vars: true,
      //drop_console: true
    }
  },
  // Single files that will be used on different pages independently, possibly aggregated by Drupal
  single: {
    files: [{
      expand: true,
      cwd: 'js/',
      dest: '../js/',
      src: ['*.js', '!*.min.js']
    }]
  },
  // All scripts that need to be on everypage live in js/global
  global: {
    files: {
      '../js/global.js' : [
        // Bootstrap build
        'components/bootstrap/js/transition.js',
        'components/bootstrap/js/alert.js',
        'components/bootstrap/js/button.js',
        //'components/bootstrap/js/carousel.js',
        'components/bootstrap/js/collapse.js',
        'components/bootstrap/js/dropdown.js',
        'components/bootstrap/js/modal.js',
        //'components/bootstrap/js/tooltip.js',
        //'components/bootstrap/js/popover.js',
        //'components/bootstrap/js/scrollspy.js',
        'components/bootstrap/js/tab.js',
        //'components/bootstrap/js/affix.js'

        // Our custom global scripts
        'js/global/*.*',
      ]
    }
  },

  // All scripts that need to be on every page in the header live in js/global
  global_head: {
    files: {
      '../js/global-head.js' : [
        'js/global_head/*.*'
      ]
    }
  },

  // Third party
  vendor: {
    options: {
      compress: true
    },
    files: [{
      expand: true,
      cwd: 'lib/js/',
      dest: '../js/lib/',
      src: ['**/*.js', '!**/*.min.js']
    }]
  }
};