var _ = require('lodash');

var
  jpegtransmith = require('jpegtransmith'),
  spritesmith = require('grunt-spritesmith/node_modules/spritesmith/src/smith.js');

//spritesmith.addEngine('jpegtransmith', jpegtransmith);

var
  respMultipliers,
  respSizes,
  // Each key is a different sprite base (we generate respSizes.length * respMultipliers.length sprite images for each base)
  respSprites = {
    resp: {
      format: 'png',
      //engine: 'gmsmith',
      paddingBase: 2
    },
    resp_p: {
      format: 'jpg',
      engine: 'jpegtransmith',
      paddingBase: 0,
      x1: {
        imgOpts: {
          quality: 85
        }
      },
      x1_5: {
        imgOpts: {
          quality: 70
        }
      },
      x2: {
        imgOpts: {
          quality: 60
        }
      },
      x3: {
        imgOpts: {
          quality: 40
        }
      }
    }
  };

var config = {
  corex1: {
    cssVarMap: basicSpriteMutator(1),
    'cssOpts': { functions: false },
    algorithm: 'binary-tree',
    src: "img/gen/core_sprite/1x/*.*",
    dest: "../img/sprite/sprite-core.png",
    destCss: "less/sprite/gen/sprite-core.less",
    imgPath: "../img/sprite/sprite-core.png",
    'padding': 2
  },
  corex1_5: {
    cssVarMap: basicSpriteMutator(1.5),
    'cssOpts': { functions: false },
    algorithm: 'binary-tree',
    src: "img/gen/core_sprite/1_5x/*.*",
    dest: "../img/sprite/sprite-core-1_5x.png",
    destCss: "less/sprite/gen/sprite-core-1_5x.less",
    imgPath: "../img/sprite/sprite-core-1_5x.png",
    'padding': 3
  },
  corex2: {
    cssVarMap: basicSpriteMutator(2),
    'cssOpts': { functions: false },
    algorithm: 'binary-tree',
    src: "img/gen/core_sprite/2x/*.*",
    dest: "../img/sprite/sprite-core-2x.png",
    destCss: "less/sprite/gen/sprite-core-2x.less",
    imgPath: "../img/sprite/sprite-core-2x.png",
    'padding': 4
  },
  corex3: {
    cssVarMap: basicSpriteMutator(3),
    'cssOpts': {functions: false},
    algorithm: 'binary-tree',
    src: "img/gen/core_sprite/3x/*.*",
    dest: "../img/sprite/sprite-core-3x.png",
    destCss: "less/sprite/gen/sprite-core-3x.less",
    imgPath: "../img/sprite/sprite-core-3x.png",
    'padding': 6
  }
};

module.exports = function(grunt, options){
  respSizes = options.spriteRespSizes;
  respMultipliers = options.spriteRespMultipliers;

  // Create sprite tasks for each responsiv sprite base + breakpoint + multiplier
  _.forOwn(respSprites, function(spriteBase, spriteBaseName){
    respSizes.forEach(function(sizeName){
      _.forOwn(respMultipliers, function(mult, multName){
        var finalSpriteName = spriteBaseName + "-" + sizeName + "-" + multName + "." + spriteBase.format;

        var conf = {
          cssVarMap: respSpriteMutator(spriteBaseName, sizeName, multName, mult),
          cssOpts: { functions: false },
          algorithm: 'binary-tree',
          engine: spriteBase.engine,
          engineOpts: {
            imagemagick: true
          },
          imgOpts: { bg: "blue" },
          src: "img/gen/" + spriteBaseName + "/" + sizeName + "-" + multName + "/*.*",
          dest: "../img/sprite/" + finalSpriteName,
          destCss: "less/sprite/gen/" + spriteBaseName + "-" + sizeName + "-" + multName + ".less",
          imgPath: "../img/sprite/" + finalSpriteName,
          padding: spriteBase.paddingBase * mult
        };

        if(spriteBase[multName])
          _.merge(conf, spriteBase[multName]);

        config[spriteBaseName + '_' + sizeName + '_' + multName] = conf;
      });
    });
  });

  return config;
};

// Modifies basic sprite variables to make them multipler-compatible
function basicSpriteMutator(mult) {
  return function (sprite) {
    sprite.name = 'sprite-' + sprite.name + '-x' + mult.toString().replace('.', '_');
    sprite.total_width = Math.round(sprite.total_width / mult);
    sprite.total_height = Math.round(sprite.total_height / mult);
  };
}

// Makes jpgetransmith processed sprites respect the multipliers
// If we dont do this, the whole grid falls apart in each multiplier of each breakpoint
var imgCache = {};
jpegtransmith.imageDataMutator = function(data){
  var mults = Object.keys(respMultipliers);
  for(var i = 0; i < mults.length; ++i){
    var multIndex = data.file.lastIndexOf(mults[i] + '/');
    if(multIndex !== -1){
      var mult = respMultipliers[mults[i]];
      data.width = ((data.realWidth/mult + 7) & ~7) * mult;
      data.height = ((data.realHeight/mult + 7) & ~7) * mult;
      imgCache[data.file] = data;
      break;
    }
  }
};

// Modies variable names for responsive sprites
// 1) Make image multiplier-comptaible (same total_width & total_height for all multipliers)
// 2) Restore original dimensions set in `jpegtransmith.imageDataMutator` for multipliers
// 3) Set a 2 padding/margin of error for each image, so they work in every browser despite rounding errors
function respSpriteMutator(spriteBaseName, sizeName, multName, mult){
  return function (sprite) {
    sprite.name = (spriteBaseName + '-' + sprite.name + '-' + sizeName + '-' +  multName).replace('_', '-');
    sprite.total_width = Math.round(sprite.total_width / mult);
    sprite.total_height = Math.round(sprite.total_height / mult);

    var origSize = imgCache[sprite.source_image];
    if(origSize){
      sprite.width = Math.round(origSize.realWidth / mult);
      sprite.height = Math.round(origSize.realHeight / mult);
    }

    sprite.width -= 4;
    sprite.height -= 4;
    sprite.x += 2;
    sprite.y += 2;
  };
}