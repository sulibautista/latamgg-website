module.exports = function(grunt) {
  require('load-grunt-config')(grunt, {
    init: true,
    loadGruntTasks: {
      pattern: 'grunt-*',
      config: require('./package.json'),
      scope: 'devDependencies'
    },
    data: {
      spriteRespSizes: ['xxs', 'xs', 'sm', 'md'],
      spriteRespMultipliers: {
        x1:   1,
        x1_5: 1.5,
        x2:   2,
        x3:   3
      }
    }
  });
};