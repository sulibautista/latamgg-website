(function ($) {

  function shrinkMenu(context){
    $('.shrink-menu', context).each(function () {
      var
        menu = $(this),
        mq = matchMedia(menu.data('shrink-query')),
        select;

      mq.addListener(shrink);
      shrink();

      function shrink(){
        if(!mq.matches || select) return;

        select = $('<select>');

        var options = '';

        select.attr('class', 'shrunken-menu ' + menu.data('shrink-class'));

        $('a', menu).each(function(){
          options += '<option value="' + $(this).attr('href') + '">' + $(this).text() + '</option>';
        });

        select.append(options);

        $(':nth-child(' + ($('.active', menu).index()+1) + ')', select)
          .attr('selected', true);

        select.change(function(){
          window.location.href = $(this).val();
        });

        menu.after(select);
      }
    });
  }

  Drupal.behaviors.lggv1_shrink_menu = {
    attach: shrinkMenu
  };
})(jQuery);