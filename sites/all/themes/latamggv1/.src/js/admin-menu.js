// Adds the adminbar occupied space to the top spacing system
(function ($) {
  "use strict";

  var topSpacer = LGGV1.makeTopSpacer(-15);

  Drupal.admin.behaviors.lggv1_admin_menu = function(context, _, menu){
    $(window).resize(setSpacing);
    setSpacing();

    function setSpacing(){
      topSpacer.setSpacing(menu.height());
    }
  };

})(jQuery);