(function(window, document){
  "use strict";
  var ajax = new XMLHttpRequest();

  // We cannot use Drupal.settings.basePath because we run before the settings are set, so just assume we can
  // reach the svg from the domain root, which has to be the case unless the site is moved to an inner path
  ajax.open('GET', '/sites/all/themes/latamggv1/img/default.svg', true);
  ajax.responseType = 'document';
  ajax.onload = function() {
    ajax.responseXML.documentElement.setAttribute('class', 'element-invisible');
    window.LGGV1_svg = ajax.responseXML.documentElement;
    if(window.LGGV1_svg_should_load){
      window.LGGV1_svg_load();
    }
  };
  ajax.send();

  window.LGGV1_svg_load = function() {
    document.body.insertBefore(window.LGGV1_svg, document.body.childNodes[0]);
  };
})(window, document);

