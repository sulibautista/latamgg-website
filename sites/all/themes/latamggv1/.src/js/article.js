(function (window, document, $) {
  "use strict";

  var
    onLoadFired = false,
    onLoadStick = [],

    isStuck = false,
    container = $('.main-container'),
    footer = $('.site-footer'),
    maybeStick = $('.maybe-stick, .maybe-stick-parent > :first-child'),

    isSide = true,
    maybeMove = $('.bs-article > aside'),
    destSide = maybeMove.parent(),
    destBelow = $('.pane-popular-articles-panel-pane-1').closest('.row');

  var
    smMin = matchMedia('(min-width: 768px)'),
    xsMax = matchMedia('(max-width: 767px)');

  smMin.addListener(checkSmMin);
  xsMax.addListener(checkXsMax);

  checkSmMin();
  checkXsMax();

  $(window).load(function(){
    onLoadFired = true;
    if(isStuck){
      onLoadStick.forEach(makeSticky);
    }
  });

  function checkSmMin(){
    if(smMin.matches){
      if(!isStuck){
        stick();
      }
    } else {
      if(isStuck){
        unstick();
      }
    }
  }

  function checkXsMax(){
    if(xsMax.matches){
      if(isSide){
        moveBelow();
      }
    } else {
      if(!isSide){
        moveSide();
        updateSticky(); // Since one sticky is part of the moved element tree, issue a refresh just in case.
      }
    }
  }

  function stick(){
    maybeStick.each(function(){
      if(!onLoadFired && $('img, picture', this).length > 0){
        onLoadStick.push(this);
      } else {
        makeSticky(this);
      }
    });
    $(window).on('resize orientationchange', updateSticky);
    $(document).on('LGGV1.topSpacingChanged', updateSticky);
    isStuck = true;
  }

  function makeSticky(el){
    var $el = $(el);
    $el.sticky({
      getWidthFrom: $el.parent(),
      responsiveWidth: true,
      topSpacing: getSpacingTop,
      bottomSpacing: getSpacingBottom
    });
    updateLeftOffset.call(el);
  }

  function unstick(){
    maybeStick.css('left', '');
    maybeStick.unstick();
    $(window).off('resize', updateSticky);
    $(document).off('LGGV1.topSpacingChanged', updateSticky);
    isStuck = false;
  }

  function moveBelow(){
    destBelow.append(maybeMove);
    isSide = false;
  }

  function moveSide(){
    destSide.append(maybeMove);
    isSide = true;
  }

  function updateSticky(){
    maybeStick.sticky('update');
    maybeStick.each(updateLeftOffset);
  }

  function updateLeftOffset(){
    var $el = $(this);
    console.log(this);
    $el.css('left', $el.parent().offset().left);
  }

  function getSpacingBottom(){
    return footer.outerHeight(true) + parseInt(container.css('padding-bottom'), 10);
  }
  getSpacingBottom.valueOf = getSpacingBottom;

  function getSpacingTop(){
    return LGGV1.getTotalTopSpacerSum() + parseInt(container.css('padding-top'), 10);
  }
  getSpacingTop.valueOf = getSpacingTop;

})(window, document, jQuery);