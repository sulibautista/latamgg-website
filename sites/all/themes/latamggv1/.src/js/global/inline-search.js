// Manages mobile navbar buttons' collapsible state
(function ($) {
  "use strict";

  var
    doc = $(document),
    searchForm = $('.navbar-main .search-inline'),
    searchBtn = $('.form-submit', searchForm),
    input = $('input[type="search"]', searchForm);

  searchBtn.click(function(e){
    if(/^\s*$/.test(input.val())) {
      e.preventDefault();
      if(searchForm.hasClass('in')) {
        close();
      } else {
        open();
      }
    }
  });

  input.focus(function(){
    if(!searchForm.hasClass('in')) {
      open();
      // Trigger autocomplete, if available
      input.trigger('keyup');
    }
  });

  function open(){
    searchForm.addClass('in');
    input.focus();
    doc.on('click touchstart', closeIfOutOfContext);
  }

  function close() {
    searchForm.removeClass('in');
    input.blur();
    doc.off('click touchstart', closeIfOutOfContext);
  }

  function closeIfOutOfContext(e){
    // Don't hide if user clicked on autocomplete suggestion
    if(!$.contains(searchForm[0], e.target)) close();
  }

})(jQuery);