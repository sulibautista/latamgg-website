// Manages mobile navbar buttons' collapsible state
(function ($) {
  "use strict";

  var
    dataKey = 'LGGV1.navbar-collapse',
    searchBtn = $('#navbar-toggle-search'),
    menuBtn = $('#navbar-toggle-menu'),
    navContainer = $('#navbar-collapse'),
    search = $('.search-inline', navContainer),
    input = $('input[type="search"]', search),
    nav = $('nav', navContainer),
    menuShouldGrow = matchMedia('(min-width: 768px)'),
    btns = searchBtn.add(menuBtn);

  navContainer.collapse({ toggle: false });
  nav.addClass('in').collapse({ toggle: false });

  menuShouldGrow.addListener(growOrShrinkMenu);
  growOrShrinkMenu();

  var navState;
  function growOrShrinkMenu(){
    if(menuShouldGrow.matches) {
      navState = nav.is('.collapse.in');
      showNavNoAnimation();
      navContainer.attr('aria-expanded', true);
    } else {
      navContainer.attr('aria-expanded', navContainer.is('.collapse.in'));
      if(navState) {
        showNavNoAnimation();
      } else {
        hideNavNoAnimation();
      }
    }
  }

  menuBtn.data(dataKey, {
    containerClass: 'menu-collapse',
    goIn: showNavNoAnimation,
    transitionIn: function() {
      nav.collapse('show');
    },
    transitionOut: function() {
      nav.collapse('hide');
    }
  });

  navContainer.on('hidden.bs.collapse', function(e){
    if(e.target === navContainer[0]) hideNavNoAnimation();
  });

  searchBtn.data(dataKey, {
    containerClass: 'search-collapse'
  });

  btns.click(function() {
    var $this = $(this);

    if(navContainer.data('bs.collapse').transitioning) return false;

    if($this.hasClass('collapsed')) {
      if(navContainer.is('.collapse.in')) {
        var out = btns.not('.collapsed');
        callOn(out, 'transitionOut');
        out.addClass('collapsed');
        navContainer.removeClass(out.data(dataKey).containerClass);
        callOn($this, 'transitionIn');
      } else {
        callOn($this, 'goIn');
        navContainer.collapse('show');
      }
      navContainer.addClass($this.data(dataKey).containerClass);
    } else {
      callOn($this, 'goOut');
      navContainer.collapse('hide');
      navContainer.removeClass($this.data(dataKey).containerClass);
    }

    $this.toggleClass('collapsed');
  });

  function hideNavNoAnimation(){
    navCollapseNoAnimation('hide');
  }

  function showNavNoAnimation() {
    navCollapseNoAnimation('show');
  }

  function navCollapseNoAnimation(op) {
    disableCollapseAnimation();
    nav.collapse(op);
    restoreCollapseAnimation();
  }

  function callOn(el, fnName){
    var fn = el.data(dataKey)[fnName];
    if(fn) fn();
  }

  var oldAnimSupport;
  function disableCollapseAnimation() {
    oldAnimSupport = $.support.transition;
    $.support.transition = false;
  }

  function restoreCollapseAnimation() {
    $.support.transition = oldAnimSupport;
  }

})(jQuery);