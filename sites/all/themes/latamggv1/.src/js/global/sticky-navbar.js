// Handles stickyness of the header on tablets and higher
(function ($) {
  "use strict";

  var
    smMin = matchMedia('(min-width: 768px)'),
    isJsStuck = false,
    navbar = $('#navbar'),
    topSpacer = LGGV1.makeTopSpacer(0),
    topSpacing;

  refreshTopSpacing();
  smMin.addListener(checkSmMin);
  checkSmMin();

  $(document).on('LGGV1.topSpacingChanged', function(){
    refreshTopSpacing();
    if(isJsStuck){
      navbar.sticky('update');
    }
  });

  function checkSmMin(){
    if(smMin.matches){
      if(!isJsStuck){
        jsStick();
      }
    } else {
      if(isJsStuck){
        jsUnstick();
      }
    }
  }

  function jsStick(){
    topSpacer.setSpacing(navbar.height());
    navbar.sticky({
      topSpacing: returnTopSpacing,
      className: 'navbar-sticked',
      wrapperClassName: 'navbar-sticky-wrapper container'
    });
    isJsStuck = true;
  }

  function jsUnstick(){
    isJsStuck = false;
    navbar.unstick();
    topSpacer.setSpacing(0);
  }

  function refreshTopSpacing(){
    topSpacing = topSpacer.getSpaceBefore();
  }

  function returnTopSpacing(){
    return topSpacing;
  }
  returnTopSpacing.valueOf = returnTopSpacing;
})(jQuery);