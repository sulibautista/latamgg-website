// Handles stickyness of the header on phones
(function ($) {
  "use strict";

  var
    enabled = matchMedia('(max-width:  767px) and (min-height: 350px)'),
    navbar = $('#navbar'),
    topSpacer = LGGV1.makeTopSpacer(1),
    topSpacing;

  refreshTopSpacing();
  enabled.addListener(checkEnabled);
  checkEnabled();

  $(document).on('LGGV1.topSpacingChanged', function(){
    refreshTopSpacing();
    if(enabled.matches){
      applyTopSpacing();
    }
  });

  function checkEnabled(){
    if(enabled.matches){
      applyTopSpacing();
    } else {
      resetTopSpacing();
    }
  }

  function applyTopSpacing(){
    topSpacer.setSpacing(navbar.height());
    navbar.css('top', topSpacing);
  }

  function resetTopSpacing(){
    navbar.css('top', 0);
    topSpacer.setSpacing(0);
  }

  function refreshTopSpacing(){
    topSpacing = topSpacer.getSpaceBefore();
  }
})(jQuery);