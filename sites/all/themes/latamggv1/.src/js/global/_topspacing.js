(function(window, document, $){
  "use strict";

  if(!window.LGGV1) window.LGGV1 = {};

  var
    LGGV1 = window.LGGV1,
    $doc = $(document),
    topSpacers = [],
    totalSum = 0;

  function TopSpacer(level){
    this._idx = -1;
    this.level = level;
    this.space = 0;
    this.beforeSum = 0;
  }

  TopSpacer.prototype.setSpacing = function(space) {
    if(this.space !== space) {
      var spaceDelta = -this.space + space;
      totalSum += spaceDelta;
      this.space = space;
      rebuildSpacersBeforeSum(this, spaceDelta);
      $doc.trigger('LGGV1.topSpacingChanged');
    }
  };

  TopSpacer.prototype.getSpaceBefore = function() {
    return this.beforeSum;
  };

  LGGV1.makeTopSpacer = function(level) {
    var spacer = new TopSpacer(level);
    topSpacers.push(spacer);
    topSpacers.sort(compareSpacer);
    for(var i = 0; i < topSpacers.length; ++i) {
      topSpacers[i]._idx = i;
    }
    return spacer;
  };

  LGGV1.getTotalTopSpacerSum = function(){
    return totalSum;
  };

  function compareSpacer(a, b) {
    return a.level - b.level;
  }

  function rebuildSpacersBeforeSum(spacer, delta){
    for(var i = spacer._idx + 1; i < topSpacers.length; ++i){
      topSpacers[i].beforeSum += delta;
    }
  }

})(window, document, jQuery);

