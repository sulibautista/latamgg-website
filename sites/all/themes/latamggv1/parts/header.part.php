<header role="banner" class="site-header">
  <div id="navbar" class="<?php print $navbar_classes; ?> navbar-main">

    <div class="navbar-header">
      <?php if ($is_front): ?> <h1 class="logo-h1-wrapper"> <?php endif; ?>
      <a class="menu-logo navbar-btn pull-left" href="<?php print $front_page; ?>">
        <?php print $header_logo; ?>
      </a>
      <?php if ($is_front): ?> </h1> <?php endif; ?>

      <button type="button" id="navbar-toggle-search" class="navbar-toggle collapsed">
        <span class="sr-only">Activar Buscador</span>
        <?php print $header_search_icon ?>
      </button>
      <button type="button" id="navbar-toggle-menu" class="navbar-toggle collapsed">
        <span class="sr-only">Activar navegación</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div>

    <?php if (!empty($primary_nav) || !empty($secondary_nav) || !empty($page['navigation'])): ?>
      <div id="navbar-collapse" class="navbar-collapse collapse">
        <nav role="navigation">
          <?php if (!empty($primary_nav)): ?>
            <?php print render($primary_nav); ?>
          <?php endif; ?>
          <?php if (!empty($secondary_nav)): ?>
            <?php print render($secondary_nav); ?>
          <?php endif; ?>
          <?php if (!empty($page['navigation'])): ?>
            <?php print render($page['navigation']); ?>
          <?php endif; ?>
        </nav>
        <?php print render($menu_search); ?>
      </div>
    <?php endif; ?>
  </div>
</header>
<div class="topic-bg"></div>