<?php

define('PART_PATH', drupal_get_path('theme', 'latamggv1') . '/parts/');

function latamggv1_path() {
  return drupal_get_path('theme', 'latamggv1');
}
/**
 * Implements hook_preprocess_html().
 * Adds variables to control the output of social SDKs and global scripts.
 */
function latamggv1_preprocess_html(&$vars){
  // Always output twitter, as it is needed even for the simple @follow button
  $vars['do_twitter'] = TRUE;
  // Default is false, below we enable it for specific pages
  $vars['do_fb'] = FALSE;

  $path = latamggv1_path();

  // Fonts
  drupal_add_css('http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,700%7CMontserrat:400,700', array(
    'type' => 'external'
  ));

  // IE9<= Fixes
  drupal_add_css($path .  '/css/ie9.css', array(
    'group' => CSS_THEME,
    'browsers' => array(
      'IE' => 'lte IE 9',
      '!IE' => FALSE
    ),
    'preprocess' => FALSE
  ));

  $basic_script = [
    'group' => JS_THEME,
    'scope' => 'footer'
  ];

  $every_page_script = ['every_page' => TRUE] + $basic_script;

  // Inline svg-load to start loading our svg sprites ASAP
  drupal_add_js(file_get_contents($path . '/js/svg-load.js'), ['type' => 'inline', 'scope' => 'force_header'] + $every_page_script);
  // Put the svg in page ASAP
  $vars['script_svg_insert'] = file_get_contents($path . '/js/svg-insert.js');

  // matchMedia.addListener polyfill, only addListener because picturefill includes the basic matchMedia polyfill
  drupal_add_js($path . '/js/lib/matchMedia/matchMedia.addListener.js', $every_page_script);

  // Small affix plugin
  drupal_add_js($path . '/js/lib/jquery-sticky/jquery.sticky.js', $every_page_script);

  // Our custom global scripts aggregated and minified
  drupal_add_js($path . '/js/global.js', $every_page_script);

  // Scripts per page
  $page = page_manager_get_current_page();
  if ($page) {
    switch($page['name']){
      case 'node_view':
        // Comments on nodes
        $vars['do_fb'] = TRUE;

        switch($page['contexts']['argument_entity_id:node_1']->data->type) {
          case 'article':
            drupal_add_js($path . '/js/article.js', $basic_script);
            break;
        }
    }
  }

  // TODO move this to a preprocessor of admin toolbar
  if(user_access('access administration menu')){
    drupal_add_js($path . '/js/admin-menu.js', $basic_script);
  }
}

/**
 * Implements hook_js_alter()
 *
 * Moves scripts from the header scope (and others) to the footer, maintaining
 * their relative positions by adjusting script groups while also making
 * sure settings are positioned in between scopes.
 */
function latamggv1_js_alter(&$javascript) {
  // List the scripts we want in to remain in their original scope.
  $keep_in_head = array(
    //'sites/all/modules/google_analytics/googleanalytics.js',
  );

  foreach ($javascript as $key => &$script) {
    switch($script['scope']) {
      case 'header':
        if(!in_array($script['data'], $keep_in_head))
          $script['scope'] = 'footer';
        break;
      case 'footer':
        $script['group'] += 1000;
        break;
      case 'force_header':
        $script['scope'] = 'header';
        break;
      default:
        throw new Exception("Unsupported script scope '{$script['scope']}'");
    }
  }

  // Add settings as an inline script before the header theme group moved to the footer and before original footer scripts.
  if (isset($javascript['settings'])) {
    $javascript['inline_settings'] = array(
       'type' => 'inline',
       'scope' => 'footer',
       'data' => 'jQuery.extend(Drupal.settings, ' . drupal_json_encode(drupal_array_merge_deep_array($javascript['settings']['data'])) . ");",
       'group' => JS_LIBRARY,
       'every_page' => TRUE,
       'weight' => 0,
     ) + drupal_js_defaults();

    // No need for drupal_get_js() to do this again.
    unset($javascript['settings']);
  }
}

/**
 * Makes the menu search form available to the page template.
 */
function latamggv1_preprocess_page(&$vars) {
  $vars['header_logo'] = _latamggv1_icon('latamgg-logo-white', 'img', ['aria-label' => 'LatamGG']);
  $vars['header_search_icon'] = _latamggv1_icon('search', 'presentation');

  $vars['menu_search'] = [
    '#cache' => [
      'keys' => ['latamggv1', 'preprocess_page', 'menu_search'],
      'granularity' => DRUPAL_CACHE_PER_ROLE
    ],
    '#pre_render' => ['_latamggv1_render_menu_search']
  ];
}

function _latamggv1_render_menu_search($element) {
  $form = drupal_get_form('latamgg_search_inline_form');
  // XXX Does TRUE actually adds it to every page?
  $form['#attached']['library'][] = ['system', 'drupal.autocomplete', TRUE];
  return $element + $form;
}

/**
 * Add styles to the menu search form.
 */
function latamggv1_form_latamgg_search_inline_form_alter(&$form) {
  //$form['#attributes']['class'][] = 'form-inline';

  $form['submit']['#hide_text'] = TRUE;
  $form['submit']['#icon'] = _latamggv1_icon('search', 'presentation');
  $form['submit']['#icon_position'] = 'after';
  $form['submit']['#button_classes'] = FALSE;
}

function latamggv1_element_info_alter(&$elements) {
  $elements['select']['#pre_render'][]  = '_latamggv1_prerender_chosen';
}

function _latamggv1_prerender_chosen($element) {
  if(!empty($element['#chosen']) && ($class = array_search('form-control', $element['#attributes']['class'])) !== false) {
    unset($element['#attributes']['class'][$class]);
    $element['#attributes']['style'] = 'width: 100%';
  }
  return $element;
}

function latamggv1_process_button(&$vars) {
  $element = &$vars['element'];
  if(isset($element['#button_classes']) && $element['#button_classes'] === FALSE) {
    $element['#attributes']['class'] = array_diff($element['#attributes']['class'], [
      'btn',
      'btn-default',
      'btn-primary',
      'btn-success',
      'btn-info',
      'btn-warning',
      'btn-danger',
      'btn-link'
    ]);
  }
}

function latamggv1_preprocess_region(&$vars){
  switch($vars['region']){
    case 'footer':
      $vars['classes_array'][] = 'container';
      break;
  }
}

/**
 * Styles `newsletter_subscribe` mailchimp block.
 */
function latamggv1_form_mailchimp_signup_subscribe_block_newsletter_subscribe_form_alter(&$form) {
  $form['submit']['#attributes']['class'][] = 'btn-primary';
  $form['mergevars']['EMAIL'] = array_merge($form['mergevars']['EMAIL'], array(
    '#field_suffix' => render($form['submit']),
    '#title_display' => 'invisible',
    '#input_group_button' => TRUE,
    '#type' => 'emailfield',
    '#attributes' => [
      'placeholder' => 'Tu email'
    ]
  ));
}

function latamggv1_form_views_exposed_form_alter(&$form, &$form_state) {
  $view = $form_state['view'];

  switch($view->name){
    case 'site_search':
      _latamggv1_form_site_search_views_exposed_alter($form, $view);
      break;
  }
}

/**
 * Markup and style changes to the site-wide search form's filters.
 */
function _latamggv1_form_site_search_views_exposed_alter(&$form, $view) {
  $form['query'] = array_merge($form['query'], [
    '#title_display' => 'invisible',
    '#type' => 'searchfield',
    '#theme_wrappers' => [],
    '#prefix' => '<div class="input-group input-group-lg">',
    '#suffix' => '<span class="input-group-btn">' . drupal_render($form['submit']) . '</span></div>',
  ]);
}

/**
 * Overrides theme_breadcrumb().
 * TODO Use microdata
 */
function latamggv1_breadcrumb(&$vars) {
  return theme('item_list', array(
    'attributes' => array(
      'class' => array('breadcrumb'),
    ),
    'items' => $vars['breadcrumb'],
    'type' => 'ol',
  ));
}

/**
 * Implements hook_preprocess_panels_pane().
 */
function latamggv1_preprocess_panels_pane(&$vars) {
  switch ($vars['pane']->type) {
    case 'block':
      switch ($vars['pane']->subtype) {
        case 'menu-menu-article-categories':
          _latamggv1_preprocess_pane_menu_article_categories($vars);
          break;
      }
  }
}

/**
 * Implements hook_preprocess_clean_markup_panels_clean_element().
 */
function latamggv1_preprocess_clean_markup_panels_clean_element(&$vars){
  switch ($vars['pane']->type) {
    case 'views_panes':
      switch ($vars['pane']->subtype) {
        case 'site_search-panel_pane_1':
          _latamggv1_preprocess_pane_views_site_search_panel_pane_1($vars);
          break;
      }
  }
}

/**
 * Applies theming to the site_search view.
 */
function _latamggv1_preprocess_pane_views_site_search_panel_pane_1(&$vars) {
  $GLOBALS['skip_page_title'] = TRUE;
  $vars['title_attributes_array']['class'][] = 'h2';

  // Disables AJAX submitting of the form, while maintaining views_load_more AJAX paging
  drupal_add_js("jQuery(function(){ jQuery('#edit-submit-site-search').off('click') })", [
    'type' => 'inline',
    'group' => JS_THEME,
    'scope' => 'footer'
  ]);
}

/**
 * Adds custom theme wrapper for the article_categories menu inside a panel pane.
 */
function _latamggv1_preprocess_pane_menu_article_categories(&$vars) {
  $GLOBALS['skip_page_title'] = TRUE;
  $vars['title_attributes_array']['class'][] = 'h2';
  $vars['content']['#theme_wrappers'] = ['menu_tree__menu_article_categories__panels_pane'];
}

function latamggv1_menu_tree__menu_article_categories__panels_pane(&$vars) {
  drupal_add_js(latamggv1_path() . '/js/shrink-menu.js', ['group' => JS_THEME, 'scope' => 'footer']);
  return '<ul class="menu nav nav-pills shrink-menu art-cat-menu"
    data-shrink-class="form-control"
    data-shrink-query="(max-width: 767px)"
  >' . $vars['tree'] . '</ul>';
}

/**
 * Implements hook_preprocess_node().
 * Adds custom metada information.
 */
function latamggv1_preprocess_node(&$vars) {
  $node = $vars['node'];

  $vars['title_attributes_array']['itemprop'] = 'headline';

  switch($vars['view_mode']){
    case 'teaser':
      $vars['title_attributes_array']['class'][] = 'h4';
      break;
    case 'thumbnail':
      $vars['classes_array'][] = 'node-thumbnail';
      break;
    case 'full':
      $vars['classes_array'][] = 'node-full';
      break;
  }

  if($node->type == 'article') {
    switch($vars['view_mode']){
      case 'teaser':
        $vars['display_submitted'] = TRUE;
        $vars['submitted'] = latamggv1_time_ago($node->created);
        break;
      case 'full':
        $vars['display_submitted'] = TRUE;
        $vars['submitted'] = format_date($node->created, 'custom', 'j M Y');
        break;
    }
  }
}

function latamggv1_node_view_alter(&$build){
  $node = $build['#node'];

  if($build['#view_mode'] == 'full'){
    // Template uses custom title location
    $GLOBALS['skip_page_title'] = TRUE;

    $url = url("node/{$node->nid}", ['absolute' => TRUE]);

    if($node->type == 'article') {
      $build['share_top'] = [
          'facebook' => ['#markup' => _latamggv1_node_share_facebook($url, 'top')],
          'twitter' => ['#markup' => _latamggv1_node_share_twitter($url, $node->title) ]
      ];
    }

    $build['share_bottom'] = [
      'facebook' => ['#markup' => _latamggv1_node_share_facebook($url, 'bottom')],
      'twitter' => ['#markup' => _latamggv1_node_share_twitter($url, $node->title) ]
    ];
  }
}

function _latamggv1_node_share_facebook($url, $ref){
  return '<div
    class="fb-like"
    data-href="' . $url . '"' .
    'data-layout="button"
    data-action="like"
    data-show-faces="false"
    data-share="true"
    data-ref="web_node_' . $ref . '"' .
  '></div>';
}

function _latamggv1_node_share_twitter($url, $title) {
  return '<a href="https://twitter.com/share" class="twitter-share-button"
    data-text="' .  check_plain($title) . '"
    data-url="' . $url . '"
    data-via="LatamGG"
    data-lang="es">
  Twittear</a>';
}

function latamggv1_time_ago($date) {
  $periods = array("segundo", "minuto", "hora", "día", "semana", "mes", "año");
  $lengths = array("60","60","24","7","4.35","12");

  $now = time();

  if(empty($date)) {
    return "Fecha incorrecta";
  }
  if($now > $date) {
    $diff     = $now - $date;
    $tense         = "Hace";
  } else {
    $diff = $date - $now;
    $tense = "Dentro de";
  }
  for($j = 0; $diff >= $lengths[$j] && $j < count($lengths)-1; $j++) {
    $diff /= $lengths[$j];
  }

  $diff = round($diff);

  if($diff != 1) {
    $periods[5].="e";
    $periods[$j].= "s";
  }

  return "$tense $diff $periods[$j]";
}

function latamggv1_views_load_more_pager($vars) {
  global $pager_page_array, $pager_total;

  $tags = $vars['tags'];
  $element = $vars['element'];
  $parameters = $vars['parameters'];

  $pager_classes = array('pager', 'pager-load-more');

  $li_next = theme('pager_next',
    array(
      'text' => (isset($tags[3]) ? $tags[3] : t($vars['more_button_text'])),
      'element' => $element,
      'interval' => 1,
      'parameters' => $parameters,
    )
  );
  if (empty($li_next)) {
    $li_next = empty($vars['more_button_empty_text']) ? '&nbsp;' : t($vars['more_button_empty_text']);
    $pager_classes[] = 'pager-load-more-empty';
  }

  if ($pager_total[$element] > 1) {
    $items[] = array(
      'class' => array('pager-next'),
      'data' => $li_next,
    );
    return theme('item_list',
      array(
        'items' => $items,
        'title' => NULL,
        'type' => 'ul',
        'attributes' => array('class' => $pager_classes),
      )
    );
  }
}

function _latamggv1_icon($name, $role, $attrs = []){
  $attrs['role'] = $role;
  $attrs['class'][] = 'ggshape';
  $attrs['class'][] = 'ggshape-' . $name;
  return '<svg' . drupal_attributes($attrs) .  '><use xlink:href="#shape-' . check_plain($name) . '"></use></svg>';
}