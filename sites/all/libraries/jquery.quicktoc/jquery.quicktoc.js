( function($, window, undefined) {"use strict";
	
	var QuickToc = function(el, options) {
		this.el = el;
		this.options = options;
		
		this.create();
	};
	
	QuickToc.prototype = {
		create: function(){
			var selectors = this.options.selectors.toUpperCase().split(','),
				tags = $(this.options.selectors, this.options.container),
				toc = [],
				list = [],
				currList = list,
				newList,
				listOnLevel = { 0: list },
				level,
				tagLevels = {},
				tagLevel,
				prevLevel = 1;
				
			for(var i = 0, len = selectors.length; i < len; ++i)
				tagLevels[selectors[i]] = i+1;
			
			// Take advantage of the fact that elements are returned in document order
			tags.each(function(){				
				tagLevel = tagLevels[this.tagName];
				
				if(prevLevel < tagLevel) {
					currList = listOnLevel[prevLevel];
				} else if(prevLevel > tagLevel){
					level = tagLevel;
					while(level--){
						if(listOnLevel[level]){
							currList = listOnLevel[level];
							break;
						}
					}				
				}
				
				newList = [this];
				listOnLevel[tagLevel] = newList;
				currList.push(newList);
				prevLevel = tagLevel;	
			});
			
			this.render(list);
		},
		
		render: function(tagList) {
			var html = '';
			
			for(var i = 0, len = tagList.length; i < len; ++i) {
				html += this.renderList(tagList[i]);
			}
			
			this.el.html('<ol class="quicktoc">' + html + '</ol>');
		},
		
		renderList: function(list) {
			var len = list.length,
				html;	
						
			html = '<li>' + this.renderLink(list[0]);	
			if(len > 1){
				html += '<ol>';
				for(var i = 1; i < len; ++i) {
					html += this.renderList(list[i]);
				}	
				html += '</ol>';
			}									
			html += '</li>';			
			
			return html;
		},
				
		renderLink: function(item) {
			if(!item.id) this.generateId(item);
			return '<a href="#' + item.id + '" rel="internal">' + $(item).text() + '</a>';
		},
		
		generateId: function(item) {
			var id = $(item).text().replace(/[^a-z1-9_\-]+/ig, "").substr(0, 20);
			$(item).attr('id', id);
		}
	};	

	$.fn.quicktoc = function(options) {
		var settings = $.extend({
			container: 'body',
			selectors: 'h1,h2,h3'
		}, options);
				
		this.data('quicktoc', new QuickToc(this, options));
		
		return this;
	};

}(jQuery, window, undefined));
