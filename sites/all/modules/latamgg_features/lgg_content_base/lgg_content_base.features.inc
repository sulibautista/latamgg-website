<?php
/**
 * @file
 * lgg_content_base.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function lgg_content_base_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
  if ($module == "scald" && $api == "context_config") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function lgg_content_base_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function lgg_content_base_image_default_styles() {
  $styles = array();

  // Exported image style: latamgg_subroutine_set_quality_x1_5.
  $styles['latamgg_subroutine_set_quality_x1_5'] = array(
    'name' => 'latamgg_subroutine_set_quality_x1_5',
    'label' => 'latamgg_subroutine_set_quality_x1_5',
    'effects' => array(
      45 => array(
        'label' => 'Change file format',
        'help' => 'Choose to save the image as a different filetype.',
        'effect callback' => 'coloractions_convert_effect',
        'dimensions passthrough' => TRUE,
        'form callback' => 'coloractions_convert_form',
        'summary theme' => 'coloractions_convert_summary',
        'module' => 'imagecache_coloractions',
        'name' => 'coloractions_convert',
        'data' => array(
          'format' => 'image/jpeg',
          'quality' => 60,
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: latamgg_subroutine_set_quality_x2.
  $styles['latamgg_subroutine_set_quality_x2'] = array(
    'name' => 'latamgg_subroutine_set_quality_x2',
    'label' => 'latamgg_subroutine_set_quality_x2',
    'effects' => array(
      42 => array(
        'label' => 'Change file format',
        'help' => 'Choose to save the image as a different filetype.',
        'effect callback' => 'coloractions_convert_effect',
        'dimensions passthrough' => TRUE,
        'form callback' => 'coloractions_convert_form',
        'summary theme' => 'coloractions_convert_summary',
        'module' => 'imagecache_coloractions',
        'name' => 'coloractions_convert',
        'data' => array(
          'format' => 'image/jpeg',
          'quality' => 40,
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: latamgg_subroutine_set_quality_x3.
  $styles['latamgg_subroutine_set_quality_x3'] = array(
    'name' => 'latamgg_subroutine_set_quality_x3',
    'label' => 'latamgg_subroutine_set_quality_x3',
    'effects' => array(
      47 => array(
        'label' => 'Change file format',
        'help' => 'Choose to save the image as a different filetype.',
        'effect callback' => 'coloractions_convert_effect',
        'dimensions passthrough' => TRUE,
        'form callback' => 'coloractions_convert_form',
        'summary theme' => 'coloractions_convert_summary',
        'module' => 'imagecache_coloractions',
        'name' => 'coloractions_convert',
        'data' => array(
          'format' => 'image/jpeg',
          'quality' => 30,
        ),
        'weight' => 0,
      ),
    ),
  );

  return $styles;
}
