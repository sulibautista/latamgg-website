<?php
/**
 * @file
 * lgg_content_base.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_handlers().
 */
function lgg_content_base_default_page_manager_handlers() {
  $export = array();

  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'node_edit_panel_context';
  $handler->task = 'node_edit';
  $handler->subtask = '';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Default node edit',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'bootstrap_twocol_stacked';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'top' => NULL,
      'left' => array(
        'column' => array(
          'xs' => '0',
          'sm' => '0',
          'md' => '8',
          'lg' => '0',
        ),
        'offset' => array(
          'xs' => '0',
          'sm' => '0',
          'md' => '0',
          'lg' => '0',
        ),
        'push' => array(
          'xs' => '0',
          'sm' => '0',
          'md' => '0',
          'lg' => '0',
        ),
        'pull' => array(
          'xs' => '0',
          'sm' => '0',
          'md' => '0',
          'lg' => '0',
        ),
      ),
      'right' => array(
        'column' => array(
          'xs' => '0',
          'sm' => '0',
          'md' => '4',
          'lg' => '0',
        ),
        'offset' => array(
          'xs' => '0',
          'sm' => '0',
          'md' => '0',
          'lg' => '0',
        ),
        'push' => array(
          'xs' => '0',
          'sm' => '0',
          'md' => '0',
          'lg' => '0',
        ),
        'pull' => array(
          'xs' => '0',
          'sm' => '0',
          'md' => '0',
          'lg' => '0',
        ),
      ),
      'bottom' => NULL,
    ),
    'left' => array(
      'style' => 'bootstrap_region',
    ),
    'right' => array(
      'style' => 'bootstrap_region',
    ),
    'top' => array(
      'style' => 'naked',
    ),
    'bottom' => array(
      'style' => 'naked',
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '730b21ea-3cc4-4f94-b423-a3b72f07511e';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-b4165f24-2c3b-4a7b-a8f7-031af0e81761';
    $pane->panel = 'left';
    $pane->type = 'form';
    $pane->subtype = 'form';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'context' => 'argument_node_edit_1',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'b4165f24-2c3b-4a7b-a8f7-031af0e81761';
    $display->content['new-b4165f24-2c3b-4a7b-a8f7-031af0e81761'] = $pane;
    $display->panels['left'][0] = 'new-b4165f24-2c3b-4a7b-a8f7-031af0e81761';
    $pane = new stdClass();
    $pane->pid = 'new-4e388134-cf8f-44f7-a6d8-8312a06b14a6';
    $pane->panel = 'right';
    $pane->type = 'entity_form_field';
    $pane->subtype = 'node:field_games';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => '',
      'formatter' => '',
      'context' => 'argument_node_edit_1',
      'override_title' => 0,
      'override_title_text' => '%title',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'naked',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '4e388134-cf8f-44f7-a6d8-8312a06b14a6';
    $display->content['new-4e388134-cf8f-44f7-a6d8-8312a06b14a6'] = $pane;
    $display->panels['right'][0] = 'new-4e388134-cf8f-44f7-a6d8-8312a06b14a6';
    $pane = new stdClass();
    $pane->pid = 'new-55910b1b-5d59-49a1-bd13-d947834148eb';
    $pane->panel = 'right';
    $pane->type = 'entity_form_field';
    $pane->subtype = 'node:field_categories';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => '',
      'formatter' => '',
      'context' => 'argument_node_edit_1',
      'override_title' => 0,
      'override_title_text' => 'Categorías',
    );
    $pane->cache = array();
    $pane->style = array(
      'style' => 'naked',
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '55910b1b-5d59-49a1-bd13-d947834148eb';
    $display->content['new-55910b1b-5d59-49a1-bd13-d947834148eb'] = $pane;
    $display->panels['right'][1] = 'new-55910b1b-5d59-49a1-bd13-d947834148eb';
    $pane = new stdClass();
    $pane->pid = 'new-d14cc6b0-12a2-4724-9c02-551b849b7ca1';
    $pane->panel = 'right';
    $pane->type = 'entity_form_field';
    $pane->subtype = 'node:field_tags';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => '',
      'formatter' => '',
      'context' => 'argument_node_edit_1',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'naked',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = 'd14cc6b0-12a2-4724-9c02-551b849b7ca1';
    $display->content['new-d14cc6b0-12a2-4724-9c02-551b849b7ca1'] = $pane;
    $display->panels['right'][2] = 'new-d14cc6b0-12a2-4724-9c02-551b849b7ca1';
    $pane = new stdClass();
    $pane->pid = 'new-9656b756-42e6-4b04-be4f-0ba812291198';
    $pane->panel = 'right';
    $pane->type = 'entity_form_field';
    $pane->subtype = 'node:field_image';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => '',
      'formatter' => '',
      'context' => 'argument_node_edit_1',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'naked',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 3;
    $pane->locks = array();
    $pane->uuid = '9656b756-42e6-4b04-be4f-0ba812291198';
    $display->content['new-9656b756-42e6-4b04-be4f-0ba812291198'] = $pane;
    $display->panels['right'][3] = 'new-9656b756-42e6-4b04-be4f-0ba812291198';
    $pane = new stdClass();
    $pane->pid = 'new-439037ae-6de7-4a83-8a66-4b43dc92f5c3';
    $pane->panel = 'right';
    $pane->type = 'node_form_publishing';
    $pane->subtype = 'node_form_publishing';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'context' => 'argument_node_edit_1',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'style' => 'clean_element',
      'settings' => array(
        'clean_markup' => array(
          'pane_wrapper' => 'div',
          'additional_pane_classes' => 'panel-pane',
          'additional_pane_attributes' => '',
          'enable_inner_div' => 0,
          'title_wrapper' => 'h3',
          'title_hide' => 0,
          'content_wrapper' => 'none',
        ),
      ),
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 4;
    $pane->locks = array();
    $pane->uuid = '439037ae-6de7-4a83-8a66-4b43dc92f5c3';
    $display->content['new-439037ae-6de7-4a83-8a66-4b43dc92f5c3'] = $pane;
    $display->panels['right'][4] = 'new-439037ae-6de7-4a83-8a66-4b43dc92f5c3';
    $pane = new stdClass();
    $pane->pid = 'new-79d76a59-58e8-4015-8a98-944b04f7265d';
    $pane->panel = 'right';
    $pane->type = 'node_form_log';
    $pane->subtype = 'node_form_log';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'context' => 'argument_node_edit_1',
      'override_title' => 1,
      'override_title_text' => '%title',
    );
    $pane->cache = array();
    $pane->style = array(
      'style' => 'clean_element',
      'settings' => array(
        'clean_markup' => array(
          'pane_wrapper' => 'div',
          'additional_pane_classes' => 'panel-pane',
          'additional_pane_attributes' => '',
          'enable_inner_div' => 0,
          'title_wrapper' => 'h3',
          'title_hide' => 0,
          'content_wrapper' => 'none',
        ),
      ),
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 5;
    $pane->locks = array();
    $pane->uuid = '79d76a59-58e8-4015-8a98-944b04f7265d';
    $display->content['new-79d76a59-58e8-4015-8a98-944b04f7265d'] = $pane;
    $display->panels['right'][5] = 'new-79d76a59-58e8-4015-8a98-944b04f7265d';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-b4165f24-2c3b-4a7b-a8f7-031af0e81761';
  $handler->conf['display'] = $display;
  $export['node_edit_panel_context'] = $handler;

  return $export;
}
