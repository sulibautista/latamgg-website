<?php
/**
 * @file
 * lgg_menus.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function lgg_menus_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-latamgg-paginas_contacto:contact
  $menu_links['menu-latamgg-paginas_contacto:contact'] = array(
    'menu_name' => 'menu-latamgg-paginas',
    'link_path' => 'contact',
    'router_path' => 'contact',
    'link_title' => 'Contacto',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-latamgg-paginas_contacto:contact',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Contacto');


  return $menu_links;
}
