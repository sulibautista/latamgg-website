<?php
/**
 * @file
 * lgg_menus.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function lgg_menus_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-alternate-principal-menu.
  $menus['menu-alternate-principal-menu'] = array(
    'menu_name' => 'menu-alternate-principal-menu',
    'title' => 'Menu Principal Alterno',
    'description' => 'Menu principal inicial, cuando se desarrollen las secciones propuestas, se cambiara por el menú principal.',
  );
  // Exported menu: menu-latamgg-paginas.
  $menus['menu-latamgg-paginas'] = array(
    'menu_name' => 'menu-latamgg-paginas',
    'title' => 'LatamGG Paginas',
    'description' => '',
  );
  // Exported menu: menu-paginas-populares.
  $menus['menu-paginas-populares'] = array(
    'menu_name' => 'menu-paginas-populares',
    'title' => 'Paginas Populares',
    'description' => '',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('LatamGG Paginas');
  t('Menu Principal Alterno');
  t('Menu principal inicial, cuando se desarrollen las secciones propuestas, se cambiara por el menú principal.');
  t('Paginas Populares');


  return $menus;
}
