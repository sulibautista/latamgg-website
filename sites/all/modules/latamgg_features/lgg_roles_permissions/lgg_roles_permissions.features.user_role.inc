<?php
/**
 * @file
 * lgg_roles_permissions.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function lgg_roles_permissions_user_default_roles() {
  $roles = array();

  // Exported role: administrator.
  $roles['administrator'] = array(
    'name' => 'administrator',
    'weight' => 3,
  );

  // Exported role: colaborador.
  $roles['colaborador'] = array(
    'name' => 'colaborador',
    'weight' => 2,
  );

  return $roles;
}
