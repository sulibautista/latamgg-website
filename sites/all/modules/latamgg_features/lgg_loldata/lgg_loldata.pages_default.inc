<?php
/**
 * @file
 * lgg_loldata.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_handlers().
 */
function lgg_loldata_default_page_manager_handlers() {
  $export = array();

  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'loldata_view_http_response';
  $handler->task = 'loldata_view';
  $handler->subtask = '';
  $handler->handler = 'http_response';
  $handler->weight = -30;
  $handler->conf = array(
    'title' => '404 Under Construction LoLData',
    'contexts' => array(),
    'relationships' => array(),
    'code' => '404',
    'destination' => '',
    'access' => array(
      'plugins' => array(
        1 => array(
          'name' => 'role',
          'settings' => array(
            'rids' => array(
              0 => 3,
            ),
          ),
          'context' => 'logged-in-user',
          'not' => TRUE,
        ),
      ),
      'logic' => 'and',
    ),
  );
  $export['loldata_view_http_response'] = $handler;

  return $export;
}
