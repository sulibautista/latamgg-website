<?php
/**
 * @file
 * lgg_loldata.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function lgg_loldata_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'loldata_panels_loldata_view_disabled';
  $strongarm->value = FALSE;
  $export['loldata_panels_loldata_view_disabled'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'loldata_update:html';
  $strongarm->value = 'raw_html';
  $export['loldata_update:html'] = $strongarm;

  return $export;
}
