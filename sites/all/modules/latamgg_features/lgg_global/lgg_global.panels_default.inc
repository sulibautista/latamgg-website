<?php
/**
 * @file
 * lgg_global.panels_default.inc
 */

/**
 * Implements hook_default_panels_mini().
 */
function lgg_global_default_panels_mini() {
  $export = array();

  $mini = new stdClass();
  $mini->disabled = FALSE; /* Edit this to true to make a default mini disabled initially */
  $mini->api_version = 1;
  $mini->name = 'footer';
  $mini->category = '';
  $mini->admin_title = 'Footer';
  $mini->admin_description = '';
  $mini->requiredcontexts = array();
  $mini->contexts = array();
  $mini->relationships = array();
  $display = new panels_display();
  $display->layout = 'bs_four_col';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'left' => array(
        'column' => array(
          'xs' => '12',
          'sm' => '4',
          'md' => '0',
          'lg' => '0',
        ),
        'offset' => array(
          'xs' => '0',
          'sm' => '0',
          'md' => '0',
          'lg' => '0',
        ),
        'push' => array(
          'xs' => '0',
          'sm' => '0',
          'md' => '0',
          'lg' => '0',
        ),
        'pull' => array(
          'xs' => '0',
          'sm' => '0',
          'md' => '0',
          'lg' => '0',
        ),
      ),
      'center_left' => array(
        'clean_markup' => array(
          'region_wrapper' => 'div',
          'additional_region_classes' => 'panel-panel center_left col-xxs-6 col-sm-2',
          'additional_region_attributes' => '',
          'enable_inner_div' => 0,
          'pane_separators' => 0,
        ),
      ),
      'center_right' => array(
        'clean_markup' => array(
          'region_wrapper' => 'div',
          'additional_region_classes' => 'panel-panel center_right col-xxs-6 col-sm-2',
          'additional_region_attributes' => '',
          'enable_inner_div' => 0,
          'pane_separators' => 0,
        ),
      ),
      'right' => array(
        'clean_markup' => array(
          'region_wrapper' => 'div',
          'additional_region_classes' => 'panel-panel panel-row right col-xxs-12 col-sm-4',
          'additional_region_attributes' => '',
          'enable_inner_div' => 1,
          'pane_separators' => 0,
        ),
      ),
      'default' => NULL,
    ),
    'center_left' => array(
      'style' => 'clean_element',
    ),
    'center_right' => array(
      'style' => 'clean_element',
    ),
    'left' => array(
      'style' => 'bootstrap_region',
    ),
    'right' => array(
      'style' => 'clean_element',
    ),
    'style' => 'naked',
  );
  $display->cache = array(
    'method' => '0',
    'settings' => array(),
  );
  $display->title = '';
  $display->uuid = '3ca637cf-604b-4d94-99ff-931649edc93a';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-a35208c4-074a-4fd9-b083-9c1599fdf683';
    $pane->panel = 'center_left';
    $pane->type = 'block';
    $pane->subtype = 'menu-menu-latamgg-paginas';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => 'LatamGG',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => array(
        'clean_markup' => array(
          'pane_wrapper' => 'div',
          'additional_pane_classes' => 'panel-pane',
          'additional_pane_attributes' => '',
          'enable_inner_div' => 0,
          'title_wrapper' => 'h4',
          'title_hide' => 0,
          'content_wrapper' => 'none',
        ),
      ),
      'style' => 'clean_element',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'a35208c4-074a-4fd9-b083-9c1599fdf683';
    $display->content['new-a35208c4-074a-4fd9-b083-9c1599fdf683'] = $pane;
    $display->panels['center_left'][0] = 'new-a35208c4-074a-4fd9-b083-9c1599fdf683';
    $pane = new stdClass();
    $pane->pid = 'new-73dca581-c068-49fb-9f38-518bd106622b';
    $pane->panel = 'center_right';
    $pane->type = 'block';
    $pane->subtype = 'menu-menu-paginas-populares';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => 'No te pierdas',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => array(
        'clean_markup' => array(
          'pane_wrapper' => 'div',
          'additional_pane_classes' => 'panel-pane',
          'additional_pane_attributes' => '',
          'enable_inner_div' => 0,
          'title_wrapper' => 'h4',
          'title_hide' => 0,
          'content_wrapper' => 'none',
        ),
      ),
      'style' => 'clean_element',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '73dca581-c068-49fb-9f38-518bd106622b';
    $display->content['new-73dca581-c068-49fb-9f38-518bd106622b'] = $pane;
    $display->panels['center_right'][0] = 'new-73dca581-c068-49fb-9f38-518bd106622b';
    $pane = new stdClass();
    $pane->pid = 'new-5d9c1866-214e-4b4a-b905-9aea647e7b93';
    $pane->panel = 'left';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => 'Logo Footer',
      'title' => '',
      'body' => '<div class="footer-logo">
  <svg viewBox="0 0 335.563 121.32" preserveAspectRatio="xMidYMin slice" class="ggshape ggshape-resp ggshape-latamgg-logo-normal" role="img" aria-label="LatamGG logo">
    <use xlink:href="#shape-latamgg-logo-normal"></use>
  </svg>
</div>
<div class="footer-mission">
  <p>Comprometidos con impulsar el nivel competitivo de los eSports en América Latina.</p>
</div>',
      'format' => 'raw_html',
      'substitute' => TRUE,
    );
    $pane->cache = array();
    $pane->style = array(
      'style' => 'naked',
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '5d9c1866-214e-4b4a-b905-9aea647e7b93';
    $display->content['new-5d9c1866-214e-4b4a-b905-9aea647e7b93'] = $pane;
    $display->panels['left'][0] = 'new-5d9c1866-214e-4b4a-b905-9aea647e7b93';
    $pane = new stdClass();
    $pane->pid = 'new-aad40b3c-5da0-42e0-8575-fe4a1edc2ba7';
    $pane->panel = 'right';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => '',
      'title' => '',
      'body' => '',
      'format' => 'full_html',
      'substitute' => TRUE,
      'name' => 'mailchimp_newsletter_subscribe_small',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => array(
        'clean_markup' => array(
          'pane_wrapper' => 'div',
          'additional_pane_classes' => 'col-xxs-12 col-xs-6 col-sm-12',
          'additional_pane_attributes' => '',
          'enable_inner_div' => 0,
          'title_wrapper' => 'h4',
          'title_hide' => 0,
          'content_wrapper' => 'none',
        ),
      ),
      'style' => 'clean_element',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'aad40b3c-5da0-42e0-8575-fe4a1edc2ba7';
    $display->content['new-aad40b3c-5da0-42e0-8575-fe4a1edc2ba7'] = $pane;
    $display->panels['right'][0] = 'new-aad40b3c-5da0-42e0-8575-fe4a1edc2ba7';
    $pane = new stdClass();
    $pane->pid = 'new-ccec6cfc-44dc-4ec4-95a4-7f37996101c1';
    $pane->panel = 'right';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => 'Social Links',
      'title' => 'Encuentranos en',
      'body' => '<a href="https://www.facebook.com/LatamGG">
  <svg title="Facebook" role="img" class="ggshape ggshape-facebook">
    <use xlink:href="#shape-facebook"></use>
  </svg>
</a>

<a href="https://twitter.com/LatamGG">
  <svg title="Twitter" role="img" class="ggshape ggshape-twitter">
    <use xlink:href="#shape-twitter"></use>
  </svg>
</a>

<a href="http://www.twitch.tv/LatamGG">
  <svg title="Twitch.tv" role="img" class="ggshape ggshape-twitch">
    <use xlink:href="#shape-twitch"></use>
  </svg>
</a>',
      'format' => 'raw_html',
      'substitute' => TRUE,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => array(
        'clean_markup' => array(
          'pane_wrapper' => 'div',
          'additional_pane_classes' => 'col-xxs-12 col-xs-6 col-sm-12',
          'additional_pane_attributes' => '',
          'enable_inner_div' => 0,
          'title_wrapper' => 'h4',
          'title_hide' => 0,
          'content_wrapper' => 'none',
        ),
      ),
      'style' => 'clean_element',
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'social-footer',
    );
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = 'ccec6cfc-44dc-4ec4-95a4-7f37996101c1';
    $display->content['new-ccec6cfc-44dc-4ec4-95a4-7f37996101c1'] = $pane;
    $display->panels['right'][1] = 'new-ccec6cfc-44dc-4ec4-95a4-7f37996101c1';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = '0';
  $mini->display = $display;
  $export['footer'] = $mini;

  return $export;
}
