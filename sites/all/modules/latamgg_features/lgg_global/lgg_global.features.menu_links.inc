<?php
/**
 * @file
 * lgg_global.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function lgg_global_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu_campeones:league-of-legends/campeones
  $menu_links['main-menu_campeones:league-of-legends/campeones'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'league-of-legends/campeones',
    'router_path' => 'league-of-legends/campeones',
    'link_title' => 'Campeones',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_campeones:league-of-legends/campeones',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -44,
    'customized' => 1,
  );
  // Exported menu link: main-menu_esports:taxonomy/term/33
  $menu_links['main-menu_esports:taxonomy/term/33'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'taxonomy/term/33',
    'router_path' => 'taxonomy/term/%',
    'link_title' => 'eSports',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_esports:taxonomy/term/33',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -47,
    'customized' => 1,
  );
  // Exported menu link: main-menu_guias:league-of-legends/guias
  $menu_links['main-menu_guias:league-of-legends/guias'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'league-of-legends/guias',
    'router_path' => 'league-of-legends/guias',
    'link_title' => 'Guias',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_guias:league-of-legends/guias',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -46,
    'customized' => 1,
  );
  // Exported menu link: main-menu_noticias:noticias
  $menu_links['main-menu_noticias:noticias'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'noticias',
    'router_path' => 'noticias',
    'link_title' => 'Noticias',
    'options' => array(
      'identifier' => 'main-menu_noticias:noticias',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
  );
  // Exported menu link: main-menu_pbe:taxonomy/term/38
  $menu_links['main-menu_pbe:taxonomy/term/38'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'taxonomy/term/38',
    'router_path' => 'taxonomy/term/%',
    'link_title' => 'PBE',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_pbe:taxonomy/term/38',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -43,
    'customized' => 1,
  );
  // Exported menu link: menu-alternate-principal-menu_actualizaciones:taxonomy/term/34
  $menu_links['menu-alternate-principal-menu_actualizaciones:taxonomy/term/34'] = array(
    'menu_name' => 'menu-alternate-principal-menu',
    'link_path' => 'taxonomy/term/34',
    'router_path' => 'taxonomy/term/%',
    'link_title' => 'Actualizaciones',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-alternate-principal-menu_actualizaciones:taxonomy/term/34',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
  );
  // Exported menu link: menu-alternate-principal-menu_diversin:taxonomy/term/37
  $menu_links['menu-alternate-principal-menu_diversin:taxonomy/term/37'] = array(
    'menu_name' => 'menu-alternate-principal-menu',
    'link_path' => 'taxonomy/term/37',
    'router_path' => 'taxonomy/term/%',
    'link_title' => 'Diversión',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-alternate-principal-menu_diversin:taxonomy/term/37',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -47,
    'customized' => 1,
  );
  // Exported menu link: menu-alternate-principal-menu_esports:taxonomy/term/33
  $menu_links['menu-alternate-principal-menu_esports:taxonomy/term/33'] = array(
    'menu_name' => 'menu-alternate-principal-menu',
    'link_path' => 'taxonomy/term/33',
    'router_path' => 'taxonomy/term/%',
    'link_title' => 'eSports',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-alternate-principal-menu_esports:taxonomy/term/33',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
  );
  // Exported menu link: menu-alternate-principal-menu_estrategia:taxonomy/term/32
  $menu_links['menu-alternate-principal-menu_estrategia:taxonomy/term/32'] = array(
    'menu_name' => 'menu-alternate-principal-menu',
    'link_path' => 'taxonomy/term/32',
    'router_path' => 'taxonomy/term/%',
    'link_title' => 'Estrategia',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-alternate-principal-menu_estrategia:taxonomy/term/32',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
  );
  // Exported menu link: menu-alternate-principal-menu_eventos:taxonomy/term/35
  $menu_links['menu-alternate-principal-menu_eventos:taxonomy/term/35'] = array(
    'menu_name' => 'menu-alternate-principal-menu',
    'link_path' => 'taxonomy/term/35',
    'router_path' => 'taxonomy/term/%',
    'link_title' => 'Eventos',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-alternate-principal-menu_eventos:taxonomy/term/35',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -46,
    'customized' => 1,
  );
  // Exported menu link: menu-latamgg-paginas_colaboradores:<front>
  $menu_links['menu-latamgg-paginas_colaboradores:<front>'] = array(
    'menu_name' => 'menu-latamgg-paginas',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Colaboradores',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-latamgg-paginas_colaboradores:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
  );
  // Exported menu link: menu-latamgg-paginas_contacto:<front>
  $menu_links['menu-latamgg-paginas_contacto:<front>'] = array(
    'menu_name' => 'menu-latamgg-paginas',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Contacto',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-latamgg-paginas_contacto:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -46,
    'customized' => 1,
  );
  // Exported menu link: menu-latamgg-paginas_nosotros:<front>
  $menu_links['menu-latamgg-paginas_nosotros:<front>'] = array(
    'menu_name' => 'menu-latamgg-paginas',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Nosotros',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-latamgg-paginas_nosotros:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
  );
  // Exported menu link: menu-latamgg-paginas_partners:<front>
  $menu_links['menu-latamgg-paginas_partners:<front>'] = array(
    'menu_name' => 'menu-latamgg-paginas',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Partners',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-latamgg-paginas_partners:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -47,
    'customized' => 1,
  );
  // Exported menu link: menu-latamgg-paginas_publicidad:<front>
  $menu_links['menu-latamgg-paginas_publicidad:<front>'] = array(
    'menu_name' => 'menu-latamgg-paginas',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Publicidad',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-latamgg-paginas_publicidad:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -45,
    'customized' => 1,
  );
  // Exported menu link: menu-latamgg-paginas_registrarse:user/register
  $menu_links['menu-latamgg-paginas_registrarse:user/register'] = array(
    'menu_name' => 'menu-latamgg-paginas',
    'link_path' => 'user/register',
    'router_path' => 'user/register',
    'link_title' => 'Registrarse',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-latamgg-paginas_registrarse:user/register',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
  );
  // Exported menu link: menu-paginas-populares_crea-tu-gua-:<front>
  $menu_links['menu-paginas-populares_crea-tu-gua-:<front>'] = array(
    'menu_name' => 'menu-paginas-populares',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Crea tu Guía ',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-paginas-populares_crea-tu-gua-:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
  );
  // Exported menu link: menu-paginas-populares_league-of-legends:<front>
  $menu_links['menu-paginas-populares_league-of-legends:<front>'] = array(
    'menu_name' => 'menu-paginas-populares',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'League of Legends',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-paginas-populares_league-of-legends:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
  );
  // Exported menu link: menu-paginas-populares_starcraft-ii:<front>
  $menu_links['menu-paginas-populares_starcraft-ii:<front>'] = array(
    'menu_name' => 'menu-paginas-populares',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'StarCraft II',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-paginas-populares_starcraft-ii:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Actualizaciones');
  t('Campeones');
  t('Colaboradores');
  t('Contacto');
  t('Crea tu Guía ');
  t('Diversión');
  t('Estrategia');
  t('Eventos');
  t('Guias');
  t('League of Legends');
  t('Nosotros');
  t('Noticias');
  t('PBE');
  t('Partners');
  t('Publicidad');
  t('Registrarse');
  t('StarCraft II');
  t('eSports');


  return $menu_links;
}
