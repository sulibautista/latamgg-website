<?php
/**
 * @file
 * lgg_global.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function lgg_global_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: main-menu.
  $menus['main-menu'] = array(
    'menu_name' => 'main-menu',
    'title' => 'Menú principal',
    'description' => 'El menú <em>Principal</em> se usa en muchos sitios web para mostrar las secciones principales del sitio, generalmente en una barra de navegación superior.',
  );
  // Exported menu: menu-alternate-principal-menu.
  $menus['menu-alternate-principal-menu'] = array(
    'menu_name' => 'menu-alternate-principal-menu',
    'title' => 'Menu Principal Alterno',
    'description' => 'Menu principal inicial, cuando se desarrollen las secciones propuestas, se cambiara por el menú principal.',
  );
  // Exported menu: menu-latamgg-paginas.
  $menus['menu-latamgg-paginas'] = array(
    'menu_name' => 'menu-latamgg-paginas',
    'title' => 'LatamGG Paginas',
    'description' => '',
  );
  // Exported menu: menu-paginas-populares.
  $menus['menu-paginas-populares'] = array(
    'menu_name' => 'menu-paginas-populares',
    'title' => 'Paginas Populares',
    'description' => '',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('El menú <em>Principal</em> se usa en muchos sitios web para mostrar las secciones principales del sitio, generalmente en una barra de navegación superior.');
  t('LatamGG Paginas');
  t('Menu Principal Alterno');
  t('Menu principal inicial, cuando se desarrollen las secciones propuestas, se cambiara por el menú principal.');
  t('Menú principal');
  t('Paginas Populares');


  return $menus;
}
