<?php
/**
 * @file
 * lgg_global.features.filter.inc
 */

/**
 * Implements hook_filter_default_formats().
 */
function lgg_global_filter_default_formats() {
  $formats = array();

  // Exported format: HTML Basico.
  $formats['filtered_html'] = array(
    'format' => 'filtered_html',
    'name' => 'HTML Basico',
    'cache' => 1,
    'status' => 1,
    'weight' => -8,
    'filters' => array(
      'filter_autop' => array(
        'weight' => -44,
        'status' => 1,
        'settings' => array(),
      ),
      'loldata_embed' => array(
        'weight' => -43,
        'status' => 1,
        'settings' => array(),
      ),
      'mee_scald_widgets' => array(
        'weight' => -42,
        'status' => 1,
        'settings' => array(),
      ),
      'filter_htmlcorrector' => array(
        'weight' => -41,
        'status' => 1,
        'settings' => array(),
      ),
      'filter_html' => array(
        'weight' => -10,
        'status' => 1,
        'settings' => array(
          'allowed_html' => '<a> <em> <strong> <cite> <blockquote> <code> <ul> <ol> <li> <dl> <dt> <dd>',
          'filter_html_help' => 1,
          'filter_html_nofollow' => 0,
        ),
      ),
    ),
  );

  // Exported format: HTML Completo.
  $formats['full_html'] = array(
    'format' => 'full_html',
    'name' => 'HTML Completo',
    'cache' => 1,
    'status' => 1,
    'weight' => -10,
    'filters' => array(
      'loldata_embed' => array(
        'weight' => -43,
        'status' => 1,
        'settings' => array(),
      ),
      'mee_scald_widgets' => array(
        'weight' => -40,
        'status' => 1,
        'settings' => array(),
      ),
      'filter_htmlcorrector' => array(
        'weight' => -39,
        'status' => 1,
        'settings' => array(),
      ),
    ),
  );

  // Exported format: Plain text.
  $formats['plain_text'] = array(
    'format' => 'plain_text',
    'name' => 'Plain text',
    'cache' => 1,
    'status' => 1,
    'weight' => -7,
    'filters' => array(
      'filter_html_escape' => array(
        'weight' => 0,
        'status' => 1,
        'settings' => array(),
      ),
      'filter_url' => array(
        'weight' => 1,
        'status' => 1,
        'settings' => array(
          'filter_url_length' => 72,
        ),
      ),
      'filter_autop' => array(
        'weight' => 2,
        'status' => 1,
        'settings' => array(),
      ),
    ),
  );

  // Exported format: Raw HTML.
  $formats['raw_html'] = array(
    'format' => 'raw_html',
    'name' => 'Raw HTML',
    'cache' => 1,
    'status' => 1,
    'weight' => -5,
    'filters' => array(),
  );

  return $formats;
}
