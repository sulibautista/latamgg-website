<?php
/**
 * @file
 * lgg_global.ctools_content.inc
 */

/**
 * Implements hook_default_ctools_custom_content().
 */
function lgg_global_default_ctools_custom_content() {
  $export = array();

  $content = new stdClass();
  $content->disabled = FALSE; /* Edit this to true to make a default content disabled initially */
  $content->api_version = 1;
  $content->name = 'become_a_fan';
  $content->admin_title = 'Conviértete en Fan';
  $content->admin_description = '';
  $content->category = 'LatamGG';
  $content->settings = array(
    'admin_title' => '',
    'title' => 'Conviértete en Fan',
    'body' => '<a href="https://twitter.com/LatamGG" class="twitter-follow-button" data-show-count="false">Follow @LatamGG</a>
<iframe src="//www.facebook.com/plugins/like.php?locale=es_LA&amp;href=https%3A%2F%2Ffacebook.com%2FLatamGG&amp;width&amp;layout=standard&amp;action=like&amp;show_faces=true&amp;share=false&amp;height=80&amp;appId=313961565478160&amp;ref=web_become_a_fan_box" scrolling="no" frameborder="0" style="border:none; overflow:hidden;" allowTransparency="true" class="fb-become-a-fan"></iframe>',
    'format' => 'raw_html',
    'substitute' => 0,
  );
  $export['become_a_fan'] = $content;

  $content = new stdClass();
  $content->disabled = FALSE; /* Edit this to true to make a default content disabled initially */
  $content->api_version = 1;
  $content->name = 'mailchimp_newsletter_subscribe_small';
  $content->admin_title = 'Suscripción al Boletín (Solo Email)';
  $content->admin_description = '';
  $content->category = 'LatamGG';
  $content->settings = array(
    'admin_title' => 'Suscripción al Boletín (Solo Email)',
    'title' => 'Suscríbete al Boletín',
    'body' => '<form action="//latamgg.us9.list-manage.com/subscribe/post?u=7167807451750fdd066d59761&amp;id=9428993295" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
  <p>Recibe en tu correo las historias más relevantes del mes. <small>(max. 2 emails al mes)</small></p>
  <label for="mce-EMAIL" class="sr-only">Dirección de email</label>  
  <div class="input-group">          
    <input type="email" value="" name="EMAIL" class="form-control" id="mce-EMAIL" placeholder="Tu email" required>
    <div style="position: absolute; left: -5000px;"><input type="text" name="b_7167807451750fdd066d59761_9428993295" tabindex="-1" value=""></div>
    <span class="input-group-btn">
      <button type="submit" name="op" value="Suscribirme" class="btn btn-primary form-submit">Suscribirme</button>
    </span>
  </div>
</form>',
    'format' => 'raw_html',
    'substitute' => TRUE,
  );
  $export['mailchimp_newsletter_subscribe_small'] = $content;

  return $export;
}
