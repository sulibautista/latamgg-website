<?php
/**
 * @file
 * lgg_basic_pages.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function lgg_basic_pages_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "panelizer" && $api == "panelizer") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function lgg_basic_pages_node_info() {
  $items = array(
    'page' => array(
      'name' => t('Página estática'),
      'base' => 'node_content',
      'description' => t('Usadas para el contenido estático, como un página de "Acerca de".'),
      'has_title' => '1',
      'title_label' => t('Título'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
