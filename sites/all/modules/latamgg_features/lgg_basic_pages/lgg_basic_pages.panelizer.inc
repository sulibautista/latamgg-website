<?php
/**
 * @file
 * lgg_basic_pages.panelizer.inc
 */

/**
 * Implements hook_panelizer_defaults().
 */
function lgg_basic_pages_panelizer_defaults() {
  $export = array();

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->name = 'node:page:default';
  $panelizer->title = 'Predeterminado';
  $panelizer->panelizer_type = 'node';
  $panelizer->panelizer_key = 'page';
  $panelizer->no_blocks = FALSE;
  $panelizer->css_id = '';
  $panelizer->css = '';
  $panelizer->pipeline = 'standard';
  $panelizer->contexts = array();
  $panelizer->relationships = array();
  $panelizer->access = array();
  $panelizer->view_mode = 'page_manager';
  $panelizer->css_class = '';
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = TRUE;
  $panelizer->extra = array();
  $display = new panels_display();
  $display->layout = 'bootstrap_panels_two_col';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'left' => array(
        'column' => array(
          'xs' => '0',
          'sm' => '8',
          'md' => '0',
          'lg' => '0',
        ),
        'offset' => array(
          'xs' => '0',
          'sm' => '0',
          'md' => '0',
          'lg' => '0',
        ),
        'push' => array(
          'xs' => '0',
          'sm' => '0',
          'md' => '0',
          'lg' => '0',
        ),
        'pull' => array(
          'xs' => '0',
          'sm' => '0',
          'md' => '0',
          'lg' => '0',
        ),
      ),
      'right' => array(
        'clean_markup' => array(
          'region_wrapper' => 'aside',
          'additional_region_classes' => 'col-sm-4 panel-row',
          'additional_region_attributes' => '',
          'enable_inner_div' => 1,
          'pane_separators' => 0,
        ),
      ),
      'default' => NULL,
    ),
    'left' => array(
      'style' => 'bootstrap_region',
    ),
    'right' => array(
      'style' => 'clean_element',
    ),
  );
  $display->cache = array();
  $display->title = '%node:title';
  $display->uuid = '29008427-a3d3-4241-913d-aa0db67536f8';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-d681b548-2b12-45d5-b6b4-5b7a9816f0ba';
    $pane->panel = 'left';
    $pane->type = 'node_content';
    $pane->subtype = 'node_content';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'links' => 0,
      'no_extras' => 0,
      'override_title' => 0,
      'override_title_text' => '',
      'identifier' => 'page',
      'link' => 0,
      'leave_node_title' => 1,
      'build_mode' => 'full',
      'context' => 'panelizer',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'naked',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'd681b548-2b12-45d5-b6b4-5b7a9816f0ba';
    $display->content['new-d681b548-2b12-45d5-b6b4-5b7a9816f0ba'] = $pane;
    $display->panels['left'][0] = 'new-d681b548-2b12-45d5-b6b4-5b7a9816f0ba';
    $pane = new stdClass();
    $pane->pid = 'new-f423c419-be84-4272-93b7-57416ace022d';
    $pane->panel = 'right';
    $pane->type = 'block';
    $pane->subtype = 'menu-menu-latamgg-paginas';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => 'LatamGG',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => array(
        'clean_markup' => array(
          'pane_wrapper' => 'div',
          'additional_pane_classes' => '',
          'additional_pane_attributes' => '',
          'enable_inner_div' => 0,
          'title_wrapper' => 'h2',
          'title_hide' => 0,
          'content_wrapper' => 'none',
        ),
      ),
      'style' => 'clean_element',
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'start-pane-spacing col-xxs-12 col-xs-6 col-sm-12',
    );
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'f423c419-be84-4272-93b7-57416ace022d';
    $display->content['new-f423c419-be84-4272-93b7-57416ace022d'] = $pane;
    $display->panels['right'][0] = 'new-f423c419-be84-4272-93b7-57416ace022d';
    $pane = new stdClass();
    $pane->pid = 'new-75efd5ba-bf84-42d6-b644-9b39a645e39c';
    $pane->panel = 'right';
    $pane->type = 'custom';
    $pane->subtype = 'mailchimp_newsletter_subscribe_small';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => '',
      'title' => '',
      'body' => '',
      'format' => 'full_html',
      'substitute' => TRUE,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => array(
        'clean_markup' => array(
          'pane_wrapper' => 'div',
          'additional_pane_classes' => '',
          'additional_pane_attributes' => '',
          'enable_inner_div' => 0,
          'title_wrapper' => 'h2',
          'title_hide' => 0,
          'content_wrapper' => 'none',
        ),
      ),
      'style' => 'clean_element',
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'col-xxs-12 col-xs-6 col-sm-12',
    );
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '75efd5ba-bf84-42d6-b644-9b39a645e39c';
    $display->content['new-75efd5ba-bf84-42d6-b644-9b39a645e39c'] = $pane;
    $display->panels['right'][1] = 'new-75efd5ba-bf84-42d6-b644-9b39a645e39c';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $panelizer->display = $display;
  $export['node:page:default'] = $panelizer;

  return $export;
}
