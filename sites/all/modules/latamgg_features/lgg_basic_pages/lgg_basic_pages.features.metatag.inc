<?php
/**
 * @file
 * lgg_basic_pages.features.metatag.inc
 */

/**
 * Implements hook_metatag_export_default().
 */
function lgg_basic_pages_metatag_export_default() {
  $config = array();

  // Exported Metatag config instance: node:page.
  $config['node:page'] = array(
    'instance' => 'node:page',
    'config' => array(),
  );

  return $config;
}
