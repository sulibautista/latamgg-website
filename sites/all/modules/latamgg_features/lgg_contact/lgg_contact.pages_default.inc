<?php
/**
 * @file
 * lgg_contact.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_handlers().
 */
function lgg_contact_default_page_manager_handlers() {
  $export = array();

  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'contact_site__panel_context_e3ec7677-82f9-422c-a095-1e9188aa6f05';
  $handler->task = 'contact_site';
  $handler->subtask = '';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => '',
  );
  $display = new panels_display();
  $display->layout = 'bootstrap_panels_two_col';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'left' => array(
        'column' => array(
          'xs' => '0',
          'sm' => '8',
          'md' => '0',
          'lg' => '0',
        ),
        'offset' => array(
          'xs' => '0',
          'sm' => '0',
          'md' => '0',
          'lg' => '0',
        ),
        'push' => array(
          'xs' => '0',
          'sm' => '0',
          'md' => '0',
          'lg' => '0',
        ),
        'pull' => array(
          'xs' => '0',
          'sm' => '0',
          'md' => '0',
          'lg' => '0',
        ),
      ),
      'right' => array(
        'clean_markup' => array(
          'region_wrapper' => 'aside',
          'additional_region_classes' => 'col-sm-4 panel-row',
          'additional_region_attributes' => '',
          'enable_inner_div' => 1,
          'pane_separators' => 0,
        ),
      ),
      'default' => NULL,
    ),
    'left' => array(
      'style' => 'bootstrap_region',
    ),
    'right' => array(
      'style' => 'clean_element',
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '9949eca4-a03e-4d57-a16d-e6ddb6b2cd42';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-d3ae948d-b126-45bf-a7f8-a229f25c794c';
    $pane->panel = 'left';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => '',
      'title' => 'Contacta con LatamGG',
      'body' => '<p>¿Tienes algo en mente? ¡Genial!, no dudes en ponerte en contacto con nosotros, te responderemos lo más rápido posible. Si lo prefieres, puedes enviarnos un mensaje a <a href="https://facebook.com/LatamGG">nuestra fanpage en Facebook.</a></p>',
      'format' => 'raw_html',
      'substitute' => FALSE,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => array(
        'clean_markup' => array(
          'pane_wrapper' => 'div',
          'additional_pane_classes' => '',
          'additional_pane_attributes' => '',
          'enable_inner_div' => 0,
          'title_wrapper' => 'h1',
          'title_hide' => 0,
          'content_wrapper' => 'none',
        ),
      ),
      'style' => 'clean_element',
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'grid-margin-bottom',
    );
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'd3ae948d-b126-45bf-a7f8-a229f25c794c';
    $display->content['new-d3ae948d-b126-45bf-a7f8-a229f25c794c'] = $pane;
    $display->panels['left'][0] = 'new-d3ae948d-b126-45bf-a7f8-a229f25c794c';
    $pane = new stdClass();
    $pane->pid = 'new-61ed2d7b-8059-4edc-bb73-be9e4100988a';
    $pane->panel = 'left';
    $pane->type = 'contact';
    $pane->subtype = 'contact';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '61ed2d7b-8059-4edc-bb73-be9e4100988a';
    $display->content['new-61ed2d7b-8059-4edc-bb73-be9e4100988a'] = $pane;
    $display->panels['left'][1] = 'new-61ed2d7b-8059-4edc-bb73-be9e4100988a';
    $pane = new stdClass();
    $pane->pid = 'new-fdd0050c-c6c1-401c-a543-1d42558923bb';
    $pane->panel = 'right';
    $pane->type = 'custom';
    $pane->subtype = 'become_a_fan';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => '',
      'title' => '',
      'body' => '',
      'format' => 'full_html',
      'substitute' => TRUE,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => array(
        'clean_markup' => array(
          'pane_wrapper' => 'div',
          'additional_pane_classes' => '',
          'additional_pane_attributes' => '',
          'enable_inner_div' => 0,
          'title_wrapper' => 'h2',
          'title_hide' => 0,
          'content_wrapper' => 'none',
        ),
      ),
      'style' => 'clean_element',
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'start-pane-spacing col-xxs-12',
    );
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'fdd0050c-c6c1-401c-a543-1d42558923bb';
    $display->content['new-fdd0050c-c6c1-401c-a543-1d42558923bb'] = $pane;
    $display->panels['right'][0] = 'new-fdd0050c-c6c1-401c-a543-1d42558923bb';
    $pane = new stdClass();
    $pane->pid = 'new-1cf81a30-1b4d-46b0-833d-b8c8ef37e1e6';
    $pane->panel = 'right';
    $pane->type = 'block';
    $pane->subtype = 'menu-menu-latamgg-paginas';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => 'LatamGG',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => array(
        'clean_markup' => array(
          'pane_wrapper' => 'div',
          'additional_pane_classes' => '',
          'additional_pane_attributes' => '',
          'enable_inner_div' => 0,
          'title_wrapper' => 'h2',
          'title_hide' => 0,
          'content_wrapper' => 'none',
        ),
      ),
      'style' => 'clean_element',
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'col-xxs-12 col-xs-6 col-sm-12',
    );
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '1cf81a30-1b4d-46b0-833d-b8c8ef37e1e6';
    $display->content['new-1cf81a30-1b4d-46b0-833d-b8c8ef37e1e6'] = $pane;
    $display->panels['right'][1] = 'new-1cf81a30-1b4d-46b0-833d-b8c8ef37e1e6';
    $pane = new stdClass();
    $pane->pid = 'new-4abc60a8-6402-4882-b846-a8403523ef59';
    $pane->panel = 'right';
    $pane->type = 'custom';
    $pane->subtype = 'mailchimp_newsletter_subscribe_small';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => '',
      'title' => '',
      'body' => '',
      'format' => 'full_html',
      'substitute' => TRUE,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => array(
        'clean_markup' => array(
          'pane_wrapper' => 'div',
          'additional_pane_classes' => '',
          'additional_pane_attributes' => '',
          'enable_inner_div' => 0,
          'title_wrapper' => 'h2',
          'title_hide' => 0,
          'content_wrapper' => 'none',
        ),
      ),
      'style' => 'clean_element',
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'col-xxs-12 col-xs-6 col-sm-12',
    );
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = '4abc60a8-6402-4882-b846-a8403523ef59';
    $display->content['new-4abc60a8-6402-4882-b846-a8403523ef59'] = $pane;
    $display->panels['right'][2] = 'new-4abc60a8-6402-4882-b846-a8403523ef59';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = 'new-d3ae948d-b126-45bf-a7f8-a229f25c794c';
  $handler->conf['display'] = $display;
  $export['contact_site__panel_context_e3ec7677-82f9-422c-a095-1e9188aa6f05'] = $handler;

  return $export;
}
