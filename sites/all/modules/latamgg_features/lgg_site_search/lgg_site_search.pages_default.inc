<?php
/**
 * @file
 * lgg_site_search.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function lgg_site_search_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'site_search';
  $page->task = 'page';
  $page->admin_title = 'Búsqueda del Sitio';
  $page->admin_description = 'NOTE: Metatags are activated (and with a padding space to right) so it "fixes" a (possibly) panel bug that renders the <title> element unusable, somehow caching the page title forever. ';
  $page->path = 'buscar';
  $page->access = array();
  $page->menu = array();
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_site_search_panel_context';
  $handler->task = 'page';
  $handler->subtask = 'site_search';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => 'node-listing',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'metatag_panels' => array(
      'enabled' => 1,
      'metatags' => array(
        'title' => array(
          'value' => '[current-page:title] | [site:name] ',
        ),
      ),
    ),
  );
  $display = new panels_display();
  $display->layout = 'bootstrap_twocol_stacked';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'left' => array(
        'column' => array(
          'xs' => '0',
          'sm' => '8',
          'md' => '0',
          'lg' => '0',
        ),
        'offset' => array(
          'xs' => '0',
          'sm' => '0',
          'md' => '0',
          'lg' => '0',
        ),
        'push' => array(
          'xs' => '0',
          'sm' => '0',
          'md' => '0',
          'lg' => '0',
        ),
        'pull' => array(
          'xs' => '0',
          'sm' => '0',
          'md' => '0',
          'lg' => '0',
        ),
      ),
      'right' => array(
        'column' => array(
          'xs' => '0',
          'sm' => '4',
          'md' => '0',
          'lg' => '0',
        ),
        'offset' => array(
          'xs' => '0',
          'sm' => '0',
          'md' => '0',
          'lg' => '0',
        ),
        'push' => array(
          'xs' => '0',
          'sm' => '0',
          'md' => '0',
          'lg' => '0',
        ),
        'pull' => array(
          'xs' => '0',
          'sm' => '0',
          'md' => '0',
          'lg' => '0',
        ),
      ),
      'bottom' => NULL,
      'top' => NULL,
    ),
    'left' => array(
      'style' => 'bootstrap_region',
    ),
    'right' => array(
      'style' => 'bootstrap_region',
    ),
    'top' => array(
      'style' => 'naked',
    ),
  );
  $display->cache = array();
  $display->title = 'Yolo';
  $display->uuid = '471fba9d-ed1a-410f-9344-1d6ad6e79148';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-9206adfd-542f-48e7-8ffa-5127a06e17c1';
    $pane->panel = 'left';
    $pane->type = 'views_panes';
    $pane->subtype = 'site_search-panel_pane_1';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'path' => '',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => array(
        'clean_markup' => array(
          'pane_wrapper' => 'div',
          'additional_pane_classes' => 'panel-pane',
          'additional_pane_attributes' => '',
          'enable_inner_div' => 0,
          'title_wrapper' => 'h1',
          'title_hide' => 0,
          'content_wrapper' => 'none',
        ),
      ),
      'style' => 'clean_element',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '9206adfd-542f-48e7-8ffa-5127a06e17c1';
    $display->content['new-9206adfd-542f-48e7-8ffa-5127a06e17c1'] = $pane;
    $display->panels['left'][0] = 'new-9206adfd-542f-48e7-8ffa-5127a06e17c1';
    $pane = new stdClass();
    $pane->pid = 'new-b90f4eff-a4ac-4b7e-a7db-d807ceaac7a8';
    $pane->panel = 'right';
    $pane->type = 'views_panes';
    $pane->subtype = 'popular_articles-panel_pane_1';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'items_per_page' => '8',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'b90f4eff-a4ac-4b7e-a7db-d807ceaac7a8';
    $display->content['new-b90f4eff-a4ac-4b7e-a7db-d807ceaac7a8'] = $pane;
    $display->panels['right'][0] = 'new-b90f4eff-a4ac-4b7e-a7db-d807ceaac7a8';
  $display->hide_title = PANELS_TITLE_PANE;
  $display->title_pane = 'new-9206adfd-542f-48e7-8ffa-5127a06e17c1';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['site_search'] = $page;

  return $pages;

}
