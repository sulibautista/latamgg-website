<?php
/**
 * @file
 * lgg_site_search.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function lgg_site_search_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function lgg_site_search_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_default_search_api_autocomplete_search().
 */
function lgg_site_search_default_search_api_autocomplete_search() {
  $items = array();
  $items['latamgg_search_inline'] = entity_import('search_api_autocomplete_search', '{
    "machine_name" : "latamgg_search_inline",
    "name" : "B\\u00fasqueda r\\u00e1pida del Sitio",
    "index_id" : "default_node_index",
    "type" : "latamgg",
    "enabled" : "1",
    "options" : {
      "result count" : true,
      "fields" : [ "search_api_aggregation_1" ],
      "results" : 0,
      "min_length" : "2",
      "submit_button_selector" : ":submit"
    }
  }');
  $items['search_api_views_site_search'] = entity_import('search_api_autocomplete_search', '{
    "machine_name" : "search_api_views_site_search",
    "name" : "B\\u00fasqueda del Sitio",
    "index_id" : "default_node_index",
    "type" : "search_api_views",
    "enabled" : "1",
    "options" : {
      "result count" : true,
      "fields" : [ "search_api_aggregation_1" ],
      "results" : 1,
      "min_length" : "1",
      "submit_button_selector" : ":submit",
      "custom" : { "display" : "default" }
    }
  }');
  return $items;
}

/**
 * Implements hook_default_search_api_index().
 */
function lgg_site_search_default_search_api_index() {
  $items = array();
  $items['default_node_index'] = entity_import('search_api_index', '{
    "name" : "Node Index",
    "machine_name" : "default_node_index",
    "description" : "An automatically created search index for indexing node data. Might be configured to specific needs. ",
    "server" : "solr_1",
    "item_type" : "node",
    "options" : {
      "index_directly" : 1,
      "cron_limit" : "50",
      "data_alter_callbacks" : {
        "search_api_alter_bundle_filter" : {
          "status" : 0,
          "weight" : "-50",
          "settings" : { "default" : "1", "bundles" : [] }
        },
        "search_api_alter_node_status" : { "status" : 1, "weight" : "-49", "settings" : [] },
        "search_api_alter_node_access" : { "status" : 1, "weight" : "-48", "settings" : [] },
        "search_api_alter_add_hierarchy" : { "status" : 0, "weight" : "-47", "settings" : { "fields" : [] } },
        "search_api_alter_add_viewed_entity" : { "status" : 0, "weight" : "-46", "settings" : { "mode" : "full" } },
        "search_api_alter_add_url" : { "status" : 0, "weight" : "-45", "settings" : [] },
        "search_api_alter_add_aggregation" : {
          "status" : 1,
          "weight" : "-44",
          "settings" : { "fields" : { "search_api_aggregation_1" : {
                "name" : "Search API autocomplete field",
                "type" : "fulltext",
                "fields" : [
                  "type",
                  "title",
                  "author",
                  "body:value",
                  "body:summary",
                  "field_tags:name",
                  "field_games:name",
                  "field_categories:name",
                  "field_rel_champions:field_loldata_title",
                  "field_rel_champions:field_loldata_champion_title"
                ],
                "description" : "A Texto completo aggregation of the following fields: Tipo de contenido, T\\u00edtulo, Autor, The main body text \\u00bb Texto, The main body text \\u00bb Resumen, Etiquetas \\u00bb Nombre, Juegos \\u00bb Nombre, Categor\\u00edas \\u00bb Nombre, Campeones relacionados \\u00bb T\\u00edtulo, Campeones relacionados \\u00bb field_loldata_champion_title."
              }
            }
          }
        }
      },
      "processors" : {
        "search_api_case_ignore" : {
          "status" : 0,
          "weight" : "0",
          "settings" : { "fields" : { "title" : true, "body:value" : true } }
        },
        "search_api_html_filter" : {
          "status" : 1,
          "weight" : "10",
          "settings" : {
            "fields" : {
              "title" : true,
              "search_api_aggregation_1" : true,
              "body:value" : true,
              "body:summary" : true
            },
            "title" : 1,
            "alt" : 1,
            "tags" : "h1 = 5\\r\\nh2 = 3\\r\\nh3 = 2\\r\\nstrong = 2\\r\\nb = 2\\r\\nem = 1.5\\r\\nu = 1.5"
          }
        },
        "search_api_transliteration" : {
          "status" : 0,
          "weight" : "15",
          "settings" : { "fields" : { "title" : true, "body:value" : true } }
        },
        "search_api_tokenizer" : {
          "status" : 0,
          "weight" : "20",
          "settings" : {
            "fields" : { "title" : true, "body:value" : true },
            "spaces" : "[^\\\\p{L}\\\\p{N}]",
            "ignorable" : "[-]"
          }
        },
        "search_api_stopwords" : {
          "status" : 0,
          "weight" : "30",
          "settings" : {
            "fields" : { "title" : true, "body:value" : true },
            "file" : "",
            "stopwords" : "but\\r\\ndid\\r\\nthe this that those\\r\\netc"
          }
        },
        "search_api_highlighting" : {
          "status" : 0,
          "weight" : "35",
          "settings" : {
            "prefix" : "\\u003Cstrong\\u003E",
            "suffix" : "\\u003C\\/strong\\u003E",
            "excerpt" : 1,
            "excerpt_length" : "256",
            "exclude_fields" : [],
            "highlight" : "always"
          }
        }
      },
      "fields" : {
        "author" : { "type" : "integer", "entity_type" : "user" },
        "body:summary" : { "type" : "text" },
        "body:value" : { "type" : "text" },
        "changed" : { "type" : "date" },
        "created" : { "type" : "date" },
        "field_categories:name" : { "type" : "list\\u003Ctext\\u003E" },
        "field_categories:tid" : { "type" : "list\\u003Cinteger\\u003E" },
        "field_games:name" : { "type" : "list\\u003Ctext\\u003E" },
        "field_games:tid" : { "type" : "list\\u003Cinteger\\u003E" },
        "field_rel_champions:field_loldata_champion_name" : { "type" : "list\\u003Ctext\\u003E" },
        "field_rel_champions:field_loldata_champion_tag" : { "type" : "list\\u003Ctext\\u003E", "boost" : "0.5" },
        "field_rel_champions:field_loldata_champion_title" : { "type" : "list\\u003Ctext\\u003E" },
        "field_rel_champions:field_loldata_title" : { "type" : "list\\u003Ctext\\u003E" },
        "field_rel_champions:loldata_id" : { "type" : "list\\u003Cinteger\\u003E" },
        "field_tags:name" : { "type" : "list\\u003Ctext\\u003E" },
        "field_tags:tid" : { "type" : "list\\u003Cinteger\\u003E" },
        "nid" : { "type" : "integer" },
        "promote" : { "type" : "boolean" },
        "search_api_access_node" : { "type" : "list\\u003Cstring\\u003E" },
        "search_api_aggregation_1" : { "type" : "text" },
        "search_api_language" : { "type" : "string" },
        "status" : { "type" : "integer" },
        "sticky" : { "type" : "boolean" },
        "title" : { "type" : "text", "boost" : "5.0" },
        "type" : { "type" : "string" }
      }
    },
    "enabled" : "1",
    "read_only" : "0"
  }');
  return $items;
}
