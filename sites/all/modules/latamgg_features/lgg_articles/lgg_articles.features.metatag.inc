<?php
/**
 * @file
 * lgg_articles.features.metatag.inc
 */

/**
 * Implements hook_metatag_export_default().
 */
function lgg_articles_metatag_export_default() {
  $config = array();

  // Exported Metatag config instance: node:article.
  $config['node:article'] = array(
    'instance' => 'node:article',
    'config' => array(
      'og:image' => array(
        'value' => '[node:field-highlighted-image:scald_thumbnail],[node:field-image:scald_thumbnail]',
      ),
      'article:section' => array(
        'value' => '[node:field-games]',
      ),
      'article:tag' => array(
        'value' => '[node:field-tags]',
      ),
    ),
  );

  return $config;
}
