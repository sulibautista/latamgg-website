<?php
/**
 * @file
 * lgg_articles.panels_default.inc
 */

/**
 * Implements hook_default_panels_mini().
 */
function lgg_articles_default_panels_mini() {
  $export = array();

  $mini = new stdClass();
  $mini->disabled = FALSE; /* Edit this to true to make a default mini disabled initially */
  $mini->api_version = 1;
  $mini->name = 'highlighted_article';
  $mini->category = 'LatamGG';
  $mini->admin_title = 'Articulo destacado';
  $mini->admin_description = '';
  $mini->requiredcontexts = array(
    0 => array(
      'identifier' => 'Contenido',
      'keyword' => 'node',
      'name' => 'entity:node',
      'entity_id' => '',
      'id' => 1,
    ),
  );
  $mini->contexts = array();
  $mini->relationships = array();
  $display = new panels_display();
  $display->layout = 'bootstrap_panels_two_col';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'top' => NULL,
      'left' => array(
        'column' => array(
          'xs' => '0',
          'sm' => '6',
          'md' => '4',
          'lg' => '0',
        ),
        'offset' => array(
          'xs' => '0',
          'sm' => '0',
          'md' => '0',
          'lg' => '0',
        ),
        'push' => array(
          'xs' => '0',
          'sm' => '6',
          'md' => '8',
          'lg' => '0',
        ),
        'pull' => array(
          'xs' => '0',
          'sm' => '0',
          'md' => '0',
          'lg' => '0',
        ),
      ),
      'right' => array(
        'column' => array(
          'xs' => '0',
          'sm' => '6',
          'md' => '8',
          'lg' => '0',
        ),
        'offset' => array(
          'xs' => '0',
          'sm' => '0',
          'md' => '0',
          'lg' => '0',
        ),
        'push' => array(
          'xs' => '0',
          'sm' => '0',
          'md' => '0',
          'lg' => '0',
        ),
        'pull' => array(
          'xs' => '0',
          'sm' => '6',
          'md' => '4',
          'lg' => '0',
        ),
      ),
      'bottom' => NULL,
      'middle' => NULL,
    ),
    'left' => array(
      'style' => 'bootstrap_region',
    ),
    'right' => array(
      'style' => 'bootstrap_region',
    ),
    'middle' => array(
      'style' => 'naked',
    ),
    'style' => 'naked',
  );
  $display->cache = array(
    'method' => '0',
    'settings' => array(),
  );
  $display->title = '';
  $display->uuid = 'c054cfab-4472-40d8-93b5-f0158bb56ad2';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-ebed4d15-424d-445f-98f9-89ff8420cecf';
    $pane->panel = 'left';
    $pane->type = 'panels_mini';
    $pane->subtype = 'highlighted_article_info';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'context' => array(
        0 => 'requiredcontext_entity:node_1',
      ),
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'naked',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'ebed4d15-424d-445f-98f9-89ff8420cecf';
    $display->content['new-ebed4d15-424d-445f-98f9-89ff8420cecf'] = $pane;
    $display->panels['left'][0] = 'new-ebed4d15-424d-445f-98f9-89ff8420cecf';
    $pane = new stdClass();
    $pane->pid = 'new-018e73ed-18ac-4dfe-9f11-13f37e01ed13';
    $pane->panel = 'right';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_highlighted_image';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'imagen_superdestacada',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(
        'link' => '1',
      ),
      'context' => 'requiredcontext_entity:node_1',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'naked',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '018e73ed-18ac-4dfe-9f11-13f37e01ed13';
    $display->content['new-018e73ed-18ac-4dfe-9f11-13f37e01ed13'] = $pane;
    $display->panels['right'][0] = 'new-018e73ed-18ac-4dfe-9f11-13f37e01ed13';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = '0';
  $mini->display = $display;
  $export['highlighted_article'] = $mini;

  $mini = new stdClass();
  $mini->disabled = FALSE; /* Edit this to true to make a default mini disabled initially */
  $mini->api_version = 1;
  $mini->name = 'highlighted_article_info';
  $mini->category = 'LatamGG';
  $mini->admin_title = 'Info del Articulo destacado';
  $mini->admin_description = '';
  $mini->requiredcontexts = array(
    0 => array(
      'identifier' => 'Contenido',
      'keyword' => 'node',
      'name' => 'entity:node',
      'entity_id' => '',
      'id' => 1,
    ),
  );
  $mini->contexts = array();
  $mini->relationships = array();
  $display = new panels_display();
  $display->layout = 'onecol_reset_clean';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => array(
        'clean_markup' => array(
          'region_wrapper' => 'article',
          'additional_region_classes' => '',
          'additional_region_attributes' => '',
          'enable_inner_div' => 0,
          'pane_separators' => 0,
        ),
      ),
      'middle' => array(
        'clean_markup' => array(
          'region_wrapper' => 'div',
          'additional_region_classes' => 'highlighted-info',
          'additional_region_attributes' => '',
          'enable_inner_div' => 0,
          'pane_separators' => 0,
        ),
      ),
    ),
    'style' => 'clean_element',
    'middle' => array(
      'style' => 'clean_element',
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '34d9d5ac-db20-452e-b935-c7d927c217d0';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-aa30dec1-466a-43b7-809c-a2269a62340e';
    $pane->panel = 'middle';
    $pane->type = 'node_title';
    $pane->subtype = 'node_title';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'link' => 1,
      'markup' => 'div',
      'id' => '',
      'class' => 'highlighted-title',
      'context' => 'requiredcontext_entity:node_1',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'naked',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'aa30dec1-466a-43b7-809c-a2269a62340e';
    $display->content['new-aa30dec1-466a-43b7-809c-a2269a62340e'] = $pane;
    $display->panels['middle'][0] = 'new-aa30dec1-466a-43b7-809c-a2269a62340e';
    $pane = new stdClass();
    $pane->pid = 'new-7f37befd-9be7-4034-a339-13b4b871ccf7';
    $pane->panel = 'middle';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:body';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'text_summary_or_trimmed',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(
        'trim_length' => '80',
      ),
      'context' => 'requiredcontext_entity:node_1',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'naked',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '7f37befd-9be7-4034-a339-13b4b871ccf7';
    $display->content['new-7f37befd-9be7-4034-a339-13b4b871ccf7'] = $pane;
    $display->panels['middle'][1] = 'new-7f37befd-9be7-4034-a339-13b4b871ccf7';
    $pane = new stdClass();
    $pane->pid = 'new-225ed1ac-3c36-4819-a36f-c6f952bacd02';
    $pane->panel = 'middle';
    $pane->type = 'views_panes';
    $pane->subtype = 'related_nodes-panel_pane_1';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'items_per_page' => '3',
      'context' => array(
        0 => 'requiredcontext_entity:node_1',
      ),
      'override_title' => 1,
      'override_title_text' => 'Además...',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => array(
        'clean_markup' => array(
          'pane_wrapper' => 'aside',
          'additional_pane_classes' => 'see-also',
          'additional_pane_attributes' => '',
          'enable_inner_div' => 0,
          'title_wrapper' => 'div',
          'title_hide' => 0,
          'content_wrapper' => 'none',
        ),
      ),
      'style' => 'clean_element',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = '225ed1ac-3c36-4819-a36f-c6f952bacd02';
    $display->content['new-225ed1ac-3c36-4819-a36f-c6f952bacd02'] = $pane;
    $display->panels['middle'][2] = 'new-225ed1ac-3c36-4819-a36f-c6f952bacd02';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = 'new-aa30dec1-466a-43b7-809c-a2269a62340e';
  $mini->display = $display;
  $export['highlighted_article_info'] = $mini;

  $mini = new stdClass();
  $mini->disabled = FALSE; /* Edit this to true to make a default mini disabled initially */
  $mini->api_version = 1;
  $mini->name = 'node_footer';
  $mini->category = 'LatamGG';
  $mini->admin_title = 'Pie del Nodo';
  $mini->admin_description = 'Muestra los comentarios, artículos relacionados y artículos populares debajo del nodo.';
  $mini->requiredcontexts = array(
    0 => array(
      'identifier' => 'Contenido',
      'keyword' => 'node',
      'name' => 'entity:node',
      'entity_id' => '',
      'id' => 1,
    ),
  );
  $mini->contexts = array();
  $mini->relationships = array();
  $display = new panels_display();
  $display->layout = 'bs_two_col_sandwich';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'top' => NULL,
      'left' => array(
        'clean_markup' => array(
          'region_wrapper' => 'section',
          'additional_region_classes' => 'col-xxs-12 col-sm-8 col-lg-9 comments-section',
          'additional_region_attributes' => '',
          'enable_inner_div' => 0,
          'pane_separators' => 0,
        ),
      ),
      'right' => array(
        'clean_markup' => array(
          'region_wrapper' => 'aside',
          'additional_region_classes' => 'col-xxs-12 col-xs-6 col-sm-4 col-lg-3 maybe-stick-parent',
          'additional_region_attributes' => '',
          'enable_inner_div' => 1,
          'pane_separators' => 0,
        ),
      ),
      'bottom' => NULL,
    ),
    'style' => 'naked',
    'left' => array(
      'style' => 'clean_element',
    ),
    'right' => array(
      'style' => 'clean_element',
    ),
    'top' => array(
      'style' => 'naked',
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '76b2cb68-03eb-44bc-84b8-611852b71584';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-b42f88b1-707c-4944-bb37-9dd73493b4b1';
    $pane->panel = 'left';
    $pane->type = 'custom';
    $pane->subtype = 'fb_comments';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => '',
      'title' => '',
      'body' => '',
      'format' => 'full_html',
      'substitute' => TRUE,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => array(
        'clean_markup' => array(
          'pane_wrapper' => 'div',
          'additional_pane_classes' => 'panel-pane',
          'additional_pane_attributes' => '',
          'enable_inner_div' => 0,
          'title_wrapper' => 'h2',
          'title_hide' => 0,
          'content_wrapper' => 'none',
        ),
      ),
      'style' => 'clean_element',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'b42f88b1-707c-4944-bb37-9dd73493b4b1';
    $display->content['new-b42f88b1-707c-4944-bb37-9dd73493b4b1'] = $pane;
    $display->panels['left'][0] = 'new-b42f88b1-707c-4944-bb37-9dd73493b4b1';
    $pane = new stdClass();
    $pane->pid = 'new-9706ccc8-4e44-4879-8097-a10815b5f44e';
    $pane->panel = 'right';
    $pane->type = 'views_panes';
    $pane->subtype = 'popular_articles-panel_pane_1';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => 'Lo más popular',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => array(
        'clean_markup' => array(
          'pane_wrapper' => 'div',
          'additional_pane_classes' => 'panel-pane',
          'additional_pane_attributes' => '',
          'enable_inner_div' => 0,
          'title_wrapper' => 'h2',
          'title_hide' => 0,
          'content_wrapper' => 'none',
        ),
      ),
      'style' => 'clean_element',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '9706ccc8-4e44-4879-8097-a10815b5f44e';
    $display->content['new-9706ccc8-4e44-4879-8097-a10815b5f44e'] = $pane;
    $display->panels['right'][0] = 'new-9706ccc8-4e44-4879-8097-a10815b5f44e';
    $pane = new stdClass();
    $pane->pid = 'new-91ab2adc-1ac7-49ec-baf7-85e929737fb7';
    $pane->panel = 'top';
    $pane->type = 'views_panes';
    $pane->subtype = 'related_articles-panel_pane_1';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'context' => array(
        0 => 'requiredcontext_entity:node_1',
      ),
      'override_title' => 1,
      'override_title_text' => 'Te recomendamos',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => array(
        'clean_markup' => array(
          'pane_wrapper' => 'aside',
          'additional_pane_classes' => 'panel-pane',
          'additional_pane_attributes' => '',
          'enable_inner_div' => 0,
          'title_wrapper' => 'h2',
          'title_hide' => 0,
          'content_wrapper' => 'none',
        ),
      ),
      'style' => 'clean_element',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '91ab2adc-1ac7-49ec-baf7-85e929737fb7';
    $display->content['new-91ab2adc-1ac7-49ec-baf7-85e929737fb7'] = $pane;
    $display->panels['top'][0] = 'new-91ab2adc-1ac7-49ec-baf7-85e929737fb7';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = 'new-b42f88b1-707c-4944-bb37-9dd73493b4b1';
  $mini->display = $display;
  $export['node_footer'] = $mini;

  return $export;
}
