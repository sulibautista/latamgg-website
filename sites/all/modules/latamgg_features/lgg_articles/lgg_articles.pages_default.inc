<?php
/**
 * @file
 * lgg_articles.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function lgg_articles_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'article_listing';
  $page->task = 'page';
  $page->admin_title = 'Noticias';
  $page->admin_description = 'Usa %taxonomy_term en lugar de "%game" para activar los comodines de términos en el apartado de Metatags. ';
  $page->path = 'gamearticles/%taxonomy_term/noticias';
  $page->access = array();
  $page->menu = array();
  $page->arguments = array(
    'taxonomy_term' => array(
      'id' => 1,
      'identifier' => 'Término de taxonomía: ID',
      'name' => 'term',
      'settings' => array(
        'input_form' => 'tid',
        'vids' => array(
          4 => '4',
          5 => 0,
          1 => 0,
          6 => 0,
          2 => 0,
          3 => 0,
        ),
        'breadcrumb' => 1,
        'transform' => 0,
      ),
    ),
  );
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_article_listing_panel_context';
  $handler->task = 'page';
  $handler->subtask = 'article_listing';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'pathalias' => '%game:name/noticias',
    'metatag_panels' => array(
      'enabled' => 1,
      'metatags' => array(
        'title' => array(
          'value' => '[current-page:title] | [term:name] | [site:name]',
        ),
      ),
    ),
    'panels_breadcrumbs_state' => 1,
    'panels_breadcrumbs_titles' => '%taxonomy_term:name',
    'panels_breadcrumbs_paths' => '',
    'panels_breadcrumbs_home' => 1,
  );
  $display = new panels_display();
  $display->layout = 'onecol';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'middle' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Noticias';
  $display->uuid = '03e2bada-9c2a-498a-bf66-4448bc8b6dcd';
  $display->content = array();
  $display->panels = array();
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['article_listing'] = $page;

  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'news';
  $page->task = 'page';
  $page->admin_title = 'Noticias';
  $page->admin_description = '';
  $page->path = 'noticias';
  $page->access = array();
  $page->menu = array(
    'type' => 'normal',
    'title' => 'Noticias',
    'name' => 'main-menu',
    'weight' => '0',
    'parent' => array(
      'type' => 'none',
      'title' => '',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_news_panel_context_2';
  $handler->task = 'page';
  $handler->subtask = 'news';
  $handler->handler = 'panel_context';
  $handler->weight = 1;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => 'node-listing',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(
      0 => array(
        'identifier' => 'Vista: Primer Sticky del Juego/Categoria',
        'keyword' => 'view',
        'name' => 'view_from_argument:first_sticky_by_game_category-ctools_context_1',
        'context' => array(
          0 => 'empty',
          1 => 'empty',
        ),
        'id' => 1,
      ),
      1 => array(
        'identifier' => 'Nodo de la vista',
        'keyword' => 'first_sticky_node',
        'name' => 'node_from_view',
        'row' => '1',
        'context' => 'relationship_view_from_argument:first_sticky_by_game_category-ctools_context_1_1',
        'id' => 1,
      ),
    ),
    'metatag_panels' => array(
      'enabled' => 0,
      'metatags' => array(
        'title' => array(
          'value' => 'Noticias | [site:name]',
        ),
      ),
    ),
    'pathalias' => '%taxonomy_term:name/%category:name',
    'panels_breadcrumbs_state' => 1,
    'panels_breadcrumbs_titles' => '%taxonomy_term:name',
    'panels_breadcrumbs_paths' => '',
    'panels_breadcrumbs_home' => 1,
    'access' => array(
      'plugins' => array(),
      'logic' => 'and',
    ),
  );
  $display = new panels_display();
  $display->layout = 'bs_two_col_sandwich';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'middle' => NULL,
      'left' => array(
        'column' => array(
          'xs' => '0',
          'sm' => '8',
          'md' => '0',
          'lg' => '0',
        ),
        'offset' => array(
          'xs' => '0',
          'sm' => '0',
          'md' => '0',
          'lg' => '0',
        ),
        'push' => array(
          'xs' => '0',
          'sm' => '0',
          'md' => '0',
          'lg' => '0',
        ),
        'pull' => array(
          'xs' => '0',
          'sm' => '0',
          'md' => '0',
          'lg' => '0',
        ),
      ),
      'right' => array(
        'column' => array(
          'xs' => '0',
          'sm' => '4',
          'md' => '0',
          'lg' => '0',
        ),
        'offset' => array(
          'xs' => '0',
          'sm' => '0',
          'md' => '0',
          'lg' => '0',
        ),
        'push' => array(
          'xs' => '0',
          'sm' => '0',
          'md' => '0',
          'lg' => '0',
        ),
        'pull' => array(
          'xs' => '0',
          'sm' => '0',
          'md' => '0',
          'lg' => '0',
        ),
      ),
      'bottom' => NULL,
      'default' => NULL,
      'top' => NULL,
    ),
    'top' => array(
      'style' => 'naked',
    ),
    'style' => 'naked',
    'left' => array(
      'style' => 'bootstrap_region',
    ),
    'right' => array(
      'style' => 'bootstrap_region',
    ),
  );
  $display->cache = array(
    'method' => '0',
    'settings' => array(),
  );
  $display->title = '';
  $display->uuid = 'b89d5c61-de7b-4b5b-bd9a-ff3dd4c994bc';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-b1192dae-49b1-4f8e-a13a-42172d6c6caf';
    $pane->panel = 'left';
    $pane->type = 'block';
    $pane->subtype = 'menu-menu-article-categories';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => 'Últimas Noticias',
    );
    $pane->cache = array();
    $pane->style = array(
      'style' => 'default',
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'b1192dae-49b1-4f8e-a13a-42172d6c6caf';
    $display->content['new-b1192dae-49b1-4f8e-a13a-42172d6c6caf'] = $pane;
    $display->panels['left'][0] = 'new-b1192dae-49b1-4f8e-a13a-42172d6c6caf';
    $pane = new stdClass();
    $pane->pid = 'new-a445ee0d-0be2-4582-8a26-361b4522c847';
    $pane->panel = 'left';
    $pane->type = 'views_panes';
    $pane->subtype = 'game_category_articles-panel_pane_1';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'use_pager' => 1,
      'pager_id' => '0',
      'items_per_page' => '3',
      'context' => array(
        0 => 'empty',
        1 => 'empty',
      ),
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'naked',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = 'a445ee0d-0be2-4582-8a26-361b4522c847';
    $display->content['new-a445ee0d-0be2-4582-8a26-361b4522c847'] = $pane;
    $display->panels['left'][1] = 'new-a445ee0d-0be2-4582-8a26-361b4522c847';
    $pane = new stdClass();
    $pane->pid = 'new-fd9580fc-f6c1-4360-89f8-15e55c65847b';
    $pane->panel = 'right';
    $pane->type = 'custom';
    $pane->subtype = 'become_a_fan';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => '',
      'title' => '',
      'body' => '',
      'format' => 'full_html',
      'substitute' => TRUE,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => array(
        'clean_markup' => array(
          'pane_wrapper' => 'div',
          'additional_pane_classes' => '',
          'additional_pane_attributes' => '',
          'enable_inner_div' => 0,
          'title_wrapper' => 'h2',
          'title_hide' => 0,
          'content_wrapper' => 'none',
        ),
      ),
      'style' => 'clean_element',
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'start-pane-spacing',
    );
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'fd9580fc-f6c1-4360-89f8-15e55c65847b';
    $display->content['new-fd9580fc-f6c1-4360-89f8-15e55c65847b'] = $pane;
    $display->panels['right'][0] = 'new-fd9580fc-f6c1-4360-89f8-15e55c65847b';
    $pane = new stdClass();
    $pane->pid = 'new-c1afecd1-3710-47fd-897e-f642d3008ad4';
    $pane->panel = 'right';
    $pane->type = 'views_panes';
    $pane->subtype = 'popular_articles-panel_pane_1';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => 'Lo mejor',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => array(
        'clean_markup' => array(
          'pane_wrapper' => 'div',
          'additional_pane_classes' => '',
          'additional_pane_attributes' => '',
          'enable_inner_div' => 0,
          'title_wrapper' => 'h2',
          'title_hide' => 0,
          'content_wrapper' => 'none',
        ),
      ),
      'style' => 'clean_element',
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => '',
    );
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = 'c1afecd1-3710-47fd-897e-f642d3008ad4';
    $display->content['new-c1afecd1-3710-47fd-897e-f642d3008ad4'] = $pane;
    $display->panels['right'][1] = 'new-c1afecd1-3710-47fd-897e-f642d3008ad4';
    $pane = new stdClass();
    $pane->pid = 'new-e08dccbc-4f3c-4e04-9e84-adff39cdca1a';
    $pane->panel = 'top';
    $pane->type = 'panels_mini';
    $pane->subtype = 'highlighted_article';
    $pane->shown = TRUE;
    $pane->access = array(
      'plugins' => array(
        0 => array(
          'name' => 'context_exists',
          'settings' => array(
            'exists' => '1',
          ),
          'context' => 'relationship_node_from_view_1',
          'not' => FALSE,
        ),
      ),
    );
    $pane->configuration = array(
      'context' => array(
        0 => 'relationship_node_from_view_1',
      ),
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array(
      'method' => 0,
    );
    $pane->style = array(
      'style' => 'naked',
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => '',
    );
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'e08dccbc-4f3c-4e04-9e84-adff39cdca1a';
    $display->content['new-e08dccbc-4f3c-4e04-9e84-adff39cdca1a'] = $pane;
    $display->panels['top'][0] = 'new-e08dccbc-4f3c-4e04-9e84-adff39cdca1a';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['news'] = $page;

  return $pages;

}
