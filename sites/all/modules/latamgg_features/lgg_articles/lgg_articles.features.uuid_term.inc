<?php
/**
 * @file
 * lgg_articles.features.uuid_term.inc
 */

/**
 * Implements hook_uuid_features_default_terms().
 */
function lgg_articles_uuid_features_default_terms() {
  $terms = array();

  $terms[] = array(
    'name' => 'Entretenimiento',
    'description' => '',
    'format' => 'full_html',
    'weight' => 0,
    'uuid' => '153c38bb-498a-406d-b043-ecdcce234c1b',
    'vocabulary_machine_name' => 'categories',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'eSports',
    'description' => '',
    'format' => 'full_html',
    'weight' => 0,
    'uuid' => '2862b31e-287e-454a-b065-57910978bb0d',
    'vocabulary_machine_name' => 'categories',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'League of Legends',
    'description' => '<p>Guías,&nbsp;Torneos,&nbsp;Noticias, Consejos y Artículos relacionados con el mundo de League of Legends en Español.</p>
',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '367606b9-5240-423f-bf3e-11fb088b1473',
    'vocabulary_machine_name' => 'juegos',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'PBE',
    'description' => '',
    'format' => 'full_html',
    'weight' => 0,
    'uuid' => '402d9886-04a0-4116-8360-06d8d7731f7a',
    'vocabulary_machine_name' => 'tags',
    'field_tags_games' => array(
      'und' => array(
        0 => array(
          'tid' => '367606b9-5240-423f-bf3e-11fb088b1473',
        ),
      ),
    ),
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'Eventos',
    'description' => '',
    'format' => 'full_html',
    'weight' => 0,
    'uuid' => '4c8e930d-ad74-45e7-ac14-acb70f648870',
    'vocabulary_machine_name' => 'categories',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'Estrategia',
    'description' => '',
    'format' => 'full_html',
    'weight' => 0,
    'uuid' => '6febca72-af75-48f6-8dea-66078857d04d',
    'vocabulary_machine_name' => 'categories',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'Torneos',
    'description' => '',
    'format' => 'full_html',
    'weight' => 0,
    'uuid' => '81de19be-08ab-4520-98ae-7c8ef8dcaca7',
    'vocabulary_machine_name' => 'categories',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'Actualizaciones',
    'description' => '',
    'format' => 'full_html',
    'weight' => 0,
    'uuid' => 'bb164292-05eb-43a2-a681-8cabf1c4ea25',
    'vocabulary_machine_name' => 'categories',
    'metatags' => array(),
  );
  return $terms;
}
