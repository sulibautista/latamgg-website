<?php
/**
 * @file
 * lgg_articles.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function lgg_articles_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-article-categories_todo:noticias
  $menu_links['menu-article-categories_todo:noticias'] = array(
    'menu_name' => 'menu-article-categories',
    'link_path' => 'noticias',
    'router_path' => 'noticias',
    'link_title' => 'Todo',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-article-categories_todo:noticias',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Todo');


  return $menu_links;
}
