<?php
/**
 * @file
 * lgg_articles.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function lgg_articles_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_highlighted_fields|node|article|form';
  $field_group->group_name = 'group_highlighted_fields';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'article';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Detalles del articulo destacado',
    'weight' => '11',
    'children' => array(
      0 => 'field_highlighted_text',
      1 => 'field_highlighted_image',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_highlighted_fields|node|article|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_league_fields|node|article|form';
  $field_group->group_name = 'group_league_fields';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'article';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'League of Legends',
    'weight' => '10',
    'children' => array(
      0 => 'field_rel_champions',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-league-fields field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_league_fields|node|article|form'] = $field_group;

  return $export;
}
