<?php
/**
 * @file
 * lgg_articles.default_breakpoint_group.inc
 */

/**
 * Implements hook_default_breakpoint_group().
 */
function lgg_articles_default_breakpoint_group() {
  $export = array();

  $breakpoint_group = new stdClass();
  $breakpoint_group->disabled = FALSE; /* Edit this to true to make a default breakpoint_group disabled initially */
  $breakpoint_group->api_version = 1;
  $breakpoint_group->machine_name = 'latamggv1_no_lg';
  $breakpoint_group->name = 'latamggv1 no lg';
  $breakpoint_group->breakpoints = array(
    0 => 'breakpoints.theme.latamggv1.screen-md',
    1 => 'breakpoints.theme.latamggv1.screen-sm',
    2 => 'breakpoints.theme.latamggv1.screen-xxs',
    3 => 'breakpoints.theme.latamggv1.screen-xs',
  );
  $breakpoint_group->type = 'custom';
  $breakpoint_group->overridden = 0;
  $export['latamggv1_no_lg'] = $breakpoint_group;

  $breakpoint_group = new stdClass();
  $breakpoint_group->disabled = FALSE; /* Edit this to true to make a default breakpoint_group disabled initially */
  $breakpoint_group->api_version = 1;
  $breakpoint_group->machine_name = 'latamggv1_no_lg_xxs';
  $breakpoint_group->name = 'latamggv1 no lg xxs';
  $breakpoint_group->breakpoints = array(
    0 => 'breakpoints.theme.latamggv1.screen-md',
    1 => 'breakpoints.theme.latamggv1.screen-sm',
    2 => 'breakpoints.theme.latamggv1.screen-xs',
  );
  $breakpoint_group->type = 'custom';
  $breakpoint_group->overridden = 0;
  $export['latamggv1_no_lg_xxs'] = $breakpoint_group;

  return $export;
}
