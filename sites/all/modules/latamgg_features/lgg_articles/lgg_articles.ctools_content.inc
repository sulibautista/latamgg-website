<?php
/**
 * @file
 * lgg_articles.ctools_content.inc
 */

/**
 * Implements hook_default_ctools_custom_content().
 */
function lgg_articles_default_ctools_custom_content() {
  $export = array();

  $content = new stdClass();
  $content->disabled = FALSE; /* Edit this to true to make a default content disabled initially */
  $content->api_version = 1;
  $content->name = 'fb_comments';
  $content->admin_title = 'Facebook Comments';
  $content->admin_description = '';
  $content->category = 'LatamGG';
  $content->settings = array(
    'admin_title' => 'Facebook Comments',
    'title' => 'Comentarios',
    'body' => '<div class="fb-comments" data-href="%node:url" data-numposts="15" data-colorscheme="light" data-width="100%"></div>',
    'format' => 'raw_html',
    'substitute' => 1,
  );
  $export['fb_comments'] = $content;

  return $export;
}
