<?php
/**
 * @file
 * lgg_articles.default_picture_mapping.inc
 */

/**
 * Implements hook_default_picture_mapping().
 */
function lgg_articles_default_picture_mapping() {
  $export = array();

  $picture_mapping = new PictureMapping();
  $picture_mapping->disabled = FALSE; /* Edit this to true to make a default picture_mapping disabled initially */
  $picture_mapping->api_version = 2;
  $picture_mapping->label = 'Full Article Body Image';
  $picture_mapping->machine_name = 'full_article';
  $picture_mapping->breakpoint_group = 'latamggv1';
  $picture_mapping->mapping = array(
    'breakpoints.theme.latamggv1.screen-lg' => array(
      '1x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'full_article_breakpoints_theme_latamggv1_screen-lg_1x',
      ),
    ),
    'breakpoints.theme.latamggv1.screen-md' => array(
      '1x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'full_article_breakpoints_theme_latamggv1_screen-md_1x',
      ),
    ),
    'breakpoints.theme.latamggv1.screen-sm' => array(
      '1x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'full_article_breakpoints_theme_latamggv1_screen-sm_1x',
      ),
      '2x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'full_article_breakpoints_theme_latamggv1_screen-sm_2x',
      ),
      '1.5x' => array(
        'mapping_type' => '_none',
      ),
    ),
    'breakpoints.theme.latamggv1.screen-xxs' => array(
      '1x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'full_article_breakpoints_theme_latamggv1_screen-xxs_1x',
      ),
      '2x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'full_article_breakpoints_theme_latamggv1_screen-xxs_2x',
      ),
      '1.5x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'full_article_breakpoints_theme_latamggv1_screen-xxs_1_5x',
      ),
      '3x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'full_article_breakpoints_theme_latamggv1_screen-xxs_3x',
      ),
    ),
    'breakpoints.theme.latamggv1.screen-xs' => array(
      '1x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'full_article_breakpoints_theme_latamggv1_screen-xs_1x',
      ),
      '2x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'full_article_breakpoints_theme_latamggv1_screen-xs_2x',
      ),
      '3x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'full_article_breakpoints_theme_latamggv1_screen-xs_3x',
      ),
      '1.5x' => array(
        'mapping_type' => '_none',
      ),
    ),
  );
  $export['full_article'] = $picture_mapping;

  $picture_mapping = new PictureMapping();
  $picture_mapping->disabled = FALSE; /* Edit this to true to make a default picture_mapping disabled initially */
  $picture_mapping->api_version = 2;
  $picture_mapping->label = 'Sticky Highlighted Image';
  $picture_mapping->machine_name = 'sticky_highlighted_image';
  $picture_mapping->breakpoint_group = 'latamggv1_no_lg';
  $picture_mapping->mapping = array(
    'breakpoints.theme.latamggv1.screen-md' => array(
      '1x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'highlight_breakpoints_theme_latamggv1_screen-md_1x',
      ),
    ),
    'breakpoints.theme.latamggv1.screen-sm' => array(
      '1x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'highlight_breakpoints_theme_latamggv1_screen-sm_1x',
      ),
      '2x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'highlight_breakpoints_theme_latamggv1_screen-sm_2x',
      ),
      '1.5x' => array(
        'mapping_type' => '_none',
      ),
    ),
    'breakpoints.theme.latamggv1.screen-xxs' => array(
      '1x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'highlight_breakpoints_theme_latamggv1_screen-xxs_1x',
      ),
      '2x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'highlight_breakpoints_theme_latamggv1_screen-xxs_2x',
      ),
      '1.5x' => array(
        'mapping_type' => '_none',
      ),
      '3x' => array(
        'mapping_type' => '_none',
      ),
    ),
    'breakpoints.theme.latamggv1.screen-xs' => array(
      '1x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'highlight_breakpoints_theme_latamggv1_screen-md_1x',
      ),
      '2x' => array(
        'mapping_type' => '_none',
      ),
      '3x' => array(
        'mapping_type' => '_none',
      ),
      '1.5x' => array(
        'mapping_type' => '_none',
      ),
    ),
  );
  $export['sticky_highlighted_image'] = $picture_mapping;

  $picture_mapping = new PictureMapping();
  $picture_mapping->disabled = FALSE; /* Edit this to true to make a default picture_mapping disabled initially */
  $picture_mapping->api_version = 2;
  $picture_mapping->label = 'Teaser';
  $picture_mapping->machine_name = 'teaser';
  $picture_mapping->breakpoint_group = 'latamggv1_no_lg_xxs';
  $picture_mapping->mapping = array(
    'breakpoints.theme.latamggv1.screen-md' => array(
      '1x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'teaser_breakpoints_theme_latamggv1_screen-md_1x',
      ),
    ),
    'breakpoints.theme.latamggv1.screen-sm' => array(
      '1x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'teaser_breakpoints_theme_latamggv1_screen-sm_1x',
      ),
      '2x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'teaser_breakpoints_theme_latamggv1_screen-sm_2x',
      ),
      '1.5x' => array(
        'mapping_type' => '_none',
      ),
    ),
    'breakpoints.theme.latamggv1.screen-xs' => array(
      '1x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'teaser_breakpoints_theme_latamggv1_screen-xs_1x',
      ),
      '2x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'teaser_breakpoints_theme_latamggv1_screen-md_1x',
      ),
      '3x' => array(
        'mapping_type' => '_none',
      ),
      '1.5x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'teaser_breakpoints_theme_latamggv1_screen-xs_1_5x',
      ),
    ),
  );
  $export['teaser'] = $picture_mapping;

  return $export;
}
