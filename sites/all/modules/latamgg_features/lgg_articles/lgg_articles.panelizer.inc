<?php
/**
 * @file
 * lgg_articles.panelizer.inc
 */

/**
 * Implements hook_panelizer_defaults().
 */
function lgg_articles_panelizer_defaults() {
  $export = array();

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->name = 'node:article:default';
  $panelizer->title = 'Predeterminado';
  $panelizer->panelizer_type = 'node';
  $panelizer->panelizer_key = 'article';
  $panelizer->no_blocks = FALSE;
  $panelizer->css_id = '';
  $panelizer->css = '';
  $panelizer->pipeline = 'standard';
  $panelizer->contexts = array();
  $panelizer->relationships = array();
  $panelizer->access = array();
  $panelizer->view_mode = 'page_manager';
  $panelizer->css_class = '';
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = TRUE;
  $panelizer->extra = array(
    'panels_breadcrumbs_state' => 1,
    'panels_breadcrumbs_titles' => 'Noticias
%node:field-categories:0:name',
    'panels_breadcrumbs_paths' => 'noticias
%node:field-categories:0:url',
    'panels_breadcrumbs_home' => 1,
  );
  $display = new panels_display();
  $display->layout = 'bs_article';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => array(),
      'top' => array(
        'column' => array(
          'xs' => '1',
          'sm' => '1',
          'md' => '1',
          'lg' => '1',
        ),
        'offset' => array(
          'xs' => '0',
          'sm' => '0',
          'md' => '0',
          'lg' => '0',
        ),
        'push' => array(
          'xs' => '0',
          'sm' => '0',
          'md' => '0',
          'lg' => '0',
        ),
        'pull' => array(
          'xs' => '0',
          'sm' => '0',
          'md' => '0',
          'lg' => '0',
        ),
      ),
      'left' => array(
        'column' => array(
          'xs' => '0',
          'sm' => '0',
          'md' => '6',
          'lg' => '0',
        ),
        'offset' => array(
          'xs' => '0',
          'sm' => '0',
          'md' => '0',
          'lg' => '0',
        ),
        'push' => array(
          'xs' => '0',
          'sm' => '0',
          'md' => '3',
          'lg' => '0',
        ),
        'pull' => array(
          'xs' => '0',
          'sm' => '0',
          'md' => '0',
          'lg' => '0',
        ),
      ),
      'right' => array(
        'column' => array(
          'xs' => '0',
          'sm' => '0',
          'md' => '3',
          'lg' => '0',
        ),
        'offset' => array(
          'xs' => '0',
          'sm' => '0',
          'md' => '0',
          'lg' => '0',
        ),
        'push' => array(
          'xs' => '0',
          'sm' => '0',
          'md' => '0',
          'lg' => '0',
        ),
        'pull' => array(
          'xs' => '0',
          'sm' => '0',
          'md' => '0',
          'lg' => '0',
        ),
      ),
      'bottom' => array(
        'column' => array(
          'xs' => '1',
          'sm' => '1',
          'md' => '1',
          'lg' => '1',
        ),
        'offset' => array(
          'xs' => '0',
          'sm' => '0',
          'md' => '0',
          'lg' => '0',
        ),
        'push' => array(
          'xs' => '0',
          'sm' => '0',
          'md' => '0',
          'lg' => '0',
        ),
        'pull' => array(
          'xs' => '0',
          'sm' => '0',
          'md' => '0',
          'lg' => '0',
        ),
      ),
      'middle' => array(
        'column' => array(
          'xs' => '0',
          'sm' => '0',
          'md' => '3',
          'lg' => '0',
        ),
        'offset' => array(
          'xs' => '0',
          'sm' => '0',
          'md' => '0',
          'lg' => '0',
        ),
        'push' => array(
          'xs' => '0',
          'sm' => '0',
          'md' => '0',
          'lg' => '0',
        ),
        'pull' => array(
          'xs' => '0',
          'sm' => '0',
          'md' => '6',
          'lg' => '0',
        ),
      ),
      'content_top' => array(
        'clean_markup' => array(
          'region_wrapper' => 'div',
          'additional_region_classes' => 'col-xxs-12 col-sm-12 col-md-10 col-lg-9',
          'additional_region_attributes' => '',
          'enable_inner_div' => 0,
          'pane_separators' => 0,
        ),
      ),
      'content_bottom' => NULL,
      'side' => NULL,
    ),
    'style' => 'bootstrap_region',
    'top' => array(
      'style' => 'bootstrap_region',
    ),
    'left' => array(
      'style' => 'bootstrap_region',
    ),
    'right' => array(
      'style' => 'bootstrap_region',
    ),
    'bottom' => array(
      'style' => 'bootstrap_region',
    ),
    'middle' => array(
      'style' => 'bootstrap_region',
    ),
    'content_top' => array(
      'style' => 'clean_element',
    ),
    'content_bottom' => array(
      'style' => 'naked',
    ),
    'side' => array(
      'style' => 'naked',
    ),
  );
  $display->cache = array();
  $display->title = '%node:title';
  $display->uuid = '294d91aa-ddb0-4e04-b1b9-3921b82a14ea';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-b077dcca-829d-4b0d-b39a-120b208db0f0';
    $pane->panel = 'content_bottom';
    $pane->type = 'panels_mini';
    $pane->subtype = 'node_footer';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'context' => array(
        0 => 'panelizer',
      ),
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'naked',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'b077dcca-829d-4b0d-b39a-120b208db0f0';
    $display->content['new-b077dcca-829d-4b0d-b39a-120b208db0f0'] = $pane;
    $display->panels['content_bottom'][0] = 'new-b077dcca-829d-4b0d-b39a-120b208db0f0';
    $pane = new stdClass();
    $pane->pid = 'new-aaf59a6b-8278-40fa-9fda-1dc5af56254d';
    $pane->panel = 'content_top';
    $pane->type = 'node_content';
    $pane->subtype = 'node_content';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'links' => 0,
      'no_extras' => 0,
      'override_title' => 1,
      'override_title_text' => '',
      'identifier' => 'article',
      'link' => 0,
      'leave_node_title' => 1,
      'build_mode' => 'full',
      'context' => 'panelizer',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'naked',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'aaf59a6b-8278-40fa-9fda-1dc5af56254d';
    $display->content['new-aaf59a6b-8278-40fa-9fda-1dc5af56254d'] = $pane;
    $display->panels['content_top'][0] = 'new-aaf59a6b-8278-40fa-9fda-1dc5af56254d';
    $pane = new stdClass();
    $pane->pid = 'new-1b9a3e11-b957-4884-b86d-d978221a9c68';
    $pane->panel = 'side';
    $pane->type = 'views_panes';
    $pane->subtype = 'recent_articles-panel_pane_1';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'context' => array(
        0 => 'panelizer',
      ),
      'override_title' => 1,
      'override_title_text' => 'Historias recientes',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => array(
        'clean_markup' => array(
          'pane_wrapper' => 'div',
          'additional_pane_classes' => 'panel-pane',
          'additional_pane_attributes' => '',
          'enable_inner_div' => 0,
          'title_wrapper' => 'h2',
          'title_hide' => 0,
          'content_wrapper' => 'none',
        ),
      ),
      'style' => 'clean_element',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '1b9a3e11-b957-4884-b86d-d978221a9c68';
    $display->content['new-1b9a3e11-b957-4884-b86d-d978221a9c68'] = $pane;
    $display->panels['side'][0] = 'new-1b9a3e11-b957-4884-b86d-d978221a9c68';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $panelizer->display = $display;
  $export['node:article:default'] = $panelizer;

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->name = 'taxonomy_term:categories:default';
  $panelizer->title = 'Predeterminado';
  $panelizer->panelizer_type = 'taxonomy_term';
  $panelizer->panelizer_key = 'categories';
  $panelizer->no_blocks = FALSE;
  $panelizer->css_id = '';
  $panelizer->css = '';
  $panelizer->pipeline = 'standard';
  $panelizer->contexts = array();
  $panelizer->relationships = array(
    0 => array(
      'identifier' => 'Vista: Primer Sticky del Juego/Categoria',
      'keyword' => 'view',
      'name' => 'view_from_argument:first_sticky_by_game_category-ctools_context_1',
      'context' => array(
        0 => 'empty',
        1 => 'panelizer',
      ),
      'id' => 1,
    ),
    2 => array(
      'identifier' => 'Nodo de la vista',
      'keyword' => 'first_sticky_node',
      'name' => 'node_from_view',
      'row' => '1',
      'context' => 'relationship_view_from_argument:first_sticky_by_game_category-ctools_context_1_1',
      'id' => 1,
    ),
  );
  $panelizer->access = array();
  $panelizer->view_mode = 'page_manager';
  $panelizer->css_class = 'node-listing';
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = TRUE;
  $panelizer->extra = array(
    'panels_breadcrumbs_state' => 1,
    'panels_breadcrumbs_titles' => 'Noticias',
    'panels_breadcrumbs_paths' => 'noticias',
    'panels_breadcrumbs_home' => 1,
  );
  $display = new panels_display();
  $display->layout = 'bootstrap_twocol_stacked';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'top' => array(
        'column' => array(
          'xs' => '12',
          'sm' => '0',
          'md' => '0',
          'lg' => '0',
        ),
        'offset' => array(
          'xs' => '0',
          'sm' => '0',
          'md' => '0',
          'lg' => '0',
        ),
        'push' => array(
          'xs' => '0',
          'sm' => '0',
          'md' => '0',
          'lg' => '0',
        ),
        'pull' => array(
          'xs' => '0',
          'sm' => '0',
          'md' => '0',
          'lg' => '0',
        ),
      ),
      'left' => array(
        'column' => array(
          'xs' => '0',
          'sm' => '8',
          'md' => '0',
          'lg' => '0',
        ),
        'offset' => array(
          'xs' => '0',
          'sm' => '0',
          'md' => '0',
          'lg' => '0',
        ),
        'push' => array(
          'xs' => '0',
          'sm' => '0',
          'md' => '0',
          'lg' => '0',
        ),
        'pull' => array(
          'xs' => '0',
          'sm' => '0',
          'md' => '0',
          'lg' => '0',
        ),
      ),
      'right' => array(
        'column' => array(
          'xs' => '0',
          'sm' => '4',
          'md' => '0',
          'lg' => '0',
        ),
        'offset' => array(
          'xs' => '0',
          'sm' => '0',
          'md' => '0',
          'lg' => '0',
        ),
        'push' => array(
          'xs' => '0',
          'sm' => '0',
          'md' => '0',
          'lg' => '0',
        ),
        'pull' => array(
          'xs' => '0',
          'sm' => '0',
          'md' => '0',
          'lg' => '0',
        ),
      ),
      'bottom' => NULL,
    ),
    'style' => 'naked',
    'top' => array(
      'style' => 'bootstrap_region',
    ),
    'right' => array(
      'style' => 'bootstrap_region',
    ),
    'left' => array(
      'style' => 'bootstrap_region',
    ),
    'bottom' => array(
      'style' => 'naked',
    ),
  );
  $display->cache = array();
  $display->title = '%taxonomy_term:name';
  $display->uuid = '59911e0b-b4b9-43ca-a206-d075ad0ac710';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-62c52d15-e6c5-4647-bc29-043d726546fe';
    $pane->panel = 'left';
    $pane->type = 'block';
    $pane->subtype = 'menu-menu-article-categories';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '%taxonomy_term:name',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'default',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '62c52d15-e6c5-4647-bc29-043d726546fe';
    $display->content['new-62c52d15-e6c5-4647-bc29-043d726546fe'] = $pane;
    $display->panels['left'][0] = 'new-62c52d15-e6c5-4647-bc29-043d726546fe';
    $pane = new stdClass();
    $pane->pid = 'new-3e425e31-4dc1-4823-8eeb-c234ca0cea0a';
    $pane->panel = 'left';
    $pane->type = 'views_panes';
    $pane->subtype = 'game_category_articles-panel_pane_1';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'context' => array(
        0 => 'empty',
        1 => 'panelizer',
      ),
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'naked',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '3e425e31-4dc1-4823-8eeb-c234ca0cea0a';
    $display->content['new-3e425e31-4dc1-4823-8eeb-c234ca0cea0a'] = $pane;
    $display->panels['left'][1] = 'new-3e425e31-4dc1-4823-8eeb-c234ca0cea0a';
    $pane = new stdClass();
    $pane->pid = 'new-1006646e-9850-427e-9553-4d651633fabb';
    $pane->panel = 'right';
    $pane->type = 'custom';
    $pane->subtype = 'become_a_fan';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => '',
      'title' => '',
      'body' => '',
      'format' => 'full_html',
      'substitute' => TRUE,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => array(
        'clean_markup' => array(
          'pane_wrapper' => 'div',
          'additional_pane_classes' => '',
          'additional_pane_attributes' => '',
          'enable_inner_div' => 0,
          'title_wrapper' => 'h2',
          'title_hide' => 0,
          'content_wrapper' => 'none',
        ),
      ),
      'style' => 'clean_element',
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'start-pane-spacing',
    );
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '1006646e-9850-427e-9553-4d651633fabb';
    $display->content['new-1006646e-9850-427e-9553-4d651633fabb'] = $pane;
    $display->panels['right'][0] = 'new-1006646e-9850-427e-9553-4d651633fabb';
    $pane = new stdClass();
    $pane->pid = 'new-4737c1c2-8516-4f70-afaa-1551ddd42476';
    $pane->panel = 'right';
    $pane->type = 'views_panes';
    $pane->subtype = 'popular_articles-panel_pane_1';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => 'Lo mejor',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => array(
        'clean_markup' => array(
          'pane_wrapper' => 'div',
          'additional_pane_classes' => '',
          'additional_pane_attributes' => '',
          'enable_inner_div' => 0,
          'title_wrapper' => 'h2',
          'title_hide' => 0,
          'content_wrapper' => 'none',
        ),
      ),
      'style' => 'clean_element',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '4737c1c2-8516-4f70-afaa-1551ddd42476';
    $display->content['new-4737c1c2-8516-4f70-afaa-1551ddd42476'] = $pane;
    $display->panels['right'][1] = 'new-4737c1c2-8516-4f70-afaa-1551ddd42476';
    $pane = new stdClass();
    $pane->pid = 'new-a32731b3-086a-4652-b761-ca91dfb4c048';
    $pane->panel = 'top';
    $pane->type = 'panels_mini';
    $pane->subtype = 'highlighted_article';
    $pane->shown = TRUE;
    $pane->access = array(
      'plugins' => array(
        0 => array(
          'name' => 'context_exists',
          'settings' => array(
            'exists' => '1',
          ),
          'context' => 'relationship_node_from_view_1',
          'not' => FALSE,
        ),
      ),
    );
    $pane->configuration = array(
      'context' => array(
        0 => 'relationship_node_from_view_1',
      ),
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'naked',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'a32731b3-086a-4652-b761-ca91dfb4c048';
    $display->content['new-a32731b3-086a-4652-b761-ca91dfb4c048'] = $pane;
    $display->panels['top'][0] = 'new-a32731b3-086a-4652-b761-ca91dfb4c048';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $panelizer->display = $display;
  $export['taxonomy_term:categories:default'] = $panelizer;

  return $export;
}
