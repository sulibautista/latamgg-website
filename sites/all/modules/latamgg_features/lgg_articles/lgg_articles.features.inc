<?php
/**
 * @file
 * lgg_articles.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function lgg_articles_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "breakpoints" && $api == "default_breakpoint_group") {
    return array("version" => "1");
  }
  if ($module == "ctools_custom_content" && $api == "ctools_content") {
    return array("version" => "1");
  }
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
  if ($module == "panelizer" && $api == "panelizer") {
    return array("version" => "1");
  }
  if ($module == "panels_mini" && $api == "panels_default") {
    return array("version" => "1");
  }
  if ($module == "picture" && $api == "default_picture_mapping") {
    return array("version" => "2");
  }
  if ($module == "scald" && $api == "context_config") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function lgg_articles_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function lgg_articles_image_default_styles() {
  $styles = array();

  // Exported image style: full_article_breakpoints_theme_latamggv1_screen-lg_1x.
  $styles['full_article_breakpoints_theme_latamggv1_screen-lg_1x'] = array(
    'name' => 'full_article_breakpoints_theme_latamggv1_screen-lg_1x',
    'label' => 'full_article_breakpoints_theme_latamggv1_screen-lg_1x',
    'effects' => array(
      33 => array(
        'label' => 'Escala',
        'help' => 'El escalado mantiene la relación de proporciones de la imagen original. Si sólo se especifica una dimensión, la otra se calculará.',
        'effect callback' => 'image_scale_effect',
        'dimensions callback' => 'image_scale_dimensions',
        'form callback' => 'image_scale_form',
        'summary theme' => 'image_scale_summary',
        'module' => 'image',
        'name' => 'image_scale',
        'data' => array(
          'width' => 623,
          'height' => '',
          'upscale' => 0,
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: full_article_breakpoints_theme_latamggv1_screen-md_1x.
  $styles['full_article_breakpoints_theme_latamggv1_screen-md_1x'] = array(
    'name' => 'full_article_breakpoints_theme_latamggv1_screen-md_1x',
    'label' => 'full_article_breakpoints_theme_latamggv1_screen-md_1x',
    'effects' => array(
      24 => array(
        'label' => 'Escala',
        'help' => 'El escalado mantiene la relación de proporciones de la imagen original. Si sólo se especifica una dimensión, la otra se calculará.',
        'effect callback' => 'image_scale_effect',
        'dimensions callback' => 'image_scale_dimensions',
        'form callback' => 'image_scale_form',
        'summary theme' => 'image_scale_summary',
        'module' => 'image',
        'name' => 'image_scale',
        'data' => array(
          'width' => 570,
          'height' => '',
          'upscale' => 0,
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: full_article_breakpoints_theme_latamggv1_screen-sm_1x.
  $styles['full_article_breakpoints_theme_latamggv1_screen-sm_1x'] = array(
    'name' => 'full_article_breakpoints_theme_latamggv1_screen-sm_1x',
    'label' => 'full_article_breakpoints_theme_latamggv1_screen-sm_1x',
    'effects' => array(
      26 => array(
        'label' => 'Escala',
        'help' => 'El escalado mantiene la relación de proporciones de la imagen original. Si sólo se especifica una dimensión, la otra se calculará.',
        'effect callback' => 'image_scale_effect',
        'dimensions callback' => 'image_scale_dimensions',
        'form callback' => 'image_scale_form',
        'summary theme' => 'image_scale_summary',
        'module' => 'image',
        'name' => 'image_scale',
        'data' => array(
          'width' => 539,
          'height' => '',
          'upscale' => 0,
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: full_article_breakpoints_theme_latamggv1_screen-sm_2x.
  $styles['full_article_breakpoints_theme_latamggv1_screen-sm_2x'] = array(
    'name' => 'full_article_breakpoints_theme_latamggv1_screen-sm_2x',
    'label' => 'full_article_breakpoints_theme_latamggv1_screen-sm_2x',
    'effects' => array(
      36 => array(
        'label' => 'Resize (percent)',
        'help' => 'Resize the image based on percent. If only a single dimension is specified, the other dimension will be calculated.',
        'effect callback' => 'canvasactions_resizepercent_effect',
        'dimensions callback' => 'canvasactions_resizepercent_dimensions',
        'form callback' => 'canvasactions_resizepercent_form',
        'summary theme' => 'canvasactions_resizepercent_summary',
        'module' => 'imagecache_canvasactions',
        'name' => 'canvasactions_resizepercent',
        'data' => array(
          'width' => 200,
          'height' => '',
        ),
        'weight' => -10,
      ),
      25 => array(
        'label' => 'Escala',
        'help' => 'El escalado mantiene la relación de proporciones de la imagen original. Si sólo se especifica una dimensión, la otra se calculará.',
        'effect callback' => 'image_scale_effect',
        'dimensions callback' => 'image_scale_dimensions',
        'form callback' => 'image_scale_form',
        'summary theme' => 'image_scale_summary',
        'module' => 'image',
        'name' => 'image_scale',
        'data' => array(
          'width' => 1078,
          'height' => '',
          'upscale' => 0,
        ),
        'weight' => -9,
      ),
      43 => array(
        'label' => 'Subroutine',
        'help' => 'Runs another defined preset on the image.',
        'effect callback' => 'imagecache_subroutine_effect',
        'dimensions callback' => 'imagecache_subroutine_dimensions',
        'form callback' => 'imagecache_subroutine_form',
        'summary theme' => 'imagecache_subroutine_summary',
        'module' => 'imagecache_customactions',
        'name' => 'imagecache_subroutine',
        'data' => array(
          'subroutine_presetname' => 'latamgg_subroutine_set_quality_x2',
        ),
        'weight' => 2,
      ),
    ),
  );

  // Exported image style: full_article_breakpoints_theme_latamggv1_screen-xs_1x.
  $styles['full_article_breakpoints_theme_latamggv1_screen-xs_1x'] = array(
    'name' => 'full_article_breakpoints_theme_latamggv1_screen-xs_1x',
    'label' => 'full_article_breakpoints_theme_latamggv1_screen-xs_1x',
    'effects' => array(
      32 => array(
        'label' => 'Escala',
        'help' => 'El escalado mantiene la relación de proporciones de la imagen original. Si sólo se especifica una dimensión, la otra se calculará.',
        'effect callback' => 'image_scale_effect',
        'dimensions callback' => 'image_scale_dimensions',
        'form callback' => 'image_scale_form',
        'summary theme' => 'image_scale_summary',
        'module' => 'image',
        'name' => 'image_scale',
        'data' => array(
          'width' => 610,
          'height' => '',
          'upscale' => 0,
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: full_article_breakpoints_theme_latamggv1_screen-xs_2x.
  $styles['full_article_breakpoints_theme_latamggv1_screen-xs_2x'] = array(
    'name' => 'full_article_breakpoints_theme_latamggv1_screen-xs_2x',
    'label' => 'full_article_breakpoints_theme_latamggv1_screen-xs_2x',
    'effects' => array(
      38 => array(
        'label' => 'Resize (percent)',
        'help' => 'Resize the image based on percent. If only a single dimension is specified, the other dimension will be calculated.',
        'effect callback' => 'canvasactions_resizepercent_effect',
        'dimensions callback' => 'canvasactions_resizepercent_dimensions',
        'form callback' => 'canvasactions_resizepercent_form',
        'summary theme' => 'canvasactions_resizepercent_summary',
        'module' => 'imagecache_canvasactions',
        'name' => 'canvasactions_resizepercent',
        'data' => array(
          'width' => 200,
          'height' => '',
        ),
        'weight' => -10,
      ),
      30 => array(
        'label' => 'Escala',
        'help' => 'El escalado mantiene la relación de proporciones de la imagen original. Si sólo se especifica una dimensión, la otra se calculará.',
        'effect callback' => 'image_scale_effect',
        'dimensions callback' => 'image_scale_dimensions',
        'form callback' => 'image_scale_form',
        'summary theme' => 'image_scale_summary',
        'module' => 'image',
        'name' => 'image_scale',
        'data' => array(
          'width' => 1274,
          'height' => '',
          'upscale' => 0,
        ),
        'weight' => -9,
      ),
      44 => array(
        'label' => 'Subroutine',
        'help' => 'Runs another defined preset on the image.',
        'effect callback' => 'imagecache_subroutine_effect',
        'dimensions callback' => 'imagecache_subroutine_dimensions',
        'form callback' => 'imagecache_subroutine_form',
        'summary theme' => 'imagecache_subroutine_summary',
        'module' => 'imagecache_customactions',
        'name' => 'imagecache_subroutine',
        'data' => array(
          'subroutine_presetname' => 'latamgg_subroutine_set_quality_x2',
        ),
        'weight' => 2,
      ),
    ),
  );

  // Exported image style: full_article_breakpoints_theme_latamggv1_screen-xs_3x.
  $styles['full_article_breakpoints_theme_latamggv1_screen-xs_3x'] = array(
    'name' => 'full_article_breakpoints_theme_latamggv1_screen-xs_3x',
    'label' => 'full_article_breakpoints_theme_latamggv1_screen-xs_3x',
    'effects' => array(
      37 => array(
        'label' => 'Resize (percent)',
        'help' => 'Resize the image based on percent. If only a single dimension is specified, the other dimension will be calculated.',
        'effect callback' => 'canvasactions_resizepercent_effect',
        'dimensions callback' => 'canvasactions_resizepercent_dimensions',
        'form callback' => 'canvasactions_resizepercent_form',
        'summary theme' => 'canvasactions_resizepercent_summary',
        'module' => 'imagecache_canvasactions',
        'name' => 'canvasactions_resizepercent',
        'data' => array(
          'width' => 300,
          'height' => '',
        ),
        'weight' => 0,
      ),
      39 => array(
        'label' => 'Escala',
        'help' => 'El escalado mantiene la relación de proporciones de la imagen original. Si sólo se especifica una dimensión, la otra se calculará.',
        'effect callback' => 'image_scale_effect',
        'dimensions callback' => 'image_scale_dimensions',
        'form callback' => 'image_scale_form',
        'summary theme' => 'image_scale_summary',
        'module' => 'image',
        'name' => 'image_scale',
        'data' => array(
          'width' => 2118,
          'height' => '',
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
      48 => array(
        'label' => 'Subroutine',
        'help' => 'Runs another defined preset on the image.',
        'effect callback' => 'imagecache_subroutine_effect',
        'dimensions callback' => 'imagecache_subroutine_dimensions',
        'form callback' => 'imagecache_subroutine_form',
        'summary theme' => 'imagecache_subroutine_summary',
        'module' => 'imagecache_customactions',
        'name' => 'imagecache_subroutine',
        'data' => array(
          'subroutine_presetname' => 'latamgg_subroutine_set_quality_x3',
        ),
        'weight' => 2,
      ),
    ),
  );

  // Exported image style: full_article_breakpoints_theme_latamggv1_screen-xxs_1_5x.
  $styles['full_article_breakpoints_theme_latamggv1_screen-xxs_1_5x'] = array(
    'name' => 'full_article_breakpoints_theme_latamggv1_screen-xxs_1_5x',
    'label' => 'full_article_breakpoints_theme_latamggv1_screen-xxs_1_5x',
    'effects' => array(
      40 => array(
        'label' => 'Resize (percent)',
        'help' => 'Resize the image based on percent. If only a single dimension is specified, the other dimension will be calculated.',
        'effect callback' => 'canvasactions_resizepercent_effect',
        'dimensions callback' => 'canvasactions_resizepercent_dimensions',
        'form callback' => 'canvasactions_resizepercent_form',
        'summary theme' => 'canvasactions_resizepercent_summary',
        'module' => 'imagecache_canvasactions',
        'name' => 'canvasactions_resizepercent',
        'data' => array(
          'width' => 150,
          'height' => '',
        ),
        'weight' => -10,
      ),
      28 => array(
        'label' => 'Escala',
        'help' => 'El escalado mantiene la relación de proporciones de la imagen original. Si sólo se especifica una dimensión, la otra se calculará.',
        'effect callback' => 'image_scale_effect',
        'dimensions callback' => 'image_scale_dimensions',
        'form callback' => 'image_scale_form',
        'summary theme' => 'image_scale_summary',
        'module' => 'image',
        'name' => 'image_scale',
        'data' => array(
          'width' => 435,
          'height' => '',
          'upscale' => 0,
        ),
        'weight' => -9,
      ),
      49 => array(
        'label' => 'Subroutine',
        'help' => 'Runs another defined preset on the image.',
        'effect callback' => 'imagecache_subroutine_effect',
        'dimensions callback' => 'imagecache_subroutine_dimensions',
        'form callback' => 'imagecache_subroutine_form',
        'summary theme' => 'imagecache_subroutine_summary',
        'module' => 'imagecache_customactions',
        'name' => 'imagecache_subroutine',
        'data' => array(
          'subroutine_presetname' => 'latamgg_subroutine_set_quality_x1_5',
        ),
        'weight' => 2,
      ),
    ),
  );

  // Exported image style: full_article_breakpoints_theme_latamggv1_screen-xxs_1x.
  $styles['full_article_breakpoints_theme_latamggv1_screen-xxs_1x'] = array(
    'name' => 'full_article_breakpoints_theme_latamggv1_screen-xxs_1x',
    'label' => 'full_article_breakpoints_theme_latamggv1_screen-xxs_1x',
    'effects' => array(
      29 => array(
        'label' => 'Escala',
        'help' => 'El escalado mantiene la relación de proporciones de la imagen original. Si sólo se especifica una dimensión, la otra se calculará.',
        'effect callback' => 'image_scale_effect',
        'dimensions callback' => 'image_scale_dimensions',
        'form callback' => 'image_scale_form',
        'summary theme' => 'image_scale_summary',
        'module' => 'image',
        'name' => 'image_scale',
        'data' => array(
          'width' => 330,
          'height' => '',
          'upscale' => 0,
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: full_article_breakpoints_theme_latamggv1_screen-xxs_2x.
  $styles['full_article_breakpoints_theme_latamggv1_screen-xxs_2x'] = array(
    'name' => 'full_article_breakpoints_theme_latamggv1_screen-xxs_2x',
    'label' => 'full_article_breakpoints_theme_latamggv1_screen-xxs_2x',
    'effects' => array(
      41 => array(
        'label' => 'Resize (percent)',
        'help' => 'Resize the image based on percent. If only a single dimension is specified, the other dimension will be calculated.',
        'effect callback' => 'canvasactions_resizepercent_effect',
        'dimensions callback' => 'canvasactions_resizepercent_dimensions',
        'form callback' => 'canvasactions_resizepercent_form',
        'summary theme' => 'canvasactions_resizepercent_summary',
        'module' => 'imagecache_canvasactions',
        'name' => 'canvasactions_resizepercent',
        'data' => array(
          'width' => 200,
          'height' => '',
        ),
        'weight' => -10,
      ),
      27 => array(
        'label' => 'Escala',
        'help' => 'El escalado mantiene la relación de proporciones de la imagen original. Si sólo se especifica una dimensión, la otra se calculará.',
        'effect callback' => 'image_scale_effect',
        'dimensions callback' => 'image_scale_dimensions',
        'form callback' => 'image_scale_form',
        'summary theme' => 'image_scale_summary',
        'module' => 'image',
        'name' => 'image_scale',
        'data' => array(
          'width' => 690,
          'height' => '',
          'upscale' => 0,
        ),
        'weight' => -9,
      ),
      46 => array(
        'label' => 'Subroutine',
        'help' => 'Runs another defined preset on the image.',
        'effect callback' => 'imagecache_subroutine_effect',
        'dimensions callback' => 'imagecache_subroutine_dimensions',
        'form callback' => 'imagecache_subroutine_form',
        'summary theme' => 'imagecache_subroutine_summary',
        'module' => 'imagecache_customactions',
        'name' => 'imagecache_subroutine',
        'data' => array(
          'subroutine_presetname' => 'latamgg_subroutine_set_quality_x2',
        ),
        'weight' => 2,
      ),
    ),
  );

  // Exported image style: full_article_breakpoints_theme_latamggv1_screen-xxs_3x.
  $styles['full_article_breakpoints_theme_latamggv1_screen-xxs_3x'] = array(
    'name' => 'full_article_breakpoints_theme_latamggv1_screen-xxs_3x',
    'label' => 'full_article_breakpoints_theme_latamggv1_screen-xxs_3x',
    'effects' => array(
      35 => array(
        'label' => 'Resize (percent)',
        'help' => 'Resize the image based on percent. If only a single dimension is specified, the other dimension will be calculated.',
        'effect callback' => 'canvasactions_resizepercent_effect',
        'dimensions callback' => 'canvasactions_resizepercent_dimensions',
        'form callback' => 'canvasactions_resizepercent_form',
        'summary theme' => 'canvasactions_resizepercent_summary',
        'module' => 'imagecache_canvasactions',
        'name' => 'canvasactions_resizepercent',
        'data' => array(
          'width' => 300,
          'height' => '',
        ),
        'weight' => -10,
      ),
      34 => array(
        'label' => 'Escala',
        'help' => 'El escalado mantiene la relación de proporciones de la imagen original. Si sólo se especifica una dimensión, la otra se calculará.',
        'effect callback' => 'image_scale_effect',
        'dimensions callback' => 'image_scale_dimensions',
        'form callback' => 'image_scale_form',
        'summary theme' => 'image_scale_summary',
        'module' => 'image',
        'name' => 'image_scale',
        'data' => array(
          'width' => 1152,
          'height' => '',
          'upscale' => 0,
        ),
        'weight' => -9,
      ),
      50 => array(
        'label' => 'Subroutine',
        'help' => 'Runs another defined preset on the image.',
        'effect callback' => 'imagecache_subroutine_effect',
        'dimensions callback' => 'imagecache_subroutine_dimensions',
        'form callback' => 'imagecache_subroutine_form',
        'summary theme' => 'imagecache_subroutine_summary',
        'module' => 'imagecache_customactions',
        'name' => 'imagecache_subroutine',
        'data' => array(
          'subroutine_presetname' => 'latamgg_subroutine_set_quality_x3',
        ),
        'weight' => 2,
      ),
    ),
  );

  // Exported image style: highlight_breakpoints_theme_latamggv1_screen-md_1x.
  $styles['highlight_breakpoints_theme_latamggv1_screen-md_1x'] = array(
    'name' => 'highlight_breakpoints_theme_latamggv1_screen-md_1x',
    'label' => 'highlight_breakpoints_theme_latamggv1_screen-md_1x',
    'effects' => array(
      18 => array(
        'label' => 'Escalar y recortar',
        'help' => 'Escalar y recortar mantendrán la relación de proporciones de la imagen original y se desechará lo que sobre por el lado mayor. Esto es especialmente útil para crear miniaturas perfectamente cuadradas sin deformar la imagen.',
        'effect callback' => 'image_scale_and_crop_effect',
        'dimensions callback' => 'image_resize_dimensions',
        'form callback' => 'image_resize_form',
        'summary theme' => 'image_resize_summary',
        'module' => 'image',
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 774,
          'height' => 280,
        ),
        'weight' => 2,
      ),
    ),
  );

  // Exported image style: highlight_breakpoints_theme_latamggv1_screen-sm_1x.
  $styles['highlight_breakpoints_theme_latamggv1_screen-sm_1x'] = array(
    'name' => 'highlight_breakpoints_theme_latamggv1_screen-sm_1x',
    'label' => 'highlight_breakpoints_theme_latamggv1_screen-sm_1x',
    'effects' => array(
      19 => array(
        'label' => 'Escalar y recortar',
        'help' => 'Escalar y recortar mantendrán la relación de proporciones de la imagen original y se desechará lo que sobre por el lado mayor. Esto es especialmente útil para crear miniaturas perfectamente cuadradas sin deformar la imagen.',
        'effect callback' => 'image_scale_and_crop_effect',
        'dimensions callback' => 'image_resize_dimensions',
        'form callback' => 'image_resize_form',
        'summary theme' => 'image_resize_summary',
        'module' => 'image',
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 492,
          'height' => 280,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: highlight_breakpoints_theme_latamggv1_screen-sm_2x.
  $styles['highlight_breakpoints_theme_latamggv1_screen-sm_2x'] = array(
    'name' => 'highlight_breakpoints_theme_latamggv1_screen-sm_2x',
    'label' => 'highlight_breakpoints_theme_latamggv1_screen-sm_2x',
    'effects' => array(
      20 => array(
        'label' => 'Escalar y recortar',
        'help' => 'Escalar y recortar mantendrán la relación de proporciones de la imagen original y se desechará lo que sobre por el lado mayor. Esto es especialmente útil para crear miniaturas perfectamente cuadradas sin deformar la imagen.',
        'effect callback' => 'image_scale_and_crop_effect',
        'dimensions callback' => 'image_resize_dimensions',
        'form callback' => 'image_resize_form',
        'summary theme' => 'image_resize_summary',
        'module' => 'image',
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 984,
          'height' => 560,
        ),
        'weight' => 1,
      ),
      53 => array(
        'label' => 'Subroutine',
        'help' => 'Runs another defined preset on the image.',
        'effect callback' => 'imagecache_subroutine_effect',
        'dimensions callback' => 'imagecache_subroutine_dimensions',
        'form callback' => 'imagecache_subroutine_form',
        'summary theme' => 'imagecache_subroutine_summary',
        'module' => 'imagecache_customactions',
        'name' => 'imagecache_subroutine',
        'data' => array(
          'subroutine_presetname' => 'latamgg_subroutine_set_quality_x2',
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: highlight_breakpoints_theme_latamggv1_screen-xxs_1x.
  $styles['highlight_breakpoints_theme_latamggv1_screen-xxs_1x'] = array(
    'name' => 'highlight_breakpoints_theme_latamggv1_screen-xxs_1x',
    'label' => 'highlight_breakpoints_theme_latamggv1_screen-xxs_1x',
    'effects' => array(
      22 => array(
        'label' => 'Escalar y recortar',
        'help' => 'Escalar y recortar mantendrán la relación de proporciones de la imagen original y se desechará lo que sobre por el lado mayor. Esto es especialmente útil para crear miniaturas perfectamente cuadradas sin deformar la imagen.',
        'effect callback' => 'image_scale_and_crop_effect',
        'dimensions callback' => 'image_resize_dimensions',
        'form callback' => 'image_resize_form',
        'summary theme' => 'image_resize_summary',
        'module' => 'image',
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 450,
          'height' => 163,
        ),
        'weight' => 2,
      ),
    ),
  );

  // Exported image style: highlight_breakpoints_theme_latamggv1_screen-xxs_2x.
  $styles['highlight_breakpoints_theme_latamggv1_screen-xxs_2x'] = array(
    'name' => 'highlight_breakpoints_theme_latamggv1_screen-xxs_2x',
    'label' => 'highlight_breakpoints_theme_latamggv1_screen-xxs_2x',
    'effects' => array(
      21 => array(
        'label' => 'Escalar y recortar',
        'help' => 'Escalar y recortar mantendrán la relación de proporciones de la imagen original y se desechará lo que sobre por el lado mayor. Esto es especialmente útil para crear miniaturas perfectamente cuadradas sin deformar la imagen.',
        'effect callback' => 'image_scale_and_crop_effect',
        'dimensions callback' => 'image_resize_dimensions',
        'form callback' => 'image_resize_form',
        'summary theme' => 'image_resize_summary',
        'module' => 'image',
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 900,
          'height' => 326,
        ),
        'weight' => -10,
      ),
      54 => array(
        'label' => 'Subroutine',
        'help' => 'Runs another defined preset on the image.',
        'effect callback' => 'imagecache_subroutine_effect',
        'dimensions callback' => 'imagecache_subroutine_dimensions',
        'form callback' => 'imagecache_subroutine_form',
        'summary theme' => 'imagecache_subroutine_summary',
        'module' => 'imagecache_customactions',
        'name' => 'imagecache_subroutine',
        'data' => array(
          'subroutine_presetname' => 'latamgg_subroutine_set_quality_x2',
        ),
        'weight' => -9,
      ),
    ),
  );

  // Exported image style: teaser_breakpoints_theme_latamggv1_screen-md_1x.
  $styles['teaser_breakpoints_theme_latamggv1_screen-md_1x'] = array(
    'name' => 'teaser_breakpoints_theme_latamggv1_screen-md_1x',
    'label' => 'teaser_breakpoints_theme_latamggv1_screen-md_1x',
    'effects' => array(
      5 => array(
        'label' => 'Escalar y recortar',
        'help' => 'Escalar y recortar mantendrán la relación de proporciones de la imagen original y se desechará lo que sobre por el lado mayor. Esto es especialmente útil para crear miniaturas perfectamente cuadradas sin deformar la imagen.',
        'effect callback' => 'image_scale_and_crop_effect',
        'dimensions callback' => 'image_resize_dimensions',
        'form callback' => 'image_resize_form',
        'summary theme' => 'image_resize_summary',
        'module' => 'image',
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 212,
          'height' => 136,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: teaser_breakpoints_theme_latamggv1_screen-sm_1x.
  $styles['teaser_breakpoints_theme_latamggv1_screen-sm_1x'] = array(
    'name' => 'teaser_breakpoints_theme_latamggv1_screen-sm_1x',
    'label' => 'teaser_breakpoints_theme_latamggv1_screen-sm_1x',
    'effects' => array(
      7 => array(
        'label' => 'Escalar y recortar',
        'help' => 'Escalar y recortar mantendrán la relación de proporciones de la imagen original y se desechará lo que sobre por el lado mayor. Esto es especialmente útil para crear miniaturas perfectamente cuadradas sin deformar la imagen.',
        'effect callback' => 'image_scale_and_crop_effect',
        'dimensions callback' => 'image_resize_dimensions',
        'form callback' => 'image_resize_form',
        'summary theme' => 'image_resize_summary',
        'module' => 'image',
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 160,
          'height' => 120,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: teaser_breakpoints_theme_latamggv1_screen-sm_2x.
  $styles['teaser_breakpoints_theme_latamggv1_screen-sm_2x'] = array(
    'name' => 'teaser_breakpoints_theme_latamggv1_screen-sm_2x',
    'label' => 'teaser_breakpoints_theme_latamggv1_screen-sm_2x',
    'effects' => array(
      6 => array(
        'label' => 'Escalar y recortar',
        'help' => 'Escalar y recortar mantendrán la relación de proporciones de la imagen original y se desechará lo que sobre por el lado mayor. Esto es especialmente útil para crear miniaturas perfectamente cuadradas sin deformar la imagen.',
        'effect callback' => 'image_scale_and_crop_effect',
        'dimensions callback' => 'image_resize_dimensions',
        'form callback' => 'image_resize_form',
        'summary theme' => 'image_resize_summary',
        'module' => 'image',
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 320,
          'height' => 240,
        ),
        'weight' => 1,
      ),
      52 => array(
        'label' => 'Subroutine',
        'help' => 'Runs another defined preset on the image.',
        'effect callback' => 'imagecache_subroutine_effect',
        'dimensions callback' => 'imagecache_subroutine_dimensions',
        'form callback' => 'imagecache_subroutine_form',
        'summary theme' => 'imagecache_subroutine_summary',
        'module' => 'imagecache_customactions',
        'name' => 'imagecache_subroutine',
        'data' => array(
          'subroutine_presetname' => 'latamgg_subroutine_set_quality_x2',
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: teaser_breakpoints_theme_latamggv1_screen-xs_1_5x.
  $styles['teaser_breakpoints_theme_latamggv1_screen-xs_1_5x'] = array(
    'name' => 'teaser_breakpoints_theme_latamggv1_screen-xs_1_5x',
    'label' => 'teaser_breakpoints_theme_latamggv1_screen-xs_1_5x',
    'effects' => array(
      10 => array(
        'label' => 'Escalar y recortar',
        'help' => 'Escalar y recortar mantendrán la relación de proporciones de la imagen original y se desechará lo que sobre por el lado mayor. Esto es especialmente útil para crear miniaturas perfectamente cuadradas sin deformar la imagen.',
        'effect callback' => 'image_scale_and_crop_effect',
        'dimensions callback' => 'image_resize_dimensions',
        'form callback' => 'image_resize_form',
        'summary theme' => 'image_resize_summary',
        'module' => 'image',
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 159,
          'height' => 102,
        ),
        'weight' => 1,
      ),
      55 => array(
        'label' => 'Subroutine',
        'help' => 'Runs another defined preset on the image.',
        'effect callback' => 'imagecache_subroutine_effect',
        'dimensions callback' => 'imagecache_subroutine_dimensions',
        'form callback' => 'imagecache_subroutine_form',
        'summary theme' => 'imagecache_subroutine_summary',
        'module' => 'imagecache_customactions',
        'name' => 'imagecache_subroutine',
        'data' => array(
          'subroutine_presetname' => 'latamgg_subroutine_set_quality_x1_5',
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: teaser_breakpoints_theme_latamggv1_screen-xs_1x.
  $styles['teaser_breakpoints_theme_latamggv1_screen-xs_1x'] = array(
    'name' => 'teaser_breakpoints_theme_latamggv1_screen-xs_1x',
    'label' => 'teaser_breakpoints_theme_latamggv1_screen-xs_1x',
    'effects' => array(
      11 => array(
        'label' => 'Escalar y recortar',
        'help' => 'Escalar y recortar mantendrán la relación de proporciones de la imagen original y se desechará lo que sobre por el lado mayor. Esto es especialmente útil para crear miniaturas perfectamente cuadradas sin deformar la imagen.',
        'effect callback' => 'image_scale_and_crop_effect',
        'dimensions callback' => 'image_resize_dimensions',
        'form callback' => 'image_resize_form',
        'summary theme' => 'image_resize_summary',
        'module' => 'image',
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 106,
          'height' => 68,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function lgg_articles_node_info() {
  $items = array(
    'article' => array(
      'name' => t('Articulo'),
      'base' => 'node_content',
      'description' => t('Contenido principal del sitio web.'),
      'has_title' => '1',
      'title_label' => t('Título'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
