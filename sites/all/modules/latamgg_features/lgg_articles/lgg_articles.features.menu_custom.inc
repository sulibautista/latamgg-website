<?php
/**
 * @file
 * lgg_articles.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function lgg_articles_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-article-categories.
  $menus['menu-article-categories'] = array(
    'menu_name' => 'menu-article-categories',
    'title' => 'Categorías de Artículos',
    'description' => '',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Categorías de Artículos');


  return $menus;
}
