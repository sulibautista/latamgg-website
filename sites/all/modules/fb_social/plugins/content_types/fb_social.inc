<?php
/**
 * @file
 *
 * FB Social plugin.
 */

$plugin = array(
  'single' => TRUE,
  'title' => t('FB Social Plugin'),
  'description' => t('Adds configurable FB social plugin pane.'),
  'category' => t('Social'),
  'edit form' => 'fb_social_pane_edit_form',
  'render callback' => 'fb_social_pane_render',
  'defaults' => array(),
);

/**
 * An edit form for the pane's settings.
 */
function fb_social_pane_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];
  $options = array('' => '-- Select a FB Plugin --');

  if ($presets = fb_social_get_presets()) {
    foreach ( $presets as $preset ) {
      if ($preset->settings['block']) {
        $options[$preset->name] = $preset->name;
      }
    }
  }

  $form['fb_social_plugin'] = array(
    '#type'          => 'select',
    '#title'         => t('FB Social Plugin'),
    '#options'       => $options,
    '#description'   => t('Render selected FB Social Plugin (where "Create as Block" has been checked).'),
    '#default_value' => $conf['fb_social_plugin'],
    '#required'      => TRUE,
  );

  return $form;
}

/**
 * Submit function for pane settings.
 */
function fb_social_pane_edit_form_submit(&$form, &$form_state) {
  $form_state['conf']['fb_social_plugin'] = $form_state['values']['fb_social_plugin'];
  drupal_set_message(t('FB Social Plugin pane has been saved.'));
}

/**
 * Render FB Social Pane widget.
 */
function fb_social_pane_render($subtype, $conf, $args, $contexts) {
  $block = new stdClass();
  $block->title = '';
  $block->content = '';

  if ($preset = fb_social_get_presets($conf['fb_social_plugin'])) {
    $block->content = fb_social_preset_view($preset);
  }

  return $block;
}
